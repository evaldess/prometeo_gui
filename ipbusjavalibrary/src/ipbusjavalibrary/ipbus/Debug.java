/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

/**
 * Utilities class for the IPbusJavaLibrary.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class Debug {

    /**
     * Boolean flag for full debug output.
     */
    private static Boolean debugIPbus = false;

    /**
     * Getter for the IPbus debug flag.
     *
     * @return the IPbus debug flag.
     */
    public static Boolean getDebugIPbus() {
        return debugIPbus;
    }

    /**
     * Setter for the IPbus debug flag.
     *
     * @param val Input IPbus debug flag.
     */
    public static void setDebugIPbus(Boolean val) {
        debugIPbus = val;
    }
}
