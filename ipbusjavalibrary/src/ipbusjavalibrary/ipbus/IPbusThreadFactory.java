/*
 * IPbusJavaLibrary project
 *
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A class to create a custom ThreadFactory. The factory creates threads that
 * have names on the form prefix-N-thread-M, where prefix is a string provided
 * in the constructor, N is the sequence number of this factory, and M is the
 * sequence number of the thread created by this factory.
 *
 * When used, all Executors methods allows the user to provide his own
 * ThreadFactory.
 *
 * The source code for this class was based entirely on
 * Executors.DefaultThreadFactory class from the JDK8 source. The only change
 * made is the ability to configure the thread name prefix.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class IPbusThreadFactory implements ThreadFactory {

    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;

    /**
     * Creates a new ThreadFactory where threads are created with a name prefix
     * of "prefix".
     *
     * @param prefix Thread name prefix. Never use a value of "pool" as in that
     * case you might as well have used
     * java.util.concurrent.Executors#defaultThreadFactory().
     */
    public IPbusThreadFactory(String prefix) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup()
                : Thread.currentThread().getThreadGroup();
        namePrefix = prefix + "-"
                + poolNumber.getAndIncrement()
                + "-thread-";
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r,
                namePrefix + threadNumber.getAndIncrement(),
                0);
        if (t.isDaemon()) {
            t.setDaemon(false);
        }
        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }

}
