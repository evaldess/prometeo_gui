/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * Abstract class to create a generic IPbus packet.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public abstract class Packet {

    /**
     * PacketHeader object header.
     */
    private PacketHeader header;

    /**
     * Header next expected Packet ID (16 bits).
     */
    private static Integer m_pkid = 0;

    /**
     * Byte stream array with the IPbus Packet content.
     */
    private byte[] bytes;

    /**
     * Length (in bytes) of the packet.
     */
    private Integer length;

    /**
     * Constructor. Creates and sets a generic IPbus packet. Constructs a
     * generic IPbus header and sets the header Packet ID with the next expected
     * Packet ID. Creates a new byte array data stream.
     */
    public Packet() {

        // Creates a new generic IPbus header.
        header = new PacketHeader();

        // Sets the header Packet ID with the next expected Packet ID.
        header.setPkid(m_pkid);

        // Initializes the Packet byte array.
        // Maximum packet size set to 1472 bytes.
        bytes = new byte[1472];
        length = 1472;
    }

    /**
     * Getter for the full packet header.
     *
     * @return the full packet header.
     */
    public PacketHeader getHeader() {
        return header;
    }

    /**
     * Getter for the length of the packet.
     *
     * @return the packet length (n bytes).
     */
    public Integer getLength() {
        return length;
    }

    /**
     * Setter for the length of the packet.
     *
     * @param length Input packet length (Integer, in bytes).
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * Getter for the header Packet ID.
     *
     * @return the Packet ID.
     */
    public Integer getPacketID() {
        return header.getPkid();
    }

    /**
     * Setter for the header PacketID.
     *
     * @param pkID Input Packet ID (Integer).
     */
    public void setPacketID(Integer pkID) {
        header.setPkid(pkID);
        //ByteTools.addBytes(bytes, 0, header.getHeader());
    }

    /**
     * Getter for the header Packet Type ID.
     *
     * @return the Packet Type ID.
     */
    public Integer getType() {
        return header.getType();
    }

    /**
     * Setter for the header Packet Type ID.
     *
     * @param type Input Packet Type ID (Integer).
     */
    public void setType(Integer type) {
        header.setType(type);
        //ByteTools.addBytes(bytes, 0, header.getHeader());
    }

    /**
     * Gets the Packet Type as a string.
     *
     * @param type Input Packet Type.
     * @return a string with the IPbus Packet Type.
     */
    public String getPacketType(Integer type) {
        if (null == type) {
            return "undefined";
        } else {
            switch (type) {
                case 0:
                    return "CONTROL";
                case 1:
                    return "STATUS";
                case 2:
                    return "RESEND";
                default:
                    return "undefined";
            }
        }
    }

    /**
     * Getter for the next expected Packet ID.
     *
     * @return the next expected Packet ID.
     */
    public static Integer getNextPacketID() {
        return m_pkid;
    }

    /**
     * Setter for the next expected IPbus packet header value. This is a 16-bit
     * value.
     *
     * @param pkid Next expected IPbus packet header value.
     */
    public static void setNextPacketID(Integer pkid) {
        if (m_pkid > 0xFFFF) {
            System.out.println("  Packet.setNextPacketID(): next expected PacketID is too high");
            System.out.println("                            nothing done!");
            return;
        }
        m_pkid = pkid;
        if (Debug.getDebugIPbus()) {
            System.out.println("  Packet.setNextPacketID(): next expected PacketID -> " + m_pkid
                    + String.format(" (0x%04X)", m_pkid));
        }
    }

    /**
     * Increments the value of the next expected IPbus packet header. If the
     * value is more than 16 bits, sets it to zero.
     */
    public static void incrementNextPacketID() {
        m_pkid++;
        if (m_pkid > 0xFFFF) {
            m_pkid = 0;
        }
        if (Debug.getDebugIPbus()) {
            System.out.println("  Packet.incrementNextPacketID(): next expected PacketID -> "
                    + m_pkid + String.format(" (0x%04X)", m_pkid));
        }
    }

    /**
     * Overridable method. Increments the generic IPbus header Packet ID in
     * child subclasses.
     */
    public void sent() {
    }

    /**
     * Dump packet bytes in formatted 32-bit words in hexadecimal.
     */
    public void dumpBytes() {
        ByteTools.dumpBytesHex(bytes, length);
    }

    /**
     * Dump packet bytes in formatted 32-bit words in hexadecimal.
     */
    public void dumpHeader() {
        System.out.println(String.format("0x%08X", header.getHeader()));
    }

    /**
     * Dump total 32-bit words in packet.
     */
    public void dumpTotalWords() {
        ByteTools.dumpTotalWords(bytes, length);
    }

    /**
     * Abstract method to add values to the input byte stream array. The method
     * is implemented in child class methods of the Packet class, this is, the
     * Status, Transaction and Resend classes.
     *
     * @param b Input byte array destination where to add data.
     * @param pos Byte index position where to start to add byte contents.
     * @return the last index position in the input destination byte array.
     */
    public abstract Integer addBytes(byte[] b, Integer pos);

    /**
     * Abstract method to parse byte stream. The method is implemented in child
     * class methods of the Packet class, this is, the Status, Transaction and
     * Resend classes.
     *
     * @param b Input byte array destination where to set the data.
     * @param pos Byte array index position where to start to set byte contents.
     * @return the last index position in the input destination byte array.
     */
    public abstract Integer setBytes(byte[] b, Integer pos);

    /**
     * Gets the Packet byte array data stream contents.
     *
     * @return the byte array contents of the Packet data stream.
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Sets the Packet byte array data stream contents and length (in bytes)
     * according to the input byte array and length.
     *
     * @param b Input byte array Packet data stream.
     * @param length Input length (in bytes) of the Packet data stream.
     */
    public void copyBytes(byte[] b, int length) {
        this.length = length;
        for (Integer i = 0; i < length; i++) {
            bytes[i] = b[i];
        }
    }

    /**
     *
     */
    public void pack() {
        Integer pos = ByteTools.addBytes(bytes, 0, header.getHeader());
        pos = addBytes(bytes, pos);
        if (bytes.length > pos) {
            copyBytes(bytes, pos);
        }
        length = pos;
    }

    /**
     * Sets the Packet generic IPbus full header from the first four bytes of
     * the Packet byte array stream.
     */
    public void unpack() {
        header.setHeader(ByteTools.pack(bytes, 0));
        setBytes(bytes, 4);
    }
}
