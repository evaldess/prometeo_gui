/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * Creates an IPbus generic packet header and bytes content.
 *
 * @author Juan Valls (2016) Taken from Carlos Solans (CERN)
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class PacketBuilder {

    /**
     * IPbus generic header of the built packet.
     */
    private PacketHeader header;

    /**
     * Byte array with the IPbus PacketBuilder content.
     */
   private byte[] bytes;

    /**
     * Length (in bytes) of the built packet.
     */
    private Integer length;

    /**
     * Constructor. Creates an IPbus generic packet header. Then crates a byte
     * array for the packet.
     */
    public PacketBuilder() {

        // Creates an IPbus packet header by constructing an instance 
        // of PacketHeader class.
        header = new PacketHeader();

        // Creates byte array data stream for the PacketBuilder.
        // A new byte array in Java will automatically be 
        // initialized with all zeroes.
        length = 1472;
        bytes = new byte[length];
    }

    /**
     * Getter for the PacketBuilder byte array field.
     *
     * @return PacketBuilder data byte array.
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Setter for the PacketBuilder byte array length (in bytes).
     *
     * @param length PacketBuilder byte array length (in bytes).
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * Dump packet bytes in formatted 32-bit words in hexadecimal.
     */
    public void dumpPacketWords() {
        ByteTools.dumpBytesHex(bytes, length);
    }

    /**
     * Dump total 32-bit words in packet.
     */
    public void dumpTotalWords() {
        ByteTools.dumpTotalWords(bytes, length);
    }

    /**
     * Extracts the IPbus generic header word into header bits.
     */
    public void unpack() {
        header.setBytes(bytes, 0);
    }

    /**
     * Getter for the IPbus generic packet header. A PacketHeader object type.
     *
     * @return the IPbus generic header PacketHeader object type.
     */
    public PacketHeader getHeader() {
        return header;
    }

    /**
     * Checks the integrity of the IPbus generic header.
     *
     * @return the status of the integrity check.
     */
    public Boolean checkHeader() {
        return !((header.getPver().intValue() != PacketHeader.VERSION)
                || (header.getByoq().intValue() != PacketHeader.BYOQ));
    }

    /**
     * Returns an IPbus Packet object of type STATUS, CONTROL or RESEND
     * according to the generic IPbus Packet Type.
     *
     * @return Packet object of type STATUS, CONTROL or RESEND.
     */
    public Packet getPacket() {

        // Declares reference to a Packet object.
        Packet p;

        // Creates an instance of a STATUS, CONTROL or RESEND class.
        if (header.getType().intValue() == PacketHeader.CONTROL) {
            p = new Transaction();
        } else if (header.getType().intValue() == PacketHeader.STATUS) {
            p = new Status();
        } else if (header.getType().intValue() == PacketHeader.RESEND) {
            p = new Resend();
        } else {
            return null;
        }

        // Sets the PacketBuilder byte content.
        p.copyBytes(bytes, length);

        // Sets the PacketBuilder generic IPbus full header content.
        p.unpack();
        if (Debug.getDebugIPbus()) {
            System.out.println("  PacketBuilder.getPacket() -> DatagramPacket " + getPacketType()
                    + " received. Byte contents set.");
        }
        return p;
    }

    public String getPacketType() {
        // Creates an instance of a STATUS, CONTROL or RESEND class.
        if (header.getType().intValue() == PacketHeader.CONTROL) {
            return "CONTROL";
        } else if (header.getType().intValue() == PacketHeader.STATUS) {
            return "STATUS";
        } else if (header.getType().intValue() == PacketHeader.RESEND) {
            return "RESEND";
        } else {
            return "undefined";
        }
    }

    /**
     * Returns a CONTROL transaction packet object from the PacketBuilder data
     * contents.
     *
     * @return the CONTROL transaction packet object from the PacketBuilder
     * data.
     */
    public Transaction getTransaction() {
        Transaction t = new Transaction();
        t.copyBytes(bytes, length);
        t.unpack();
        return t;
    }

    /**
     * Returns a STATUS packet object from the PacketBuilder data contents.
     *
     * @return the STATUS packet object from the PacketBuilder data.
     */
    public Status getStatus() {
        Status s = new Status();
        s.copyBytes(bytes, length);
        s.unpack();
        return s;
    }

    /**
     * Returns a RESEND packet object from the PacketBuilder data contents.
     *
     * @return the RESEND packet object from the PacketBuilder data.
     */
    public Resend getResend() {
        Resend r = new Resend();
        r.copyBytes(bytes, length);
        r.unpack();
        return r;
    }
}
