/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * Generic IPbus header constructor with methods to set and retrive its
 * contents.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class PacketHeader {

    /**
     * Packet Protocol Version value, 4 bits [28-31].
     */
    private Integer pver;

    /**
     * Packet ID value, 16 bits [8-23].
     */
    private Integer pkid;

    /**
     * Byte Order Qualifier value, 4 bits [4-7].
     */
    private Integer byoq;

    /**
     * Packet Type value, 4 bits [0-3].
     */
    private Integer type;

    /**
     * Full packet header value.
     */
    private Integer head;

    /**
     * Packet Type CONTROL.
     */
    public static Integer CONTROL = 0;

    /**
     * Packet Type STATUS.
     */
    public static Integer STATUS = 1;

    /**
     * Packet Type RESEND.
     */
    public static Integer RESEND = 2;

    /**
     * Packet Byte Order Qualifier.
     */
    public static Integer BYOQ = 0xF;

    /**
     * Packet Protocol Version.
     */
    public static Integer VERSION = 2;

    /**
     * Constructor. Sets default IPbus generic packet header bit contents. Then
     * packs the bits into the 32-bit header word.
     */
    public PacketHeader() {
        pver = VERSION;
        pkid = 0;
        byoq = BYOQ;
        type = 0;
        head = 0;
        pack();
    }

    /**
     * Sets (or packs) IPbus generic header bit contents into header word.
     */
    private void pack() {
        head = 0;
        head |= (pver & 0xF) << 28;
        head |= (pkid & 0xFFFF) << 8;
        head |= (byoq & 0xF) << 4;
        head |= (type & 0xF) << 0;
    }

    /**
     * Extract IPbus generic header word into header bits.
     */
    private void unpack() {
        pver = (int) ((0xF0000000 & head) >>> 28);
        pkid = (int) ((0x00FFFF00 & head) >>> 8);
        byoq = (int) ((0x000000F0 & head) >>> 4);
        type = (int) (0x0000000F & head);
    }

    /**
     *
     */
    public void dumpIPbusHeader() {
        System.out.println("Pver: " + pver);
        System.out.println("Pkid: " + pkid);
        System.out.println("Byoq: " + byoq);
        System.out.println("Type: " + type);
    }

    /**
     * Getter for the header Protocol Version.
     *
     * @return the Protocol Version.
     */
    public Integer getPver() {
        return pver;
    }

    /**
     * Setter for the header Protocol Version.
     *
     * @param pver Input Protocol Version bits (Integer).
     */
    public void setPver(Integer pver) {
        this.pver = pver;
        pack();
    }

    /**
     * Getter for the generic IPbus header Packet Type.
     *
     * @return the Packet Type.
     */
    public Integer getType() {
        return type;
    }

    /**
     * Setter for the generic IPbus header Packet Type.
     *
     * @param type Input Packet Type bits (Integer).
     */
    public void setType(Integer type) {
        this.type = type;
        pack();
    }

    /**
     * Getter for the generic IPbus header Packet Byte Order Qualifier.
     *
     * @return the Byte Order Qualifier.
     */
    public Integer getByoq() {
        return byoq;
    }

    /**
     * Setter for the generic IPbus header Byte Order Qualifier.
     *
     * @param byoq Input Byte Order Qualifier bits (Integer).
     */
    public void setByoq(Integer byoq) {
        this.byoq = byoq;
        pack();
    }

    /**
     * Getter for the generic IPbus header Packet ID.
     *
     * @return the Packet ID.
     */
    public Integer getPkid() {
        return pkid;
    }

    /**
     * Setter for the generic IPbus header Packet ID.
     *
     * @param pkid Input Packet ID bits (Integer).
     */
    public void setPkid(Integer pkid) {
        this.pkid = pkid;
        pack();
    }

    /**
     * Getter for the packet full header.
     *
     * @return the full packet header value (head).
     */
    public Integer getHeader() {
        return head;
    }

    /**
     * Setter for packet full header.
     *
     * @param head Full packet header value.
     */
    public void setHeader(Integer head) {
        this.head = head;
        unpack();
    }

    /**
     * Sets (or packs) the IPbus generic header word with an input byte array
     * starting at a specified index byte array position. Then extracts the
     * IPbus generic header word into header bits.
     *
     * @param b Input byte stream.
     * @param pos Byte index position where to start to pack byte contents.
     */
    public void setBytes(byte[] b, Integer pos) {
        head = ByteTools.pack(b, pos);
        unpack();
    }

}
