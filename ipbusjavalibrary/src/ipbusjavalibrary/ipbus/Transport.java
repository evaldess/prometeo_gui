/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Abstract class to create a generic IPbus packet.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public abstract class Transport {

    /**
     * Declares a PacketBuilder variable used to construct the response packet
     * coming from a socket.Used in the send() method.
     */
    public PacketBuilder responsePacketBuilder;

    /**
     *
     */
    public Transport() {
        responsePacketBuilder = new PacketBuilder();
    }

    /**
     * Sends an input request IPbus Packet object and returns an IPbus response
     * Packet.
     *
     * @param inputPacket Input IPbus request Packet.
     * @return an IPbus response Packet.
     */
    public abstract Packet send(Packet inputPacket);

    /**
     *
     * @param milis Input time in milliseconds.
     * @return the boolean status to the call to setSoTimeout().
     */
    public abstract Boolean setTimeout(Integer milis);

    /**
     *
     * @return a Boolean with the status of Transport constructor.
     */
    public abstract Boolean getTransportReturn();

    /**
     *
     * @return the Transport thread pool executor.
     */
    public abstract ThreadPoolExecutor getTransportExecutor();

    /**
     *
     * @return a Boolean with the status of close conection.
     */
    public abstract Boolean closeConnection();
}
