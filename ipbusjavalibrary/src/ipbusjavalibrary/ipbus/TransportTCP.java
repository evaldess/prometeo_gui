/*
 * IPbusJavaLibrary project
 * 2019 (c) IFIC-CERN
 */
package ipbusjavalibrary.ipbus;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ExecutionException;
import java.util.Objects;

import ipbusjavalibrary.utilities.ByteTools;

/**
 * Abstract class to create a generic IPbus packet.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class TransportTCP extends Transport {

    /**
     * Java Socket.
     */
    private Socket socket;

    private DataOutputStream m_out;

    private DataInputStream m_in;

    private InetAddress m_host1, m_host2;

    private Integer m_port1, m_port2;

    private Integer m_addr2;

    /* Atomic Integer variable for concurrent accesses. It is used for handling
     * received transactions after sending data through a socket.
     */
    private AtomicInteger socketStatus;

    /**
     * Byte array with preamble.
     */
    private byte[] preamble;

    /**
     * Declares and defines a ThreadPoolExecutor class object to create a thread
     * pool. A thread pool manages a collection of Runnable threads.
     */
    private ThreadPoolExecutor executor;

    // Defines instance of ThreadPoolExecutor class to create a thread pool.
    private Callable<Boolean> CallableRecPackAck;

    Future<Boolean> futureReadAll;

    /**
     * Status boolean flag from the TransportTCP instance object constructor.
     */
    private Boolean retTransportTCP = false;

    /**
     *
     * @param host1 Input host1 String.
     * @param port1 Input port1 Integer.
     * @param host2 Input host2 String.
     * @param port2 Input port2 Integer.
     * @param threadFactoryName Input name for the ThrteadPoolExecutor thread.
     */
    public TransportTCP(String host1, Integer port1,
            String host2, Integer port2,
            String threadFactoryName) {

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportTCP() -> host1 = "
                    + host1 + "  port1 = " + port1
                    + "  host2 = " + host2 + "  port2 = " + port2
                    + "  ThreadPoolExecutor: " + threadFactoryName);
        }

        // Defines a thread pool through a ThreadPoolExecutor.
        executor = new ThreadPoolExecutor(
                20,
                20,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new IPbusThreadFactory(threadFactoryName));

        // Defines Callables through lambda expressions.
        initDefineCallables();

        socketStatus = new AtomicInteger(0);

        // Defines preamble array (3 words x 4 bytes = 12 bytes).
        preamble = new byte[12];

        // Gets the IP address for the remote host as an InetAddress object, given 
        // the host's name.
        try {
            m_host1 = InetAddress.getByName(host1);
        } catch (UnknownHostException ex) {
            System.out.println("TransportTCP() -> UnknownHostException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportTCP = false;
            return;
        } catch (SecurityException ex) {
            System.out.println("TransportTCP() -> SecurityException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportTCP = false;
            return;
        }

        m_port1 = port1;

        // Gets the IP address for remote host2 as an InetAddress object, given 
        // the host's name.
        try {
            m_host2 = InetAddress.getByName(host2);
        } catch (UnknownHostException ex) {
            System.out.println("TransportTCP() -> UnknownHostException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportTCP = false;
            return;
        } catch (SecurityException ex) {
            System.out.println("TransportTCP() -> SecurityException on InetAddress.getByName = "
                    + ex.getMessage());
            retTransportTCP = false;
            return;
        }

        m_addr2 = ByteTools.pack(m_host2.getAddress(), 0);

        m_port2 = port2;

        // Creates a client Socket to initiate a communication with 
        // a host/server for sending/receiving packets.
        // Throws a SocketException.
        try {
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportTCP() -> trying to create Socket... ");
            }
            socket = new Socket(m_host1, m_port1);
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportTCP() -> TCP Socket created. ");
            }
        } catch (IOException ex) {
            System.out.println("TransportTCP() -> IOException on creating Socket() in host: "
                    + host1 + " port: " + port1 + " = " + ex);
            retTransportTCP = false;
            return;
        } catch (IllegalArgumentException ex) {
            System.out.println("TransportTCP() -> IllegalArgumentException on creating Socket() = " + ex);
            retTransportTCP = false;
            return;
        }

        // Enables TCP_NODELAY (disables Nagle's algorithm).
        try {
            socket.setTcpNoDelay(true);
        } catch (SocketException ex) {
            System.out.println("TransportTCP() -> SocketException on setting socket.setTcpNoDelay(true) = " + ex);
            retTransportTCP = false;
            return;
        }

        // Defines a new data output stream to write data to the specified underlying output stream.
        // A data output stream lets the application write primitive Java data types to an 
        // output stream in a portable way. With it, one may now use a data input stream to read the data back in.
        try {
            m_out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            System.out.println("TransportTCP() -> IOException on creating DataOutputStream() = " + ex);
            retTransportTCP = false;
            return;
        }

        // Creates a DataInputStream that uses the specified underlying InputStream.
        try {
            m_in = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            System.out.println("TransportTCP() -> Exception on creating DataInputStream() = " + ex);
            retTransportTCP = false;
            return;
        }

        retTransportTCP = true;
    }

    /**
     *
     * @param milis Input time in milliseconds.
     * @return the boolean status to the call to setSoTimeout().
     */
    @Override
    public Boolean setTimeout(Integer milis) {
        try {
            if (socket != null) {
                if (Debug.getDebugIPbus()) {
                    System.out.println("TransportTCP.setTimeout() -> setting timeout to: " + milis + " milliseconds");
                }
                socket.setSoTimeout(milis);
            }
        } catch (IOException ex) {
            System.out.println("TransportTCP.setTimeout() -> IOException error: " + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     *
     * @return the close connection return Boolean status.
     */
    @Override
    public Boolean closeConnection() {
        try {
            socket.close();
        } catch (IOException ex) {
            System.out.println("TransportTCP.closeConnection() -> IOException error: " + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Sends an input request IPbus Packet object and returns an IPbus response
     * Packet.
     *
     * @param inputPacket Input IPbus request Packet.
     * @return an IPbus response Packet.
     */
    @Override
    public Packet send(Packet inputPacket) {
        // Packs...
        inputPacket.pack();

        // Creates reference to the the IPbus response Packet object. 
        // Initialized to null.
        Packet response = null;

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportTCP.send() -> request frame created from input Packet object of type "
                    + inputPacket.getPacketType(inputPacket.getType()) + ".");
            System.out.println("TransportTCP.send() -> PacketID: " + inputPacket.getPacketID()
                    + String.format(" (0x%04X)", inputPacket.getPacketID()));
            System.out.println("TransportTCP.send() -> Packet length: " + inputPacket.getLength() + " bytes");
            System.out.println("TransportTCP.send() -> Dump byte frames to be sent:");
            inputPacket.dumpTotalWords();
            inputPacket.dumpBytes();
        }

        // Sends a preamble and the request packet to the host. 
        // Builds first a 3-word preamble (12 bytes).
        // 0 [31- 0] tcp packet size
        // 1 [31- 0] device address 
        // 2 [31-16] device port
        // 2 [15- 0] number of 32-bit words in the IPbus packet
        ByteTools.addBytes(preamble, 0, inputPacket.getLength() + 8);
        ByteTools.copyBytes(m_host2.getAddress(), 0, preamble, 4, 4, false);
        preamble[8 + 0] = (byte) ((m_port2 >> 8) & 0xFF);
        preamble[8 + 1] = (byte) ((m_port2) & 0xFF);
        preamble[8 + 2] = (byte) (((inputPacket.getLength() / 4) >> 8) & 0xFF);
        preamble[8 + 3] = (byte) (((inputPacket.getLength() / 4)) & 0xFF);

        // Writes the preamble.
        try {
            m_out.write(preamble, 0, preamble.length);
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportTCP.send() -> request preamble successfully write through DataOutputStream.");
            }
        } catch (IOException ex) {
            System.out.println("TransportTCP.send() -> IOException on writing preamble through DataOutputStream = "
                    + ex.getMessage());
            return response;
        }

        // Writes the request packet.
        try {
            m_out.write(inputPacket.getBytes(), 0, inputPacket.getLength());
            if (Debug.getDebugIPbus()) {
                System.out.println("TransportTCP.send() -> request packet successfully write through DataOutputStream.");
            }
        } catch (IOException ex) {
            System.out.println("TransportTCP.send() -> IOException on writing packet data through DataOutputStream = "
                    + ex.getMessage());
            return response;
        }

        //inputPacket.sent();
        socketStatus.set(0);

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportTCP.send() -> Thread started. Waiting for server response from the Socket...");
        }

        // Receive acknowledge. Executes the given task.
        try {
            futureReadAll = executor.submit(CallableRecPackAck);
        } catch (RejectedExecutionException e) {
            System.out.println("TransportTCP.send() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("TransportTCP.send() -> NullPointerException = "
                    + e.getMessage());
        }

        // Wait for data or timeout
        while (socketStatus.get() == 0) {
        }

        if (socketStatus.get() == 1) {

            try {
                if (!futureReadAll.get()) {
                    System.out.println("TransportTCP.send() -> return from thread... " + futureReadAll.get());
                    return null;
                }
            } catch (InterruptedException ex) {
                System.out.println("TransportTCP.send() -> InterruptedException on futureReadAll.get() = "
                        + ex.getMessage());
            } catch (ExecutionException ex) {
                System.out.println("TransportTCP.send() -> ExecutionException on futureReadAll.get() = "
                        + ex.getMessage());
            }

            // According to header, set IPbus response packet to 
            // either STATUS, CONTROL or RESENT type
            response = responsePacketBuilder.getPacket();

            if (Debug.getDebugIPbus()) {
                System.out.println("TransportTCP.send() -> Dump incoming response packet byte frames:");
                response.dumpTotalWords();
                response.dumpBytes();
            }
        }

        return response;
    }

    /**
     * Getter with a boolean flag from the TransportTCP instance object
     * constructor.
     *
     * @return a Boolean with the status of TransportTCP constructor.
     */
    @Override
    public Boolean getTransportReturn() {
        return retTransportTCP;
    }

    /**
     *
     * @return the executor thread pool.
     */
    @Override
    public ThreadPoolExecutor getTransportExecutor() {
        return executor;
    }

    /**
     * Define Callables through lambda expressions. A Callable object is an
     * interface designed to provide a common protocol for objects that wish to
     * execute code while they are active
     */
    public final void initDefineCallables() {
        CallableRecPackAck = () -> {
            return send_ReceivedPacket();
        };
    }

    /**
     *
     * @return the status
     */
    public Boolean send_ReceivedPacket() {
        // Response preamble.
        // 0 [31- 0] tcp packet size
        // 1 [31- 0] device address top
        // 2 [31- 0] device address
        // 3 [15- 0] device port
        // 3 [31-16] error code
        Integer bytecount = 0;
        try {
            bytecount = m_in.readInt();
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.readInt() for bytecount = "
                    + ex.getMessage());
            socketStatus.set(1);
            return false;
        }

        Integer deviceaddr = 0;
        try {
            deviceaddr = m_in.readInt();
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.readInt() for deviceaddr = "
                    + ex.getMessage());
        }

        Integer devicehost = 0;
        try {
            devicehost = m_in.readInt();
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.readInt() for devicehost = "
                    + ex.getMessage());
        }

        Integer deviceport = 0;
        try {
            deviceport = m_in.readShort() & 0xFFFFFFFF;
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.readShort() for deviceport = "
                    + ex.getMessage());
        }

        Integer errorcode = 0;
        try {
            errorcode = m_in.readShort() & 0xFFFFFFFF;
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.readShort() for errorcode = "
                    + ex.getMessage());
        }

        if (Debug.getDebugIPbus()) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> preamble: ");
            System.out.println("  bytecount: " + bytecount);
            System.out.println("  deviceaddr: " + deviceaddr);
            System.out.println("  devicehost: " + devicehost + " expected: " + m_addr2);
            System.out.println("  deviceport: " + deviceport + " " + ((deviceport >> 16) & 0xFFFF));
            System.out.println("  errorcode: " + errorcode);
        }

        if (Objects.equals(devicehost, m_addr2)) {
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> Reply does not match target address");
        }

        if (Objects.equals(deviceport, m_port2)) {
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> Reply does not match target port");
        }

        switch (errorcode) {
            case 0:
                /*all ok*/ break;
            case 1:
            case 3:
            case 4:
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> No response from target");
            case 2:
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> Timeout from target");
            case 5:
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> Malformed status from target");
            default:
            //throw new IOException("TransportTCP.send_ReceivedPacket() -> Unknown error code " + errorcode);
        }

        // Reads the response PacketBuilder.
        try {
            m_in.read(responsePacketBuilder.getBytes(), 0, bytecount - 8);
        } catch (IOException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IOException on DataInputStream.read() = "
                    + ex.getMessage());
        } catch (NullPointerException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> NullPointerException on DataInputStream.read() = "
                    + ex.getMessage());
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("TransportTCP.send_ReceivedPacket() -> IndexOutOfBoundsException on DataInputStream.read() = "
                    + ex.getMessage());
        }

        // Sets the response PacketBuilder length.
        responsePacketBuilder.setLength(bytecount - 8);

        responsePacketBuilder.unpack();

        socketStatus.set(1);

        return true;
    }
}
