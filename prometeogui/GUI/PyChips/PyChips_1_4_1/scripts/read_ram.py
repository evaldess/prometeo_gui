#  Import the PyChips code - PYTHONPATH must be set to the PyChips installation src folder!
from PyChipsUser import *
from time import sleep

##################################################################################################
### Uncomment one of the following two lines to turn on verbose or very-verbose debug modes.   ###
### These debug modes allow you to see the packets being sent and received.                    ###
##################################################################################################
#chipsLog.setLevel(logging.DEBUG)    # Verbose logging (see packets being sent and received)

import time

# Read in an address table by creating an AddressTable object (Note the forward slashes, not backslashes!)
glibAddrTable = AddressTable("./glibAddrTable.dat")

f = open('./ipaddr.dat', 'r')
ipaddr = f.readline()
f.close()
ppr = ChipsBusUdp(glibAddrTable, ipaddr, 50001)
print
print "--=======================================--"
print "  Opening PPR with IP", ipaddr
print "--=======================================--"


#print glib.read("ram_enable_0")
#glib.write("ram_enable_0",1)
print "-> Dummy register is", uInt32HexStr(ppr.read("dummy"))
data = []
nreg = 16
npmts = 48
start_time = time.time()
for i in range(0,npmts):
    data.append(ppr.blockRead("dummy",nreg))
for i in range(0,npmts):
    for j in range(0,nreg):
        print "-> Dummy register number",i,"  is:", uInt32HexStr(data[i][j])
    print
print("--- %s seconds ---" % (time.time() - start_time))