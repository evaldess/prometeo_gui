#  Import the PyChips code - PYTHONPATH must be set to the PyChips installation src folder!
#
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#

import sys
sys.path.append('./PyChips/PyChips_1_4_1/src')
from PyChipsUser import *

class ipbus:
    def __init__(self):       
        AddrTable = AddressTable("common/AddrTable.dat")
        ipaddr = '192.168.0.1'
        self.ipbus = ChipsBusUdp(AddrTable, ipaddr, 50001)
        print
        print "--=======================================--"
        print "  Opening PPR with IP", ipaddr
        print "--=======================================--"
    def ResetCounter(self):
        self.ipbus.write("rst_counter",0x0)
        self.ipbus.write("rst_counter",0x1)
        self.ipbus.write("rst_counter",0x0)
    def AsyncWrite(self,md,db,cmd):
        self.ipbus.write("Async_md",md)
        self.ipbus.write("Async_db_addr",db)
        self.ipbus.write("Async_data",cmd)
    def SyncWrite(self,md,db,bcid,cmd):
        self.ipbus.write("Sync_md", md)
        self.ipbus.write("Sync_bcid", bcid)
        self.ipbus.write("Sync_db_addr", db)
        self.ipbus.write("Sync_data", cmd)
    def SyncClear(self):
        self.ipbus.write("Sync_dis_DCS", 0x0)
        self.ipbus.write("Sync_rst_cnt", 0x0)
        self.ipbus.write("Sync_nb_iter", 0x0)
        self.ipbus.write("Sync_en", 0x0)
        self.ipbus.write("Sync_rst", 0x1)
    def SyncRest(self):
        self.ipbus.write("Sync_dis_DCS", 0x0)
        self.ipbus.write("Sync_rst_cnt", 0x0)
        self.ipbus.write("Sync_nb_iter", 0x0)
        self.ipbus.write("Sync_en", 0x0)
        self.ipbus.write("Sync_rst", 0x0)   
    def SyncLoop(self,iter):
        self.ipbus.write("Sync_dis_DCS", 0x0)
        self.ipbus.write("Sync_rst_cnt", 0x0)
        self.ipbus.write("Sync_rst", 0x0)   
        self.ipbus.write("Sync_en", 0x1)       
        self.ipbus.write("Sync_nb_iter", iter)
        
    def ReadPipelines(self,md, ch, gain):
        offset = (ch+md*12)*32
        samples = [0]*16
        if gain: #High gain
            samples =  self.ipbus.blockRead("PipelineHG",16,offset)
        else:   # Low gain
            samples =  self.ipbus.blockRead("PipelineLG",16,offset)
    #        print " Read from:", uInt32HexStr(md), "", uInt32HexStr(ch)
        return samples