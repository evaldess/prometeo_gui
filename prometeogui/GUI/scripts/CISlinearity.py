#
# Migrated from Demo scripts
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import numpy as np
from scipy import stats
import sys
sys.path.append('./common')
from ipbus import ipbus

def CISlinearity(gain,iterations):
    ppr = ipbus()
    
    ###################
    ### CIS CONFIGURATION ###
    BCID_discharge = 2200 # 256 0x800(2048)
    BCID_charge = 500
    
    bcid_l1a = BCID_discharge + 41# 73 #41
    
    Gain_cis = gain ## '1' HG, '0' LG
    ################################
    nsteps=iterations
  
    stableP=1650
    stableM=2530
    
    pedestalDAC=1
    
    offset=100
    nDAC=nsteps
    step=(4096-offset)/nDAC
    DAC_CIS_list = [0]*nsteps
    print ("nDAC", nDAC)
    print ("nsteps", nsteps)
    print ("step", step)
    for i in range(nDAC):
        DAC_CIS_list[i] =(i*step)+offset
    
    nchan = 12 # 6: A side - 12: Both sides
    ######################################
    nMD=4# Number of Minidrawers to be tested
    firstMD=0# .. starting from this MD ( MD goes from 0 to 3)
    ######################################
    
    #FPGA_map = [1,3,1,3,1,3,0,2,0,2,0,2]
    #card_map = [2,2,1,1,0,0,2,2,1,1,0,0]
    
    
    #####========================================#####
    ##### Set the pedestal for all the channels  #####
    #####========================================#####
    stableP=1650
    stableM=2530
    
    
    chargeP=stableP+pedestalDAC
    chargeM=stableM-pedestalDAC
    
    #Disable TTC
    ppr.ipbus.write("cfg1", 0x0)
    #Disable Deadtime 
    ppr.ipbus.write("cfg2",0x0)
    ppr.ResetCounter
    ppr.ipbus.write("cfg2_dead_time",1)  
    
    #############
    
    
    CMD=0
    TPH=0 #0-pass through DB pulse, 1-open, 2-closed 
    TPL=0 #pass thru
    ITG=1 #enable
    S1=0
    S2=0
    S3=0
    S4=0
    TRG=1 #1 trigger is disabled
    cmd=(CMD<<12)|(TPH<<10)|(TPL<<8)|(ITG<<7)|(S4<<6)|(S3<<5)|(S2<<4)|(S1<<3)|(TRG<<2)
    for md in range(firstMD,firstMD+nMD):
        for adc in range(12):
            FPGA = ((adc/6)<<1)+ (adc%2)
            card = (adc/2)%3
#                FPGA= FPGA_map[adc]
#                card = card_map[adc]            
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x6000+int(chargeP))
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x7000+int(chargeM))
            ppr.AsyncWrite((md+1),0x1,0x8000000)
    
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x4000+int(chargeP))
            ppr.AsyncWrite((md+1),0x1,0x8000000)
    
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x5000+int(chargeM))
            ppr.AsyncWrite((md+1),0x1,0x8000000)
    
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xc000+int(0)) #loadLG
            ppr.AsyncWrite((md+1),0x1,0x80000000)
            ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xd000+int(0)) #loadHG
            ppr.AsyncWrite((md+1),0x1,0x80000000)
    
            ppr.AsyncWrite((md+1),0x1,(1<<22)|(FPGA<<18)|(card<<16)|cmd) #SWITCHES
            ppr.AsyncWrite((md+1),0x1,0x80000000)
    		
    samplesLG = np.zeros((4,12,nsteps))
    samplesHG = np.zeros((4,12,nsteps))
    
    #####========================================#####
    ##### Set the DAC for CIS is all the channels and configure TPH/TPL from DB  #####
    #####========================================#####
    
    ## Event LOOP ##
    CIS_Enable=(0 << 16)
    Enable_LED=0x10
    for md in range(firstMD,firstMD+nMD):
        print (" ++ Enable Automatic CIS from DB")
        #enable
        BCIDcharge=(BCID_charge << 2)
        BCIDdischarge=(BCID_discharge << 14)
        CIS_Enable=1
        CIS_Gain=(Gain_cis << 1)
        ppr.AsyncWrite((md+1),0x121, BCIDdischarge + BCIDcharge + CIS_Gain + CIS_Enable)
        ppr.AsyncWrite((md+1),0x1,0x80000000)
        ppr.AsyncWrite(0,0x14,CIS_Enable + Enable_LED)
        print( "Lista", DAC_CIS_list)
        iterr = 0
    for ncharge in range(nDAC):
        print "Iteration number", iterr
        iterr +=1
        print " +++++++  New Event ++++++++"
        for md in range(firstMD,firstMD+nMD):
            for adc in range(nchan):
                FPGA = ((adc/6)<<1)+ (adc%2)
                card = (adc/2)%3
#                FPGA= FPGA_map[adc]
#                card = card_map[adc]
                ppr.AsyncWrite((md+1),0x1,0x80000000)
                ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0x1000+int(DAC_CIS_list[ncharge]))
                ppr.AsyncWrite((md+1),0x1,0x80000000)
    
        maxidhg =[0]*nchan*nMD
        maxidlg=[0]*nchan*nMD
        maxsamplehg=[0]*nchan*nMD
        maxsamplelg=[0]*nchan*nMD
        minsamplehg=[0]*nchan*nMD
        minsamplelg=[0]*nchan*nMD
    
                #####================================#####
                ##### Configure the L1A in the PPr #####
                #####===============================#####
        print (" ++ Configure L1A generation in the PPR")
        ppr.SyncClear
        ppr.SyncRest
        for t in range (0,20):
            ppr.SyncWrite(0,0,bcid_l1a&0xFFF, 3)
        ppr.SyncLoop(0)                 
        ##################################################
        ####  Now read the data from the PPr mem ########
        ##################################################
        for md in range(firstMD,firstMD+nMD):
            for adc in range(nchan):
                ppr.ipbus.read("LastEventBCID")
                ppr.ipbus.read("LastEventL1ID")
                hg = ppr.ReadPipelines(md, adc, 1)
                lg = ppr.ReadPipelines(md, adc, 0)                            
                maxidhg[adc]=0
                maxidlg[adc]=0
                maxsamplehg[adc]=0
                maxsamplelg[adc]=0
                minsamplehg[adc]=10000
                minsamplelg[adc]=10000
                for a in range(len(hg)):
                    hg[a] = hg[a] & 0xFFF
                    lg[a] = lg[a] & 0xFFF
                    maxsamplehg[adc]=np.amax(hg)
                    maxsamplelg[adc]=np.amax(lg)
    			## Force the pedestal as the first sample
                minsamplelg[adc]=lg[0]
                minsamplehg[adc]=hg[0]
                if(maxsamplehg[adc]-minsamplehg[adc]>0):
                    samplesHG[md][adc][ncharge] = maxsamplehg[adc]-minsamplehg[adc]
                if(maxsamplelg[adc]-minsamplelg[adc]>0):
                    samplesLG[md][adc][ncharge] = maxsamplelg[adc]-minsamplelg[adc]
                    print ("MD", md, "channel", adc, "value", samplesLG[md][adc][ncharge])
    
    for md in range(firstMD,firstMD+nMD):
        ppr.AsyncWrite((md+1),0x1,0x80000000)
        ppr.AsyncWrite((md+1),0x121,0)
    ppr.ipbus.write("cfg2_dead_time",1)  
    return DAC_CIS_list, samplesLG, samplesHG