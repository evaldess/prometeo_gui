#
# Migrated from Demo scripts
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import numpy as np

import sys
sys.path.append('./common')
from UpdatePlot import UpdatePlot 
from ipbus import ipbus

def PedNoise(DACvalue,iterations):
    ppr = ipbus()
    
    ###################
    bcid_l1a = 3200
    ################################
    
    ###########  CHANGE HERE THE NUMBER OF MINIDRAWERS TO TEST  ########################
    nMD=4# Number of Minidrawers to be tested
    firstMD=0# .. starting from this MD ( MD goes from 0 to 3)
    ####################################################################################
    
    stableP=1650
    stableM=2530
    #### Range is 1 V (+/- 500 mV if pedestal at 0). StepDAC = 1000/nsteps

    #Disable TTC

    ppr.ipbus.write("cfg1", 0x0)
    #Disable Deadtime 
    ppr.ipbus.write("cfg2",0x0)
    ppr.ResetCounter
    
    #############
    samplesLG = np.zeros((4,12,16*iterations))
    samplesHG = np.zeros((4,12,16*iterations)) 

    chargeP=stableP+DACvalue
    chargeM=stableM-DACvalue

    CMD=0
    TPH=0 #0-pass through DB pulse, 1-open, 2-closed 
    TPL=0 #pass thru
    ITG=1 #enable
    S1=0
    S2=0
    S3=0
    S4=0
    TRG=1
    cmd=(CMD<<12)|(TPH<<10)|(TPL<<8)|(ITG<<7)|(S4<<6)|(S3<<5)|(S2<<4)|(S1<<3)|(TRG<<2)

    for md in range(firstMD,firstMD+nMD):
        for adc in range(12):
            FPGA = ((adc/6)<<1)+ (adc%2)
            card = (adc/2)%3
            
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x6000+int(chargeP))
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            
            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x7000+int(chargeM))
            ppr.AsyncWrite((md+1),0x1,0x8000000)

            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x4000+int(chargeP))
            ppr.AsyncWrite((md+1),0x1,0x8000000)

            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1, (1<<22)+(FPGA<<18)+(card<<16)+0x5000+int(chargeM))
            ppr.AsyncWrite((md+1),0x1,0x8000000)

            ppr.AsyncWrite((md+1),0x1,0x8000000)
            ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xc000) #loadLG
            ppr.AsyncWrite((md+1),0x1,0x80000000)
            ppr.AsyncWrite((md+1),0x1,(1<<22)+(FPGA<<18)+(card<<16)+0xd000) #loadHG
            ppr.AsyncWrite((md+1),0x1,0x80000000)
            
            ppr.AsyncWrite((md+1),0x1,(1<<22)|(FPGA<<18)|(card<<16)|cmd) #SWITCHES
            ppr.AsyncWrite((md+1),0x1,0x80000000)            
    print ("I arrived here")
    for i in range (0, iterations):
        if (i%100==0):
            print ("Number of iterations is:", i)
        ppr.SyncClear
        ppr.SyncRest
        ppr.SyncWrite(0,0,bcid_l1a, 1)
        ppr.SyncLoop(0)
        ppr.SyncRest
        ppr.SyncClear
#    
        for md in range(firstMD, firstMD+nMD):
            for adc in range(12):
                hg = ppr.ReadPipelines(md, adc, 1)
                lg = ppr.ReadPipelines(md, adc, 0)
                for r in range(len(hg)):
                    samplesLG[md][adc][16*i + r] = lg[r] & 0xfff
                    samplesHG[md][adc][16*i + r] = hg[r] & 0xfff

    ###Finally Read the L1ID to release the busy 
        ppr.ipbus.read("LastEventBCID")
        ppr.ipbus.read("LastEventL1ID")
    return (samplesLG, samplesHG)
#print (samplesLG[0][1:10])