#
# Sergio Landete
# Fernando Carrio Argos
# University of Valencia
# fernando.carrio@cern.ch
#
import Tkinter as tk
from matplotlib import style
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np

import sys
sys.path.append('./scripts')
from PedNoise import PedNoise
sys.path.append('./common')
from UpdatePlot import UpdatePlot 

class PedNoiseTab(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)

        self.configure(background="snow3")
        botonFrame=tk.Frame(self)
        botonFrame.configure(background="snow3")
        botonFrame.pack(side=tk.TOP)

        self.md=tk.IntVar()
        self.stateHGLG=tk.IntVar()

        buttonMD1 = tk.Radiobutton(botonFrame, text="MD1", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=0)
        buttonMD2 = tk.Radiobutton(botonFrame, text="MD2", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=1)
        buttonMD3 = tk.Radiobutton(botonFrame, text="MD3", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=2)
        buttonMD4 = tk.Radiobutton(botonFrame, text="MD4", indicatoron = 0, width = 2, padx = 10, variable=self.md, command=lambda:self.animate(), value=3)
        buttonMD1.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD2.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD3.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)
        buttonMD4.pack(padx=20, pady=10, ipadx=50, side=tk.LEFT)

        buttonHG = tk.Radiobutton(botonFrame, text="HG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=1)
        buttonLG = tk.Radiobutton(botonFrame, text="LG", indicatoron = 0, width = 2, padx = 10, variable=self.stateHGLG, command=lambda:self.animate(), value=0)
        buttonHG.config(activebackground='dark green', bg='dark green', fg='black')
        buttonLG.config(activebackground='dark red', bg='dark red', fg='black')
        buttonHG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)
        buttonLG.pack(padx=5, pady=10, ipadx=10, side=tk.LEFT)

        buttonRun=tk.Button(botonFrame, text="Run", command=lambda:self.runtest())
        buttonRun.pack(padx=20, pady=10, ipadx=20, side=tk.LEFT)

        self.iterations = 100
        style.use("ggplot")
        self.figure=Figure(figsize=(5,5), dpi=100)
        self.pedLG = np.zeros((4,12,16*self.iterations))
        self.pedHG = np.zeros((4,12,16*self.iterations))
        self.ax = 12*[0]
        for i in range(12):
            self.ax[i] = self.figure.add_subplot(2, 6, i+1)
        self.canvas=FigureCanvasTkAgg(self.figure, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH ,expand=True)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.animate()
                
    def runtest(self):
        print ("Test is running")
        DACvalue = 0
        self.pedLG, self.pedHG = PedNoise(DACvalue,self.iterations)
        self.animate()

    def animate(self):
        pedaux = np.zeros((12,16*self.iterations))
        hg = self.stateHGLG.get()
        md = self.md.get()
        if hg == 1:
            pedaux = self.pedHG[md]
            color = 'g'
        else:
            pedaux = self.pedLG[md]
            color = 'r'
        for i in range (12):
            self.ax[i].clear()
            n, bins, patches = self.ax[i].hist(pedaux[i], 20, facecolor=color, alpha=0.75)

        xlabel = 'ADC counts'
        ylabel = 'Frequency'
        UpdatePlot(self,self.md.get(),self.stateHGLG.get(),xlabel, ylabel)