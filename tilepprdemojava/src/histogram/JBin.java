/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package histogram;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Carlos Solans {@literal (solans@cern.ch)}
 */
public class JBin {

    protected Double lo;
    protected Double hi;
    protected Double x;
    protected Double y;
    protected Double w;
    protected Double y2;
    protected Double w2;
    protected Double n;

    /**
     * Constructor. Builds bin value from two inputs.
     *
     * @param low Input low Number object.
     * @param high Input high Number object.
     */
    public JBin(Number low, Number high) {
        lo = low.doubleValue();
        hi = high.doubleValue();
        x = (hi + lo) / 2.;
        clear();
    }

    /**
     *
     */
    public final void clear() {
        y = 0.;
        n = 0.;
        w = 0.;
        y2 = 0.;
        w2 = 0.;
    }

    public double getLow() {
        return lo;
    }

    public double getHigh() {
        return hi;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getY2() {
        return y2;
    }

    public double getValue() {
        return y;
    }

    public double getRms() {
        return Math.sqrt(y2);
    }

    public double getStdErr() {
        return Math.sqrt(y2 - y * y);
    }

    public void setValue(Number value) {
        y = value.doubleValue();
        n = 1.;
        w = 1.;
        y2 = y * y;
    }

    /**
     * To fill histogram bins with no weight.
     */
    public void count() {
        count(1.);
    }

    /**
     * To fill histogram values with weight.
     *
     * @param weight
     */
    public void count(Number weight) {
        y += weight.doubleValue();
        y2 += weight.doubleValue() * weight.doubleValue();
        w += weight.doubleValue();
        n++;
    }

    /**
     * To fill a profile histograms withoout weights.
     *
     * @param value Input Y-value.
     */
     public void addValue(Number value) {
        addValue(value, 1.);
    }

    /**
     * To fill a profile histograms with weights.
     *
     * @param value Input Y-value.
     * @param weight Input weight.
     */
    public void addValue(Number value, Number weight) {
        y = (y * w + value.doubleValue() * weight.doubleValue()) / (w + weight.doubleValue());
        y2 = (y2 * w + value.doubleValue() * value.doubleValue()
                * weight.doubleValue() * weight.doubleValue()) / (w + weight.doubleValue());
        w += weight.doubleValue();
        n++;
    }
}
