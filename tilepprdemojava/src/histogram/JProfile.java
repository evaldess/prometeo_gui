/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package histogram;

import com.xeiam.xchart.XChartPanel;
import com.xeiam.xchart.ChartBuilder;
import com.xeiam.xchart.StyleManager.ChartType;
import com.xeiam.xchart.StyleManager.ChartTheme;

import java.util.ArrayList;
import java.awt.BorderLayout;
import static java.awt.Color.blue;

public class JProfile extends JH1 {

    /**
     *
     * @param stitle
     * @param nbins
     * @param xlo
     * @param xhi
     */
    public JProfile(String stitle, Integer nbins, Integer xlo, Integer xhi) {
        bins = new ArrayList<>();
        x = new ArrayList<>();
        y = new ArrayList<>();
        e = new ArrayList<>();

        // Defines the Chart class object with the data.
        initJProfileHisto(stitle, nbins, xlo, xhi);
    }

    /**
     * This method is made private as called within constructor and want to
     * avoid overridable methods called within constructor.
     *
     * @param stitle Input String with the title of the histogram (encoded).
     * @param nbins Input number of bins.
     * @param xlo Input minimum bin value.
     * @param xhi Input maximum bin value.
     */
    private void initJProfileHisto(String stitle, Integer nbins, Integer xlo, Integer xhi) {
        // Sets JPanel layout.
        setLayout(new BorderLayout());

        chart = new ChartBuilder()
                .chartType(ChartType.Scatter)
                .theme(ChartTheme.Matlab)
                .width(400).height(300).build();

        // Manages all things related to styling of the Chart components.
        // Sets the chart legend visibility.
        chart.getStyleManager().setLegendVisible(false);

        // Sets the visibility of the gridlines on the plot area
        chart.getStyleManager().setPlotGridLinesVisible(true);

        // Set the plot area's grid lines color
        chart.getStyleManager().setPlotGridLinesColor(blue);

        // Sets the visibility of the ticks marks inside the plot area
        chart.getStyleManager().setAxisTicksMarksVisible(true);

        // Sets the visibility of the tick marks
        chart.getStyleManager().setAxisTicksVisible(true);

        // Fills ArrayList of JBin objects (bins).
        setBins(nbins, xlo, xhi);

        //
        chart.addSeries("data", x, y);

        // Sets title and labels of the histogram on the Chart class object.
        setTitle(stitle);

        // Defines instance of class XChartPanel.
        panel = new XChartPanel(chart);

        // Adds XChartPanel object (uses JPanel method).
        add(panel, BorderLayout.CENTER);
    }

    /**
     * Fills profile histogram.
     *
     * @param value input X-value.
     * @param yval input y value.
     * @param notify
     */
    @Override
    public void fill(Number value, Number yval, Boolean notify) {
        // Finds JBin object corresponding to input x value.
        JBin bin = findBin(value);

        bin.addValue(yval);

        if (notify) {
            update();
        }
    }

    /**
     * Finds JBin object corresponding to the input x value.
     *
     * @param value Input X-values where to find the bin.
     * @return
     */
    @Override
    public JBin findBin(Number value) {
        double dval = value.doubleValue();

        JBin foundBin;

        foundBin = bins.get(1);

        for (Integer i = 1; i < bins.size() - 1; i++) {
            if (dval >= bins.get(i).getLow() && dval < bins.get(i).getHigh()) {
                foundBin = bins.get(i);
                break;
            }
        }

        System.out.println("findBin: Xval: " + value
                + " Bin: " + lastBin.getX());

        return foundBin;
    }

    /**
     *
     * @param value
     * @return
     */
    public Integer findBinNumber(Number value) {
        double dval = value.doubleValue();
        Integer foundBinNumber = 0;

        for (Integer i = 1; i < bins.size() - 1; i++) {
            if (dval >= bins.get(i).getLow() && dval < bins.get(i).getHigh()) {
                foundBinNumber = i;
                break;
            }
        }

        return foundBinNumber;
    }

    /**
     *
     */
    @Override
    public void update() {
        if (bins.size() < 1) {
            return;
        }

        for (Integer i = 1; i < bins.size() - 1; i++) {
            JBin bin = bins.get(i);
            y.set(i - 1, bin.getY());
            e.set(i - 1, bin.getStdErr());
        }

        panel.updateSeries("data", x, y, e);
    }

    public void setYAxisMin(Integer min) {
        chart.getStyleManager().setYAxisMin(min);
    }

    public void setYAxisMinMax(Integer min, Integer max) {
        chart.getStyleManager().setYAxisMin(min);
        chart.getStyleManager().setYAxisMax(max);
    }

    public void setXAxisMinMax(Integer min, Integer max) {
        chart.getStyleManager().setXAxisMin(min);
        chart.getStyleManager().setXAxisMax(max);
    }

    public void setMarkerSize(Integer val) {
        chart.getStyleManager().setMarkerSize(val);
    }

    public void setXAxisTickMarkSpacing(Integer val) {
        chart.getStyleManager().setXAxisTickMarkSpacingHint(val);
    }
    
    public void getTicks() {
        System.out.println("P1 : "
                + chart.getStyleManager().getXAxisTickMarkSpacingHint()
                + " P2: " + chart.getStyleManager().getAxisTickLabelsFont());
    }
}
