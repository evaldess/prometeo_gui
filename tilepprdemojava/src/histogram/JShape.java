/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package histogram;

import com.xeiam.xchart.Chart;
import com.xeiam.xchart.ChartBuilder;
import com.xeiam.xchart.StyleManager.ChartTheme;
import com.xeiam.xchart.StyleManager.ChartType;
import com.xeiam.xchart.XChartPanel;

import java.awt.BorderLayout;
import static java.awt.Color.blue;
import java.util.ArrayList;
import javax.swing.JPanel;
import tilepprdemo.Console;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class JShape extends JPanel {

    /**
     * Declares instance of class Chart. Thisn is an XChart Chart.
     */
    private Chart chart;
    /**
     * Declares instance of class XChartPanel. This is a Swing JPanel that
     * contains a Chart.
     */
    private XChartPanel panel;

    private ArrayList<Double> x;
    private ArrayList<Double> y;
    private ArrayList<Double> e;

    /**
     *
     * @param stitle
     * @param npoints
     * @param xval
     */
    public JShape(String stitle, Integer npoints, Float xval[]) {
        x = new ArrayList<>();
        y = new ArrayList<>();
        e = new ArrayList<>();

        // Defines the Chart class object with the data.
        initJShape(stitle, npoints, xval);
    }

    /**
     * This method is made private as called within constructor and want to
     * avoid overridable methods called within constructor.
     *
     * @param stitle Input String with the title of the scan (encoded).
     * @param npoints
     * @param xval
     */
    private void initJShape(String stitle, Integer npoints, Float xval[]) {
        // Sets JPanel layout.
        setLayout(new BorderLayout());

        // Defines Chart class object, an instance of class ChartBuilder.
        chart = new ChartBuilder()
                .chartType(ChartType.Scatter)
                .theme(ChartTheme.Matlab)
                .width(400).height(300).build();

        // Manages all things related to styling of the Chart components.
        // Sets the chart legend visibility.
        chart.getStyleManager().setLegendVisible(false);

        // Sets the visibility of the gridlines on the plot area
        chart.getStyleManager().setPlotGridLinesVisible(true);

        // Set the plot area's grid lines color
        chart.getStyleManager().setPlotGridLinesColor(blue);

        // Sets the visibility of the ticks marks inside the plot area
        chart.getStyleManager().setAxisTicksMarksVisible(true);

        // Sets the visibility of the tick marks
        chart.getStyleManager().setAxisTicksVisible(true);

        // Fills ArrayList of scan points.
        setData(npoints, xval);

        // Add a series to the chart using double arrays
        chart.addSeries("data", x, y, e);

        // Sets title and labels of the histogram on the Chart class object.
        setTitle(stitle);

        // Defines instance of class XChartPanel.
        panel = new XChartPanel(chart);

        // Adds XChartPanel object (uses JPanel method).
        add(panel, BorderLayout.CENTER);
    }

    /**
     * Sets title and X and Y labels of the series scan on the Chart class
     * object.
     *
     * @param stitle Input String title.
     */
    public void setTitle(String stitle) {
        String[] t = stitle.replace(";", ":").split(":");

        String title = t[0];

        // Sets x-label if available.
        String xlabel = "";
        if (t.length > 1) {
            xlabel = t[1];
        }

        String ylabel = "";
        if (t.length > 2) {
            ylabel = t[2];
        }

        chart.setChartTitle(title);
        chart.setXAxisTitle(xlabel);
        chart.setYAxisTitle(ylabel);
    }

    /**
     * Define JScan object with number of scan points and length between points.
     *
     * @param npoints Input number of scan steps.
     * @param xval
     */
    public void setData(Integer npoints, Float xval[]) {
        // Clear data ArrayLists.
        x.clear();
        y.clear();
        e.clear();

        // Fills ArrayLists with X scan point values.
        for (Integer i = 0; i < npoints; i++) {
            x.add(xval[i].doubleValue());
            y.add(0d);
            e.add(0d);
        }
    }

    /**
     *
     * @param xval
     * @param yval
     */
    public void fill(Integer xval, Integer yval) {
        y.set(xval, yval.doubleValue());

        // Update scan point series.
        update();
    }

    /**
     * Update scan point series.
     */
    public void draw() {
        update();
    }

    /**
     * Update the scan series of points on the XChartPanel (the Java Swing
     * JPanel that contains a Chart).
     */
    public void update() {
        panel.updateSeries("data", x, y, e);
    }

    /**
     *
     */
    public void resetContents() {
        for (Integer i = 0; i < x.size(); i++) {
            y.set(i, 0d);
            e.set(i, 0d);
        }

        // Update scan point series.
        update();
    }

    /**
     *
     */
    public void getBins() {
        String fs;
        fs = "Total points: %d ";
        Console.print_cr(String.format(fs, x.size()));
        for (Integer i = 0; i < x.size(); i++) {
            fs = " points: %d %.1f";
            Console.print_cr(String.format(fs, i, x.get(i)));
        }
    }

    /**
     *
     * @param val
     */
    public void setMarkerSize(Integer val) {
        chart.getStyleManager().setMarkerSize(val);
    }

    /**
     *
     * @param val
     */
    public void setXAxisTickMarkSpacing(Integer val) {
        chart.getStyleManager().setXAxisTickMarkSpacingHint(val);
    }

    /**
     *
     * @param min
     * @param max
     */
    public void setYAxisMinMax(Integer min, Integer max) {
        chart.getStyleManager().setYAxisMin(min);
        chart.getStyleManager().setYAxisMax(max);
    }

    /**
     *
     * @param min
     * @param max
     */
    public void setXAxisMinMax(Float min, Float max) {
        chart.getStyleManager().setXAxisMin(min);
        chart.getStyleManager().setXAxisMax(max);
    }
}
