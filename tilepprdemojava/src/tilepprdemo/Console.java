/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

/**
 * Printing control on the MainFrame JTextArea TextAreaMessages.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class Console {

    /**
     * Prints an input string into the MainFrame JTextArea TextAreaMessages
     * followed by carriage return.
     *
     * @param s Input string.
     */
    public static void print_cr(String s) {
        MainFrame.getTextAreaConsole().append(s + "\n");
    }

    /**
     * Prints an input string into the MainFrame JTextArea TextAreaMessages with
     * no carriage return.
     *
     * @param s Input string.
     */
    public static void print_nr(String s) {
        MainFrame.getTextAreaConsole().append(s);
    }
}
