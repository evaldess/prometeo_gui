/* 
 *  PPr Test Bench
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

/**
 * Handles debug flags.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class DebugSettings {

    /**
     * Declares and sets default values for IPbus library debug flag.
     */
    private static Boolean debugIPbusLibrary = false;

    /**
     * Declares and sets default values for PPr library debug flag.
     */
    private static Boolean debugPPrLibrary = false;

    /**
     * Declares and sets default values for the verbose cosole flag.
     */
    private static Boolean verbose = false;

    /**
     * Getter for the IPbus library debug flag.
     *
     * @return the IPbus library debug flag.
     */
    public static Boolean getDebugIPbusLibrary() {
        return debugIPbusLibrary;
    }

    /**
     * Getter for the PPr library debug flag.
     *
     * @return the PPr library debug flag.
     */
    public static Boolean getDebugPPrLibrary() {
        return debugPPrLibrary;
    }

    /**
     * Getter for the verbose flag.
     *
     * @return the verbose flag.
     */
    public static Boolean getVerbose() {
        return verbose;
    }

    /**
     * Setter for the IPbus library debug flag.
     *
     * @param val Input Boolean with the IPbus library debug flag.
     */
    public static void setDebugIPbusLibrary(Boolean val) {
        debugIPbusLibrary = val;
    }

    /**
     * Setter for the PPr library debug flag.
     *
     * @param val Input Boolean with the PPr library debug flag.
     */
    public static void setDebugPPrLibrary(Boolean val) {
        debugPPrLibrary = val;
    }

    /**
     * Setter for the verbose flag.
     *
     * @param val Input Boolean with the verbose flag.
     */
    public static void setVerbose(Boolean val) {
        verbose = val;
    }
}
