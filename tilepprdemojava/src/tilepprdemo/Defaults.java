/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class Defaults {

    private static SortedProperties prop;

    public static File propsFile;

    public static InputStream inputpropsFile = null;

    public static OutputStream outputpropsFile = null;

    /**
     *
     */
    public Defaults() {
        System.out.println("Defaults -> constructor");

        // Defines new Properties object.
        prop = new SortedProperties();

        // Defines new File object with properties file.
        propsFile = new File("tilepprdemo.properties");
    }

    public static void getProperties() {
        getIPbusProperties();
        getPedestalProperties();
        getADCLinearityProperties();
        getCISLinearityProperties();
        getCISShapeProperties();
    }

    public static void getIPbusProperties() {
        String hosts = prop.getProperty("IPbus.hosts");
        List<String> list_hosts = new ArrayList<>(Arrays.asList(hosts.split(",")));
        IPbusPanel.hostsStrArr.clear();
        for (Integer i = 0; i < list_hosts.size(); i++) {
            IPbusPanel.hostsStrArr.add(list_hosts.get(i));
        }

        String ports = prop.getProperty("IPbus.ports");
        List<String> list_ports = new ArrayList<>(Arrays.asList(ports.split(",")));
        IPbusPanel.portsStrArr.clear();
        for (Integer i = 0; i < list_ports.size(); i++) {
            IPbusPanel.portsStrArr.add(list_ports.get(i));
        }

        String hostsPPr = prop.getProperty("IPbus.hostsPPr");
        List<String> list_hostsPPr = new ArrayList<>(Arrays.asList(hostsPPr.split(",")));
        IPbusPanel.hostsPPrStrArr.clear();
        for (Integer i = 0; i < list_hostsPPr.size(); i++) {
            IPbusPanel.hostsPPrStrArr.add(list_hostsPPr.get(i));
        }

        String portsPPr = prop.getProperty("IPbus.portsPPr");
        List<String> list_portsPPr = new ArrayList<>(Arrays.asList(portsPPr.split(",")));
        IPbusPanel.portsPPrStrArr.clear();
        for (Integer i = 0; i < list_portsPPr.size(); i++) {
            IPbusPanel.portsPPrStrArr.add(list_portsPPr.get(i));
        }

        IPbusPanel.protocol = prop.getProperty("IPbus.protocol");
        IPbusPanel.host = prop.getProperty("IPbus.host");
        IPbusPanel.port = prop.getProperty("IPbus.port");
        IPbusPanel.hostPPr = prop.getProperty("IPbus.hostPPr");
        IPbusPanel.portPPr = prop.getProperty("IPbus.portPPr");
    }

    public static void getPedestalProperties() {
        STPedestalPanel.dbsidestring = prop.getProperty("RunPed.DBsideFPGA");
        STPedestalPanel.MDbroadcast = Integer.valueOf(prop.getProperty("RunPed.MDbroadcast"), 10);
        String list = prop.getProperty("RunPed.MDsel");
        List<String> lost = new ArrayList<>(Arrays.asList(list.split(",")));
        STPedestalPanel.MDsel.set(0, Integer.valueOf(lost.get(0)));
        STPedestalPanel.MDsel.set(1, Integer.valueOf(lost.get(1)));
        STPedestalPanel.MDsel.set(2, Integer.valueOf(lost.get(2)));
        STPedestalPanel.MDsel.set(3, Integer.valueOf(lost.get(3)));
        STPedestalPanel.totEvents = Integer.valueOf(prop.getProperty("RunPed.events"), 10);
        STPedestalPanel.BCID = Integer.valueOf(prop.getProperty("RunPed.BCID"), 10);
        STPedestalPanel.samples = Integer.valueOf(prop.getProperty("RunPed.samples"), 10);
        STPedestalPanel.DACbiasP = Integer.valueOf(prop.getProperty("RunPed.DACbiasP"), 10);
        STPedestalPanel.DACbiasN = Integer.valueOf(prop.getProperty("RunPed.DACbiasN"), 10);
        STPedestalPanel.ADCped = Integer.valueOf(prop.getProperty("RunPed.ADCped"), 10);
        STPedestalPanel.setPed = prop.getProperty("RunPed.setPed");
        STPedestalPanel.hnbins = Integer.valueOf(prop.getProperty("RunPed.hnbins"), 10);
        STPedestalPanel.hxlow = Integer.valueOf(prop.getProperty("RunPed.hxlow"), 10);
        STPedestalPanel.hxhigh = Integer.valueOf(prop.getProperty("RunPed.hxhigh"), 10);
        STPedestalPanel.smartBinning = Boolean.parseBoolean(prop.getProperty("RunPed.smartBinning"));
    }

    public static void getADCLinearityProperties() {
        STADCLinearityPanel.dbsidestring = prop.getProperty("ADCLin.DBsideFPGA");
        STADCLinearityPanel.MDbroadcast = Integer.valueOf(prop.getProperty("ADCLin.MDbroadcast"), 10);
        String list = prop.getProperty("ADCLin.MDsel");
        List<String> lost = new ArrayList<>(Arrays.asList(list.split(",")));
        STADCLinearityPanel.MDsel.set(0, Integer.valueOf(lost.get(0)));
        STADCLinearityPanel.MDsel.set(1, Integer.valueOf(lost.get(1)));
        STADCLinearityPanel.MDsel.set(2, Integer.valueOf(lost.get(2)));
        STADCLinearityPanel.MDsel.set(3, Integer.valueOf(lost.get(3)));
        STADCLinearityPanel.BCID = Integer.valueOf(prop.getProperty("ADCLin.BCID"), 10);
        STADCLinearityPanel.samples = Integer.valueOf(prop.getProperty("ADCLin.samples"), 10);
        STADCLinearityPanel.nbSteps = Integer.valueOf(prop.getProperty("ADCLin.nbSteps"), 10);
        STADCLinearityPanel.stepEvents = Integer.valueOf(prop.getProperty("ADCLin.stepEvents"), 10);
        STADCLinearityPanel.scanUnits = prop.getProperty("ADCLin.scanUnits");
    }

    public static void getCISLinearityProperties() {
        STCISLinearityPanel.dbsidestring = prop.getProperty("CISLin.DBsideFPGA");
        STCISLinearityPanel.MDbroadcast = Integer.valueOf(prop.getProperty("CISLin.MDbroadcast"), 10);
        String list = prop.getProperty("CISLin.MDsel");
        List<String> lost = new ArrayList<>(Arrays.asList(list.split(",")));
        STCISLinearityPanel.MDsel.set(0, Integer.valueOf(lost.get(0)));
        STCISLinearityPanel.MDsel.set(1, Integer.valueOf(lost.get(1)));
        STCISLinearityPanel.MDsel.set(2, Integer.valueOf(lost.get(2)));
        STCISLinearityPanel.MDsel.set(3, Integer.valueOf(lost.get(3)));
        STCISLinearityPanel.gain = Integer.valueOf(prop.getProperty("CISLin.gain"), 10);
        STCISLinearityPanel.BCID_charge = Integer.valueOf(prop.getProperty("CISLin.BCID_charge"), 10);
        STCISLinearityPanel.BCID_discharge = Integer.valueOf(prop.getProperty("CISLin.BCID_discharge"), 10);
        STCISLinearityPanel.BCID = Integer.valueOf(prop.getProperty("CISLin.BCID"), 10);
        STCISLinearityPanel.nbSteps = Integer.valueOf(prop.getProperty("CISLin.nbSteps"), 10);
        STCISLinearityPanel.stepEvents = Integer.valueOf(prop.getProperty("CISLin.stepEvents"), 10);
        STCISLinearityPanel.scanUnits = prop.getProperty("CISLin.scanUnits");
        STCISLinearityPanel.setPed = prop.getProperty("CISLin.setPed");
        STCISLinearityPanel.DACbiasP = Integer.valueOf(prop.getProperty("CISLin.DACbiasP"), 10);
        STCISLinearityPanel.DACbiasN = Integer.valueOf(prop.getProperty("CISLin.DACbiasN"), 10);
        STCISLinearityPanel.ADCped = Integer.valueOf(prop.getProperty("CISLin.ADCped"), 10);
    }

    public static void getCISShapeProperties() {
        STCISShapePanel.dbsidestring = prop.getProperty("CISShape.DBsideFPGA");
        STCISShapePanel.MDbroadcast = Integer.valueOf(prop.getProperty("CISShape.MDbroadcast"), 10);
        String list = prop.getProperty("CISShape.MDsel");
        List<String> lost = new ArrayList<>(Arrays.asList(list.split(",")));
        STCISShapePanel.MDsel.set(0, Integer.valueOf(lost.get(0)));
        STCISShapePanel.MDsel.set(1, Integer.valueOf(lost.get(1)));
        STCISShapePanel.MDsel.set(2, Integer.valueOf(lost.get(2)));
        STCISShapePanel.MDsel.set(3, Integer.valueOf(lost.get(3)));
        STCISShapePanel.gain = Integer.valueOf(prop.getProperty("CISShape.gain"), 10);
        STCISShapePanel.BCID_charge = Integer.valueOf(prop.getProperty("CISShape.BCID_charge"), 10);
        STCISShapePanel.BCID_discharge = Integer.valueOf(prop.getProperty("CISShape.BCID_discharge"), 10);
        STCISShapePanel.BCID = Integer.valueOf(prop.getProperty("CISShape.BCID"), 10);
        STCISShapePanel.setPed = prop.getProperty("CISShape.setPed");
        STCISShapePanel.DACbiasP = Integer.valueOf(prop.getProperty("CISShape.DACbiasP"), 10);
        STCISShapePanel.DACbiasN = Integer.valueOf(prop.getProperty("CISShape.DACbiasN"), 10);
        STCISShapePanel.ADCped = Integer.valueOf(prop.getProperty("CISShape.ADCped"), 10);
        STCISShapePanel.DACcharge = Integer.valueOf(prop.getProperty("CISShape.DACcharge"), 10);
    }

    /**
     * Prints key and value from properties file (Java 8).
     */
    public static void showProperties() {
        System.out.println("Defaults.showProperties() -> properties contents:");

        @SuppressWarnings("unchecked")
        Map<String, String> sortedMap = new TreeMap(prop);

        // Output sorted properties (key=value)
        sortedMap.keySet().forEach((key) -> {
            Console.print_cr(key + "=" + sortedMap.get(key));
        });
    }

    /**
     * Create properties file. First set values and then store them in a
     * properties file.
     */
    public static void createProperties() {
        System.out.println("Defaults.createProperties -> creating properties file: " + propsFile);

        // Set properties file values.
        setProperties();

        storeProperties();
    }

    /**
     * Set properties file values from classes fields.
     */
    public static void setProperties() {
        setIPbusProperties();
        setPedestalProperties();
        setADCLinearityProperties();
        setCISLinearityProperties();
        setCISShapeProperties();
    }

    /**
     * Set properties file values from IPbusPanel fields.
     */
    public static void setIPbusProperties() {
        String hosts = "";
        String ports = "";
        String hostsPPr = "";
        String portsPPr = "";

        for (Integer i = 0; i < IPbusPanel.hostsStrArr.size(); i++) {
            hosts = hosts + IPbusPanel.hostsStrArr.get(i);
            if (i == IPbusPanel.hostsStrArr.size() - 1) {
                break;
            }
            hosts = hosts + ",";
        }
        prop.setProperty("IPbus.hosts", hosts);

        for (Integer i = 0; i < IPbusPanel.portsStrArr.size(); i++) {
            ports = ports + IPbusPanel.portsStrArr.get(i);
            if (i == IPbusPanel.portsStrArr.size() - 1) {
                break;
            }
            ports = ports + ",";
        }
        prop.setProperty("IPbus.ports", ports);

        for (Integer i = 0; i < IPbusPanel.hostsPPrStrArr.size(); i++) {
            hostsPPr = hostsPPr + IPbusPanel.hostsPPrStrArr.get(i);
            if (i == IPbusPanel.hostsPPrStrArr.size() - 1) {
                break;
            }
            hostsPPr = hostsPPr + ",";
        }
        prop.setProperty("IPbus.hostsPPr", hostsPPr);

        for (Integer i = 0; i < IPbusPanel.portsPPrStrArr.size(); i++) {
            portsPPr = portsPPr + IPbusPanel.portsPPrStrArr.get(i);
            if (i == IPbusPanel.portsPPrStrArr.size() - 1) {
                break;
            }
            portsPPr = portsPPr + ",";
        }
        prop.setProperty("IPbus.portsPPr", portsPPr);

        prop.setProperty("IPbus.protocol", IPbusPanel.protocol);
        prop.setProperty("IPbus.host", IPbusPanel.host);
        prop.setProperty("IPbus.port", IPbusPanel.port);
        prop.setProperty("IPbus.hostPPr", IPbusPanel.hostPPr);
        prop.setProperty("IPbus.portPPr", IPbusPanel.portPPr);
    }

    /**
     * Set properties file values from STPedestalPanel fields.
     */
    public static void setPedestalProperties() {
        prop.setProperty("RunPed.DBsideFPGA", STPedestalPanel.dbsidestring);
        prop.setProperty("RunPed.MDbroadcast", STPedestalPanel.MDbroadcast.toString());
        prop.setProperty("RunPed.MDsel", STPedestalPanel.MDsel.get(0).toString()
                + "," + STPedestalPanel.MDsel.get(1).toString()
                + "," + STPedestalPanel.MDsel.get(2).toString()
                + "," + STPedestalPanel.MDsel.get(3).toString());
        prop.setProperty("RunPed.events", STPedestalPanel.totEvents.toString());
        prop.setProperty("RunPed.BCID", STPedestalPanel.BCID.toString());
        prop.setProperty("RunPed.samples", STPedestalPanel.samples.toString());
        prop.setProperty("RunPed.DACbiasP", STPedestalPanel.DACbiasP.toString());
        prop.setProperty("RunPed.DACbiasN", STPedestalPanel.DACbiasN.toString());
        prop.setProperty("RunPed.ADCped", STPedestalPanel.ADCped.toString());
        prop.setProperty("RunPed.setPed", STPedestalPanel.setPed);
        prop.setProperty("RunPed.hnbins", STPedestalPanel.hnbins.toString());
        prop.setProperty("RunPed.hxlow", STPedestalPanel.hxlow.toString());
        prop.setProperty("RunPed.hxhigh", STPedestalPanel.hxhigh.toString());
        prop.setProperty("RunPed.smartBinning", STPedestalPanel.smartBinning.toString());
    }

    /**
     * Set properties file values from STADCLinearityPanel fields.
     */
    public static void setADCLinearityProperties() {
        prop.setProperty("ADCLin.DBsideFPGA", STADCLinearityPanel.dbsidestring);
        prop.setProperty("ADCLin.MDbroadcast", STADCLinearityPanel.MDbroadcast.toString());
        prop.setProperty("ADCLin.MDsel", STADCLinearityPanel.MDsel.get(0).toString()
                + "," + STADCLinearityPanel.MDsel.get(1).toString()
                + "," + STADCLinearityPanel.MDsel.get(2).toString()
                + "," + STADCLinearityPanel.MDsel.get(3).toString());
        prop.setProperty("ADCLin.BCID", STADCLinearityPanel.BCID.toString());
        prop.setProperty("ADCLin.samples", STADCLinearityPanel.samples.toString());
        prop.setProperty("ADCLin.nbSteps", STADCLinearityPanel.nbSteps.toString());
        prop.setProperty("ADCLin.stepEvents", STADCLinearityPanel.stepEvents.toString());
        prop.setProperty("ADCLin.scanUnits", STADCLinearityPanel.scanUnits);
    }

    /**
     * Set properties file values from STCISLinearityPanel fields.
     */
    public static void setCISLinearityProperties() {
        prop.setProperty("CISLin.DBsideFPGA", STCISLinearityPanel.dbsidestring);
        prop.setProperty("CISLin.MDbroadcast", STCISLinearityPanel.MDbroadcast.toString());
        prop.setProperty("CISLin.MDsel", STCISLinearityPanel.MDsel.get(0).toString()
                + "," + STCISLinearityPanel.MDsel.get(1).toString()
                + "," + STCISLinearityPanel.MDsel.get(2).toString()
                + "," + STCISLinearityPanel.MDsel.get(3).toString());
        prop.setProperty("CISLin.gain", STCISLinearityPanel.gain.toString());
        prop.setProperty("CISLin.BCID_charge", STCISLinearityPanel.BCID_charge.toString());
        prop.setProperty("CISLin.BCID_discharge", STCISLinearityPanel.BCID_discharge.toString());
        prop.setProperty("CISLin.BCID", STCISLinearityPanel.BCID.toString());
        prop.setProperty("CISLin.nbSteps", STCISLinearityPanel.nbSteps.toString());
        prop.setProperty("CISLin.stepEvents", STCISLinearityPanel.stepEvents.toString());
        prop.setProperty("CISLin.scanUnits", STCISLinearityPanel.scanUnits);
        prop.setProperty("CISLin.setPed", STCISLinearityPanel.setPed);
        prop.setProperty("CISLin.DACbiasP", STCISLinearityPanel.DACbiasP.toString());
        prop.setProperty("CISLin.DACbiasN", STCISLinearityPanel.DACbiasN.toString());
        prop.setProperty("CISLin.ADCped", STCISLinearityPanel.ADCped.toString());
    }

    /**
     * Set properties file values from STCISShapePanel fields.
     */
    public static void setCISShapeProperties() {
        prop.setProperty("CISShape.DBsideFPGA", STCISShapePanel.dbsidestring);
        prop.setProperty("CISShape.MDbroadcast", STCISShapePanel.MDbroadcast.toString());
        prop.setProperty("CISShape.MDsel", STCISShapePanel.MDsel.get(0).toString()
                + "," + STCISShapePanel.MDsel.get(1).toString()
                + "," + STCISShapePanel.MDsel.get(2).toString()
                + "," + STCISShapePanel.MDsel.get(3).toString());
        prop.setProperty("CISShape.gain", STCISShapePanel.gain.toString());
        prop.setProperty("CISShape.BCID_charge", STCISShapePanel.BCID_charge.toString());
        prop.setProperty("CISShape.BCID_discharge", STCISShapePanel.BCID_discharge.toString());
        prop.setProperty("CISShape.BCID", STCISShapePanel.BCID.toString());
        prop.setProperty("CISShape.setPed", STCISShapePanel.setPed);
        prop.setProperty("CISShape.DACbiasP", STCISShapePanel.DACbiasP.toString());
        prop.setProperty("CISShape.DACbiasN", STCISShapePanel.DACbiasN.toString());
        prop.setProperty("CISShape.ADCped", STCISShapePanel.ADCped.toString());
        prop.setProperty("CISShape.DACcharge", STCISShapePanel.DACcharge.toString());
    }

    /**
     * Loads IPbus field values from properties file.
     */
    public static void loadIPbusProperties() {
        if (!propsFile.exists()) {
            Console.print_cr("Defaults.loadIPbusProperties() -> properties file does not exist: " + propsFile);
            Console.print_cr("Defaults.loadIPbusProperties() -> creating one with defaults");
            createProperties();
            return;
        }

        loadProperties();

        getIPbusProperties();
    }

    /**
     * Loads pedestal run field values from properties file.
     */
    public static void loadPedestalProperties() {
        if (!propsFile.exists()) {
            Console.print_cr("Defaults.loadPedestalProperties() -> properties file does not exist: " + propsFile);
            Console.print_cr("Defaults.loadPedestalProperties() -> creating one with defaults");
            createProperties();
            return;
        }

        loadProperties();

        getPedestalProperties();
    }

    /**
     * Loads ADC linearity scan field values from properties file.
     */
    public static void loadADCLinearityProperties() {
        if (!propsFile.exists()) {
            Console.print_cr("Defaults.loadADCLinearityProperties() -> properties file does not exist: " + propsFile);
            Console.print_cr("Defaults.loadADCLinearityProperties() -> creating one with defaults");
            createProperties();
            return;
        }

        loadProperties();

        getADCLinearityProperties();
    }

    /**
     * Load properties file and get CIS linearity scan field values.
     */
    public static void loadCISLinearityProperties() {
        if (!propsFile.exists()) {
            Console.print_cr("Defaults.loadCISLinearityProperties() -> properties file does not exist: " + propsFile);
            Console.print_cr("Defaults.loadCISLinearityProperties() -> creating one with defaults");
            createProperties();
            return;
        }

        loadProperties();

        getCISLinearityProperties();
    }

    /**
     * Load properties file and get CIS shape scan field values.
     */
    public static void loadCISShapeProperties() {
        if (!propsFile.exists()) {
            Console.print_cr("Defaults.loadCISShapeProperties() -> properties file does not exist: " + propsFile);
            Console.print_cr("Defaults.loadCISShapeProperties() -> creating one with defaults");
            createProperties();
            return;
        }

        loadProperties();

        getCISShapeProperties();
    }

    /**
     *
     */
    public static void loadProperties() {
        System.out.println("Defaults.loadProperties() -> ");
        System.out.println("  properties found: " + propsFile);
        System.out.println("  absolute path: " + propsFile.getAbsolutePath());

        try {
            inputpropsFile = new FileInputStream(propsFile);
        } catch (FileNotFoundException ex) {
            System.out.println("Defaults.loadProperties() -> FileNotFoundException: " + ex);
        }

        try {
            prop.load(inputpropsFile);
            System.out.println("  file " + propsFile + " loaded");
        } catch (IOException ex) {
            System.out.println("Defaults.loadProperties() -> IOException while loading InputStream" + ex);
        }

        try {
            inputpropsFile.close();
        } catch (IOException e) {
            System.out.println("Defaults.loadProperties() -> IOException while closing InputStream: " + e);
        }
    }

    /**
     * Saves IPbus field values into properties file.
     */
    public static void saveIPbusProperties() {
        setIPbusProperties();
        storeProperties();
    }

    /**
     *
     */
    public static void savePedestalRunProperties() {
        setPedestalProperties();
        storeProperties();
    }

    /**
     * Saves ADC linearity scan field values into properties file.
     */
    public static void saveADCLinearityScanProperties() {
        setADCLinearityProperties();
        storeProperties();
    }

    /**
     * Saves CIS linearity scan field values into properties file.
     */
    public static void saveCISLinearityScanProperties() {
        setCISLinearityProperties();
        storeProperties();
    }

    /**
     * Saves CIS shape scan field values into properties file.
     */
    public static void saveCISShapeScanProperties() {
        setCISShapeProperties();
        storeProperties();
    }

    /**
     *
     */
    public static void storeProperties() {
        // Defines new FileOutputStream object with properties file.
        try {
            outputpropsFile = new FileOutputStream(propsFile);
        } catch (FileNotFoundException ex) {
            System.out.println("Defaults.storeProperties() -> -> IOException while defining OutputStream: " + ex);
        }

        // Save properties to project root folder
        try {
            prop.store(outputpropsFile, null);
        } catch (IOException ex) {
            System.out.println("Defaults.storeProperties() -> IOException while storing properties file: " + ex);
        }

        try {
            outputpropsFile.close();
        } catch (IOException e) {
            System.out.println("Defaults.storeProperties() -> IOException while closing OutputStream: " + e);
        }
    }
}

class SortedProperties extends Properties {

    @Override
    public void store(OutputStream out, String comments) throws IOException {
        Properties sortedProps = new Properties() {
            @Override
            public Set<Map.Entry<Object, Object>> entrySet() {
                /*
                     * Using comparator to avoid the following exception on jdk >=9: 
                     * java.lang.ClassCastException: java.base/java.util.concurrent.ConcurrentHashMap$MapEntry cannot be cast to java.base/java.lang.Comparable
                 */
                Set<Map.Entry<Object, Object>> sortedSet = new TreeSet<>(new Comparator<Map.Entry<Object, Object>>() {
                    @Override
                    public int compare(Map.Entry<Object, Object> o1, Map.Entry<Object, Object> o2) {
                        return o1.getKey().toString().compareTo(o2.getKey().toString());
                    }
                }
                );
                sortedSet.addAll(super.entrySet());
                return sortedSet;
            }

            @Override
            public Set<Object> keySet() {
                return new TreeSet<>(super.keySet());
            }

            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<>(super.keySet()));
            }
        };
        sortedProps.putAll(this);
        sortedProps.store(out, comments);
    }
}
