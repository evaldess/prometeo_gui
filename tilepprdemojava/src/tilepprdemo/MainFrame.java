
/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import javax.swing.text.DefaultCaret;

import tilepprjavalibrary.lib.PPrLib;

/**
 * Class with the main() method.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Constructor. Creates new form PPrTBMainFrame
     */
    public MainFrame() {
        System.out.println("MainFrame -> constructor");

        // NetBeans components settings.
        initComponents();

        // NetBeans components modifications.
        initModifyComponents();

        // Defines Properties object.
        defaults = new Defaults();

        pprlib = new PPrLib();

        // Defines instance of IPbusPanel class
        pIPbusPanel = new IPbusPanel(pprlib);

        // Defines instance of PPrDemoPanel class
        pPPrDemoPanel = new PPrDemoPanel();

        // Defines instance of PPrDemoGlobal class
        pPPrDemoGlobal = new PPrDemoGlobal(pprlib);

        // Defines instance of PPrDemoLinksCtrl class
        pPPrDemoLinksCtrl = new PPrDemoLinksCtrl(pprlib);

        // Defines instance of PPrDemoLinksStatus class
        pPPrDemoLinksStatus = new PPrDemoLinksStatus(pprlib);

        // Defines instance of PPrDemoCRCCounters class
        pPPrDemoCRCCounters = new PPrDemoCRCCounters(pprlib);

        // Defines instance of PPrDemoCRCChecks class
        pPPrDemoCRCChecks = new PPrDemoCRCChecks(pprlib);

        // Defines instance of PPrDemoDB class
        pPPrDemoDB = new PPrDemoDB(pprlib);

        // Defines instance of PPrDemoFE class
        pPPrDemoFE = new PPrDemoFE(pprlib);

        // Defines instance of STPanel class
        pSTPanel = new STPanel();

        // Defines instance of STPedestalPanel class
        pSTPedestalPanel = new STPedestalPanel(pprlib);

        // Defines instance of STADCLinearity class
        pSTADCLinearityPanel = new STADCLinearityPanel(pprlib);

        // Defines instance of STCISLinearity class
        pSTCISLinearityPanel = new STCISLinearityPanel(pprlib);

        // Defines instance of STCISLinearity class
        pSTCISShapePanel = new STCISShapePanel(pprlib);

        // Defines instance of DIagnostics class.
        pDiagnostics = new Diagnostics(pprlib);

        // Defines instance of Credits class.
        pCredits = new Credits();

        if (Defaults.propsFile.exists()) {
            System.out.println("MainFrame -> loading properties file.");

            Defaults.loadProperties();

            Defaults.getProperties();

            IPbusPanel.set_all_parameters();
            STPedestalPanel.set_all_parameters();
            STADCLinearityPanel.set_all_parameters();
            STCISLinearityPanel.set_all_parameters();
            STCISShapePanel.set_all_parameters();
        } else {
            System.out.println("MainFrame -> no properties file found.");

            // Create properties file. First set values from fields (at this point
            // NetBeans default fields) and then store them in the properties file.
            Defaults.createProperties();
        }

        STPedestalPanel.plot_BookHistograms();
        STADCLinearityPanel.plot_BookHistograms();
        STCISLinearityPanel.plot_BookHistograms();
        STCISShapePanel.plot_BookHistograms();

        // Populate frame with tabs panels.
        populateFrameTabs();

        // Initializes label for MainFrame IPbus connection status.
        LabelConnectMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame Links Status run status.
        LabelLinksStatusMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame CRC Checks run status.
        LabelCRCChecksMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame pedestal run status.
        LabelPedestalRunMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame ADC linearity run status.
        LabelADCLinearityRunMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame CIS linearity run status.
        LabelCISLinearityRunMainFrame.setForeground(Color.RED);

        // Initializes label for MainFrame CIS shape run status.
        LabelCISShapeRunMainFrame.setForeground(Color.RED);

        // Sets default verbose state for the application console.
        CheckBoxMenuItemVerbose.setState(DebugSettings.getVerbose());

        // Sets default debug flag for the IPbusJavaLibrary.
        CheckBoxMenuItemDebugIPbus.setState(DebugSettings.getDebugIPbusLibrary());
        ipbusjavalibrary.ipbus.Debug.setDebugIPbus(DebugSettings.getDebugIPbusLibrary());

        // Sets default debug flag for the TilePPrJavaLibrary.
        CheckBoxMenuItemDebugPPr.setState(DebugSettings.getDebugPPrLibrary());
        tilepprjavalibrary.lib.PPrLib.setDebugPPr(DebugSettings.getDebugPPrLibrary());
    }

    /**
     * Populate frame with tabs panels.
     */
    private void populateFrameTabs() {
        // Draws the subpanes.
        TabbedPane.addTab("IPbus Server", pIPbusPanel);
        TabbedPane.addTab("PPr Demonstrator", pPPrDemoPanel);
        TabbedPane.addTab("System Tests", pSTPanel);
        TabbedPane.addTab("Diagnostics", pDiagnostics);
        TabbedPane.addTab("Credits", pCredits);

        pPPrDemoPanel.TabbedPane.addTab("PPrDemo Global", pPPrDemoGlobal);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo Links Control", pPPrDemoLinksCtrl);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo CRC Counters", pPPrDemoCRCCounters);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo Links Status", pPPrDemoLinksStatus);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo CRC Checks", pPPrDemoCRCChecks);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo DaughterBoard", pPPrDemoDB);
        pPPrDemoPanel.TabbedPane.addTab("PPrDemo Front-End", pPPrDemoFE);

        pSTPanel.TabbedPane.addTab("Pedestal Run", pSTPedestalPanel);
        pSTPanel.TabbedPane.addTab("ADC Linearity Run", pSTADCLinearityPanel);
        pSTPanel.TabbedPane.addTab("CIS Linearity Run", pSTCISLinearityPanel);
        pSTPanel.TabbedPane.addTab("CIS Shape Run", pSTCISShapePanel);
    }

    /**
     * Modify some of the Swing components.
     */
    private void initModifyComponents() {
        //Needed for the JScrollPane to keep scrolling down as more text is added
        DefaultCaret caret = (DefaultCaret) TextAreaConsole.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        TabbedPane = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        TextAreaConsole = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        ButtonClearConsole = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        LabelPPrServer = new javax.swing.JLabel();
        LabelConnectMainFrame = new javax.swing.JLabel();
        LabelLinksStatus = new javax.swing.JLabel();
        LabelLinksStatusMainFrame = new javax.swing.JLabel();
        LabelCRCChecks = new javax.swing.JLabel();
        LabelCRCChecksMainFrame = new javax.swing.JLabel();
        LabelPedestalRun = new javax.swing.JLabel();
        LabelPedestalRunMainFrame = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        LabelCISLinearityRunMainFrame = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        LabelCISShapeRunMainFrame = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        LabelADCLinearityRunMainFrame = new javax.swing.JLabel();
        MenuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        MenuItemExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        CheckBoxMenuItemVerbose = new javax.swing.JCheckBoxMenuItem();
        CheckBoxMenuItemDebugIPbus = new javax.swing.JCheckBoxMenuItem();
        CheckBoxMenuItemDebugPPr = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PPrDemo Java Client");
        setLocation(new java.awt.Point(15, 30));
        setResizable(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Console"));
        jScrollPane1.setAutoscrolls(true);

        TextAreaConsole.setColumns(20);
        TextAreaConsole.setRows(5);
        jScrollPane1.setViewportView(TextAreaConsole);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 15, 0};
        jPanel1Layout.rowHeights = new int[] {0};
        jPanel1.setLayout(jPanel1Layout);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        ButtonClearConsole.setText("Clear Console");
        ButtonClearConsole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonClearConsoleActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(ButtonClearConsole, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 25, 0};
        jPanel2Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel2.setLayout(jPanel2Layout);

        LabelPPrServer.setText("PPr Server:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(LabelPPrServer, gridBagConstraints);

        LabelConnectMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelConnectMainFrame.setText("not connected");
        LabelConnectMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(LabelConnectMainFrame, gridBagConstraints);

        LabelLinksStatus.setText("Links Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(LabelLinksStatus, gridBagConstraints);

        LabelLinksStatusMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelLinksStatusMainFrame.setText("not running");
        LabelLinksStatusMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel2.add(LabelLinksStatusMainFrame, gridBagConstraints);

        LabelCRCChecks.setText("CRC Checks:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(LabelCRCChecks, gridBagConstraints);

        LabelCRCChecksMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCRCChecksMainFrame.setText("not running");
        LabelCRCChecksMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel2.add(LabelCRCChecksMainFrame, gridBagConstraints);

        LabelPedestalRun.setText("Pedestal Run:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(LabelPedestalRun, gridBagConstraints);

        LabelPedestalRunMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelPedestalRunMainFrame.setText("not running");
        LabelPedestalRunMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel2.add(LabelPedestalRunMainFrame, gridBagConstraints);

        jLabel7.setText("CIS Linearity Run:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(jLabel7, gridBagConstraints);

        LabelCISLinearityRunMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCISLinearityRunMainFrame.setText("not running");
        LabelCISLinearityRunMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel2.add(LabelCISLinearityRunMainFrame, gridBagConstraints);

        jLabel3.setText("CIS shape run:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(jLabel3, gridBagConstraints);

        LabelCISShapeRunMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCISShapeRunMainFrame.setText("not running");
        LabelCISShapeRunMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel2.add(LabelCISShapeRunMainFrame, gridBagConstraints);

        jLabel5.setText("ADC Linearity Run:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel2.add(jLabel5, gridBagConstraints);

        LabelADCLinearityRunMainFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelADCLinearityRunMainFrame.setText("not running");
        LabelADCLinearityRunMainFrame.setPreferredSize(new java.awt.Dimension(110, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel2.add(LabelADCLinearityRunMainFrame, gridBagConstraints);

        jPanel5.add(jPanel2, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel4.add(jPanel1, gridBagConstraints);

        jMenu1.setText("File");

        MenuItemExit.setText("Exit");
        MenuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItemExitActionPerformed(evt);
            }
        });
        jMenu1.add(MenuItemExit);

        MenuBar.add(jMenu1);

        jMenu2.setText("Edit");
        MenuBar.add(jMenu2);

        jMenu3.setText("Verbose");
        jMenu3.setBorderPainted(true);
        jMenu3.setOpaque(false);
        jMenu3.setRolloverEnabled(true);

        CheckBoxMenuItemVerbose.setText("Verbose console");
        CheckBoxMenuItemVerbose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMenuItemVerboseActionPerformed(evt);
            }
        });
        jMenu3.add(CheckBoxMenuItemVerbose);

        CheckBoxMenuItemDebugIPbus.setText("Debug IPbus Library");
        CheckBoxMenuItemDebugIPbus.setOpaque(false);
        CheckBoxMenuItemDebugIPbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMenuItemDebugIPbusActionPerformed(evt);
            }
        });
        jMenu3.add(CheckBoxMenuItemDebugIPbus);

        CheckBoxMenuItemDebugPPr.setText("Debug PPr Library");
        CheckBoxMenuItemDebugPPr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMenuItemDebugPPrActionPerformed(evt);
            }
        });
        jMenu3.add(CheckBoxMenuItemDebugPPr);

        MenuBar.add(jMenu3);

        setJMenuBar(MenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 1237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 703, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TabbedPane)
                .addContainerGap())
        );

        pack();
    }//GEN-END:initComponents

    /**
     * Getter method for private field TextAreaConsole.
     *
     * @return the TextAreaConsole JTextArea field.
     */
    public static javax.swing.JTextArea getTextAreaConsole() {
        return TextAreaConsole;
    }

    /**
     * Exit action from File menu item.
     *
     * @param evt Input ActionEvent object.
     */
    private void MenuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItemExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_MenuItemExitActionPerformed

    /**
     * Set debug flag for IPbus library from Verbose menu item.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMenuItemDebugIPbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMenuItemDebugIPbusActionPerformed
        if (!DebugSettings.getDebugIPbusLibrary()) {
            DebugSettings.setDebugIPbusLibrary(true);
            Console.print_cr("Set debug (IPbus) ON");
        } else {
            DebugSettings.setDebugIPbusLibrary(false);
            Console.print_cr("Set debug (IPbus) OFF");
        }

        // Set debug flag for the IPbusJavaLibrary.
        ipbusjavalibrary.ipbus.Debug.setDebugIPbus(DebugSettings.getDebugIPbusLibrary());
    }//GEN-LAST:event_CheckBoxMenuItemDebugIPbusActionPerformed

    /**
     * Set debug flag for PPr library from Verbose menu item.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMenuItemDebugPPrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMenuItemDebugPPrActionPerformed
        if (!DebugSettings.getDebugPPrLibrary()) {
            DebugSettings.setDebugPPrLibrary(true);
            Console.print_cr("Set debug (PPr) ON");
        } else {
            DebugSettings.setDebugPPrLibrary(false);
            Console.print_cr("Set debug (PPr) OFF");
        }

        // Set debug flag for the PPrJavaLibrary.
        tilepprjavalibrary.lib.PPrLib.setDebugPPr(DebugSettings.getDebugPPrLibrary());
    }//GEN-LAST:event_CheckBoxMenuItemDebugPPrActionPerformed

    /**
     * Clear console.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonClearConsoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonClearConsoleActionPerformed
        TextAreaConsole.setText(null);
    }//GEN-LAST:event_ButtonClearConsoleActionPerformed

    /**
     * Set debug flag for verbose console from Verbose menu item.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMenuItemVerboseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMenuItemVerboseActionPerformed
        if (!DebugSettings.getVerbose()) {
            DebugSettings.setVerbose(true);
            Console.print_cr("Set verbose ON");
        } else {
            DebugSettings.setVerbose(false);
            Console.print_cr("Set verbose OFF");
        }
    }//GEN-LAST:event_CheckBoxMenuItemVerboseActionPerformed

    /**
     * main() method.
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    /**
     * TilePPrDemoJava release version number.
     */
    public static String RELEASE_NB = "0.3";

    /**
     * Declare PPrLib object.
     */
    public PPrLib pprlib;

    /**
     * Initialization debug flag.
     */
    public static Boolean debug = true;

    /**
     * Declare an IPbusPanel class object.
     */
    private final IPbusPanel pIPbusPanel;

    /**
     * Declare PPrDemoPanel class object.
     */
    private final PPrDemoPanel pPPrDemoPanel;

    /**
     * Declare PPrDemoGlobal class object.
     */
    private final PPrDemoGlobal pPPrDemoGlobal;

    /**
     * Declare PPrDemoLinksCtrl class object.
     */
    private final PPrDemoLinksCtrl pPPrDemoLinksCtrl;

    /**
     * Declare PPrDemoLinksStatus class object.
     */
    private final PPrDemoLinksStatus pPPrDemoLinksStatus;

    /**
     * Declare PPrDemoCRCCounters class object.
     */
    private final PPrDemoCRCCounters pPPrDemoCRCCounters;

    /**
     * Declare PPrDemoCRCChecks class object.
     */
    private final PPrDemoCRCChecks pPPrDemoCRCChecks;

    /**
     * Declare PPrDemoDB class object.
     */
    private final PPrDemoDB pPPrDemoDB;

    /**
     * Declare PPrDemoFE class object.
     */
    private final PPrDemoFE pPPrDemoFE;

    /**
     * Declares a System Test Panel class object.
     */
    private final STPanel pSTPanel;

    private final STPedestalPanel pSTPedestalPanel;

    private final STADCLinearityPanel pSTADCLinearityPanel;

    private final STCISLinearityPanel pSTCISLinearityPanel;

    private final STCISShapePanel pSTCISShapePanel;

    /**
     * Declares a Diagnostics Panel class object.
     */
    private final Diagnostics pDiagnostics;

    private final Credits pCredits;

    public Defaults defaults;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonClearConsole;
    private javax.swing.JCheckBoxMenuItem CheckBoxMenuItemDebugIPbus;
    private javax.swing.JCheckBoxMenuItem CheckBoxMenuItemDebugPPr;
    private javax.swing.JCheckBoxMenuItem CheckBoxMenuItemVerbose;
    public static javax.swing.JLabel LabelADCLinearityRunMainFrame;
    public static javax.swing.JLabel LabelCISLinearityRunMainFrame;
    public static javax.swing.JLabel LabelCISShapeRunMainFrame;
    private javax.swing.JLabel LabelCRCChecks;
    public static javax.swing.JLabel LabelCRCChecksMainFrame;
    public static javax.swing.JLabel LabelConnectMainFrame;
    private javax.swing.JLabel LabelLinksStatus;
    public static javax.swing.JLabel LabelLinksStatusMainFrame;
    private javax.swing.JLabel LabelPPrServer;
    private javax.swing.JLabel LabelPedestalRun;
    public static javax.swing.JLabel LabelPedestalRunMainFrame;
    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JMenuItem MenuItemExit;
    private javax.swing.JTabbedPane TabbedPane;
    private static javax.swing.JTextArea TextAreaConsole;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
