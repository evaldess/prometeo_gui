/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JTextField;

import ipbusjavalibrary.utilities.ByteTools;

import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoCRCCounters extends javax.swing.JPanel {

    /**
     * Creates new form PPrDemoDB
     */
    public PPrDemoCRCCounters(PPrLib pprlib) {
        System.out.println("PPrDemoCRCCounters -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        link_strings = new String[]{"A0", "A1", "B0", "B1"};

        side_strings = new String[]{"A", "B"};

        TextFieldMD1 = new JTextField[]{
            TextFieldMD1A0, TextFieldMD1A1, TextFieldMD1B0,
            TextFieldMD1B1};

        TextFieldMD2 = new JTextField[]{
            TextFieldMD2A0, TextFieldMD2A1, TextFieldMD2B0,
            TextFieldMD2B1};

        TextFieldMD3 = new JTextField[]{
            TextFieldMD3A0, TextFieldMD3A1, TextFieldMD3B0,
            TextFieldMD3B1};

        TextFieldMD4 = new JTextField[]{
            TextFieldMD4A0, TextFieldMD4A1, TextFieldMD4B0,
            TextFieldMD4B1};

        TextFieldMD = new JTextField[4][4];
        for (int link = 0; link < 4; link++) {
            TextFieldMD[0][link] = TextFieldMD1[link];
            TextFieldMD[1][link] = TextFieldMD2[link];
            TextFieldMD[2][link] = TextFieldMD3[link];
            TextFieldMD[3][link] = TextFieldMD4[link];
        }

        TextFieldMD1Tot = new JTextField[]{
            TextFieldMD1TotA, TextFieldMD1TotB};

        TextFieldMD2Tot = new JTextField[]{
            TextFieldMD2TotA, TextFieldMD2TotB};

        TextFieldMD3Tot = new JTextField[]{
            TextFieldMD3TotA, TextFieldMD3TotB};

        TextFieldMD4Tot = new JTextField[]{
            TextFieldMD4TotA, TextFieldMD4TotB};

        TextFieldMDTot = new JTextField[4][2];
        for (int side = 0; side < 2; side++) {
            TextFieldMDTot[0][side] = TextFieldMD1Tot[side];
            TextFieldMDTot[1][side] = TextFieldMD2Tot[side];
            TextFieldMDTot[2][side] = TextFieldMD3Tot[side];
            TextFieldMDTot[3][side] = TextFieldMD4Tot[side];
        }

        TextFieldFramesMD = new JTextField[]{
            TextFieldFramesMD1, TextFieldFramesMD2, TextFieldFramesMD3,
            TextFieldFramesMD4};

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoCRCCounters"
                        + ""));
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableResetCRC = () -> {
            reset_CRC_counters();
        };

        RunnableReadCRC = () -> {
            read_CRC_counters();
        };

        RunnableReadFrames = () -> {
            read_Frames();
        };
    }

    /**
     * Reads PPr Link Control Select register.
     */
    private synchronized void reset_CRC_counters() {
        Boolean ret = pprlib.ppr.reset_CRC_counters();
        Console.print_cr("PPrDemoCRCCounters.reset_CRC_counters() -> " + (ret ? "Ok" : "Fail"));
    }

    private synchronized void show_CRC_data() {

    }

    /**
     * Reads PPr CRC error registers for all minidrawers.
     */
    private synchronized void read_CRC_counters() {
        Integer val;
        String fs;

        fs = "%s";
        Console.print_cr(String.format(fs, "PPrDemoCRCCounters.read_CRC_counters() -> "));

        // Loop on minidrawers.
        for (int md = 0; md < 4; md++) {

            // Loop on links.
            for (int link = 0; link < 4; link++) {

                val = pprlib.ppr.get_CRC_errors(md, link_strings[link]);
                if (DebugSettings.getVerbose()) {
                    fs = "  MD%d Link %s CRC errors read status: %s";
                    Console.print_cr(String.format(fs, md + 1, link_strings[link], (val != null ? "Ok" : "Fail")));
                }
                if (val == null) {
                    TextFieldMD[md][link].setText("Error");
                    TextFieldMD[md][link].setForeground(Color.RED);
                } else {
                    if (DebugSettings.getVerbose()) {
                        fs = "  data: 0x%08X (%d) -> %s";
                        Console.print_cr(String.format(fs, val, val, ByteTools.intToString(val, 4)));
                    }
                    if (val <= -1) {
                        val = Integer.MAX_VALUE;
                    }
                    TextFieldMD[md][link].setText(String.format("%d", val));
                    TextFieldMD[md][link].setForeground(Color.black);
                }
            }

            // Loop on sides.
            for (int side = 0; side < 2; side++) {

                val = pprlib.ppr.get_CRC_tot_errors(md, side_strings[side]);
                if (DebugSettings.getVerbose()) {
                    fs = "  MD%d Side %s tot CRC errors read status: %s";
                    Console.print_cr(String.format(fs, md + 1, side_strings[side], (val != null ? "Ok" : "Fail")));
                }
                if (val == null) {
                    TextFieldMDTot[md][side].setText("Error");
                    TextFieldMDTot[md][side].setForeground(Color.RED);
                } else {
                    if (DebugSettings.getVerbose()) {
                        fs = "  data: 0x%08X (%d) -> %s";
                        Console.print_cr(String.format(fs, val, val, ByteTools.intToString(val, 4)));
                    }
                    if (val <= -1) {
                        val = Integer.MAX_VALUE;
                    }
                    TextFieldMDTot[md][side].setText(String.format("%d", val));
                    TextFieldMDTot[md][side].setForeground(Color.black);
                }
            }
        }
    }

    /**
     * Reads PPr Frames registers for all minidrawers.
     */
    private synchronized void read_Frames() {
        Long val;
        String fs;

        Console.print_cr("PPrDemoCRCCounters.read_Frames() -> ");

        // Loop on minidrawers.
        for (int md = 0; md < 4; md++) {

            val = pprlib.ppr.get_frames(md);
            if (DebugSettings.getVerbose()) {
                fs = "  MD%d frames read status: %s";
                Console.print_cr(String.format(fs, md + 1, (val != null ? "Ok" : "Fail")));
            }
            if (val == null) {
                TextFieldFramesMD[md].setText("Error");
                TextFieldFramesMD[md].setForeground(Color.RED);
            } else {
                if (DebugSettings.getVerbose()) {
                    fs = "  frames: 0x%016X (%d)";
                    Console.print_cr(String.format(fs, val, val));
                }
                TextFieldFramesMD[md].setText(String.format("%d", val));
                TextFieldFramesMD[md].setForeground(Color.black);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        TextFieldFramesMD1 = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        TextFieldFramesMD2 = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        TextFieldFramesMD3 = new javax.swing.JTextField();
        jPanel20 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldFramesMD4 = new javax.swing.JTextField();
        jPanel21 = new javax.swing.JPanel();
        ButtonReadFrames = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        TextFieldMD1A0 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        TextFieldMD1A1 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        TextFieldMD1B0 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        TextFieldMD1B1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        TextFieldMD1TotA = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        TextFieldMD1TotB = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        TextFieldMD2A0 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        TextFieldMD2A1 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        TextFieldMD2B0 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TextFieldMD2B1 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        TextFieldMD2TotA = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        TextFieldMD2TotB = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        TextFieldMD3A0 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        TextFieldMD3A1 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        TextFieldMD3B0 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        TextFieldMD3B1 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        TextFieldMD3TotA = new javax.swing.JTextField();
        TextFieldMD3TotB = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        TextFieldMD4A0 = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        TextFieldMD4A1 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        TextFieldMD4B0 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        TextFieldMD4B1 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        TextFieldMD4TotA = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        TextFieldMD4TotB = new javax.swing.JTextField();
        jPanel22 = new javax.swing.JPanel();
        ButtonReadCRCErrors = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        ButtonResetCRC = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0};
        jPanel2Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel2.setLayout(jPanel2Layout);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0};
        jPanel6Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel6.setLayout(jPanel6Layout);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel8.setText("<html><b>Number of Frames</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(jPanel7, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0, 15, 0, 15, 0, 15, 0};
        jPanel16Layout.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout);

        java.awt.GridBagLayout jPanel17Layout = new java.awt.GridBagLayout();
        jPanel17Layout.columnWidths = new int[] {0};
        jPanel17Layout.rowHeights = new int[] {0, 5, 0};
        jPanel17.setLayout(jPanel17Layout);

        jLabel9.setText("MD1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel9, gridBagConstraints);

        TextFieldFramesMD1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldFramesMD1.setText("0");
        TextFieldFramesMD1.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel17.add(TextFieldFramesMD1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel17, gridBagConstraints);

        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0};
        jPanel18Layout.rowHeights = new int[] {0, 5, 0};
        jPanel18.setLayout(jPanel18Layout);

        jLabel10.setText("MD2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel10, gridBagConstraints);

        TextFieldFramesMD2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldFramesMD2.setText("0");
        TextFieldFramesMD2.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel18.add(TextFieldFramesMD2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel18, gridBagConstraints);

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0};
        jPanel19Layout.rowHeights = new int[] {0, 5, 0};
        jPanel19.setLayout(jPanel19Layout);

        jLabel11.setText("MD3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jLabel11, gridBagConstraints);

        TextFieldFramesMD3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldFramesMD3.setText("0");
        TextFieldFramesMD3.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel19.add(TextFieldFramesMD3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel19, gridBagConstraints);

        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0};
        jPanel20Layout.rowHeights = new int[] {0, 5, 0};
        jPanel20.setLayout(jPanel20Layout);

        jLabel12.setText("MD4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel12, gridBagConstraints);

        TextFieldFramesMD4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldFramesMD4.setText("0");
        TextFieldFramesMD4.setPreferredSize(new java.awt.Dimension(150, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel20.add(TextFieldFramesMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel20, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(jPanel16, gridBagConstraints);

        jPanel21.setLayout(new java.awt.GridBagLayout());

        ButtonReadFrames.setText("Read");
        ButtonReadFrames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadFramesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(ButtonReadFrames, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0};
        jPanel8Layout.rowHeights = new int[] {0, 10, 0, 10, 0};
        jPanel8.setLayout(jPanel8Layout);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jLabel22.setText("<html><b>CRC Error Counters</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 15, 0, 15, 0, 15, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0};
        jPanel11Layout.rowHeights = new int[] {0, 10, 0};
        jPanel11.setLayout(jPanel11Layout);

        jLabel23.setText("MD1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jLabel23, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        jPanel12Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel12.setLayout(jPanel12Layout);

        jLabel24.setText("Link A0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel24, gridBagConstraints);

        TextFieldMD1A0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A0.setText("0");
        TextFieldMD1A0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel12.add(TextFieldMD1A0, gridBagConstraints);

        jLabel25.setText("Link A1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel25, gridBagConstraints);

        TextFieldMD1A1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1A1.setText("0");
        TextFieldMD1A1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel12.add(TextFieldMD1A1, gridBagConstraints);

        jLabel26.setText("Link B0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel26, gridBagConstraints);

        TextFieldMD1B0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B0.setText("0");
        TextFieldMD1B0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel12.add(TextFieldMD1B0, gridBagConstraints);

        jLabel1.setText("Link B1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel1, gridBagConstraints);

        TextFieldMD1B1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1B1.setText("0");
        TextFieldMD1B1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel12.add(TextFieldMD1B1, gridBagConstraints);

        jLabel13.setText("Tot Side A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel13, gridBagConstraints);

        TextFieldMD1TotA.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1TotA.setText("0");
        TextFieldMD1TotA.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel12.add(TextFieldMD1TotA, gridBagConstraints);

        jLabel16.setText("Tot Side B:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel12.add(jLabel16, gridBagConstraints);

        TextFieldMD1TotB.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD1TotB.setText("0");
        TextFieldMD1TotB.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        jPanel12.add(TextFieldMD1TotB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel11.add(jPanel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel1Layout1 = new java.awt.GridBagLayout();
        jPanel1Layout1.columnWidths = new int[] {0};
        jPanel1Layout1.rowHeights = new int[] {0, 10, 0};
        jPanel1.setLayout(jPanel1Layout1);

        jLabel2.setText("MD2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel2, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 5, 0};
        jPanel13Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel13.setLayout(jPanel13Layout);

        jLabel27.setText("Link A0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel27, gridBagConstraints);

        TextFieldMD2A0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A0.setText("0");
        TextFieldMD2A0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel13.add(TextFieldMD2A0, gridBagConstraints);

        jLabel28.setText("Link A1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel28, gridBagConstraints);

        TextFieldMD2A1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2A1.setText("0");
        TextFieldMD2A1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel13.add(TextFieldMD2A1, gridBagConstraints);

        jLabel29.setText("Link B0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel29, gridBagConstraints);

        TextFieldMD2B0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B0.setText("0");
        TextFieldMD2B0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel13.add(TextFieldMD2B0, gridBagConstraints);

        jLabel3.setText("Link B1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel3, gridBagConstraints);

        TextFieldMD2B1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2B1.setText("0");
        TextFieldMD2B1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel13.add(TextFieldMD2B1, gridBagConstraints);

        jLabel14.setText("Tot Side A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel14, gridBagConstraints);

        TextFieldMD2TotA.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2TotA.setText("0");
        TextFieldMD2TotA.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel13.add(TextFieldMD2TotA, gridBagConstraints);

        jLabel15.setText("Tot Side B:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel13.add(jLabel15, gridBagConstraints);

        TextFieldMD2TotB.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD2TotB.setText("0");
        TextFieldMD2TotB.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel13.add(TextFieldMD2TotB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel1, gridBagConstraints);

        java.awt.GridBagLayout jPanel4Layout = new java.awt.GridBagLayout();
        jPanel4Layout.columnWidths = new int[] {0};
        jPanel4Layout.rowHeights = new int[] {0, 10, 0};
        jPanel4.setLayout(jPanel4Layout);

        jLabel4.setText("MD3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jLabel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0, 5, 0};
        jPanel14Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel14.setLayout(jPanel14Layout);

        jLabel30.setText("Link A0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel30, gridBagConstraints);

        TextFieldMD3A0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A0.setText("0");
        TextFieldMD3A0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel14.add(TextFieldMD3A0, gridBagConstraints);

        jLabel31.setText("Link A1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel31, gridBagConstraints);

        TextFieldMD3A1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3A1.setText("0");
        TextFieldMD3A1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel14.add(TextFieldMD3A1, gridBagConstraints);

        jLabel32.setText("Link B0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel32, gridBagConstraints);

        TextFieldMD3B0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B0.setText("0");
        TextFieldMD3B0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel14.add(TextFieldMD3B0, gridBagConstraints);

        jLabel5.setText("Link B1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel5, gridBagConstraints);

        TextFieldMD3B1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3B1.setText("0");
        TextFieldMD3B1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel14.add(TextFieldMD3B1, gridBagConstraints);

        jLabel17.setText("Tot Side A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel17, gridBagConstraints);

        jLabel18.setText("Tot Side B:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel14.add(jLabel18, gridBagConstraints);

        TextFieldMD3TotA.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3TotA.setText("0");
        TextFieldMD3TotA.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel14.add(TextFieldMD3TotA, gridBagConstraints);

        TextFieldMD3TotB.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD3TotB.setText("0");
        TextFieldMD3TotB.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel14.add(TextFieldMD3TotB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel4.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0};
        jPanel5Layout.rowHeights = new int[] {0, 10, 0};
        jPanel5.setLayout(jPanel5Layout);

        jLabel6.setText("MD4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jLabel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel15Layout = new java.awt.GridBagLayout();
        jPanel15Layout.columnWidths = new int[] {0, 5, 0};
        jPanel15Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel15.setLayout(jPanel15Layout);

        jLabel33.setText("Link A0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel33, gridBagConstraints);

        TextFieldMD4A0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A0.setText("0");
        TextFieldMD4A0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel15.add(TextFieldMD4A0, gridBagConstraints);

        jLabel34.setText("Link A1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel34, gridBagConstraints);

        TextFieldMD4A1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4A1.setText("0");
        TextFieldMD4A1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel15.add(TextFieldMD4A1, gridBagConstraints);

        jLabel35.setText("Link B0:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel35, gridBagConstraints);

        TextFieldMD4B0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B0.setText("0");
        TextFieldMD4B0.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel15.add(TextFieldMD4B0, gridBagConstraints);

        jLabel7.setText("Link B1:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel7, gridBagConstraints);

        TextFieldMD4B1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4B1.setText("0");
        TextFieldMD4B1.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel15.add(TextFieldMD4B1, gridBagConstraints);

        jLabel19.setText("Tot Side A:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel19, gridBagConstraints);

        TextFieldMD4TotA.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4TotA.setText("0");
        TextFieldMD4TotA.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel15.add(TextFieldMD4TotA, gridBagConstraints);

        jLabel20.setText("Tot Side B:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jLabel20, gridBagConstraints);

        TextFieldMD4TotB.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMD4TotB.setText("0");
        TextFieldMD4TotB.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel15.add(TextFieldMD4TotB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel10, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        ButtonReadCRCErrors.setText("Read");
        ButtonReadCRCErrors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadCRCErrorsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel22.add(ButtonReadCRCErrors, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel8.add(jPanel22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel8, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        ButtonResetCRC.setText("Reset CRC Counters");
        ButtonResetCRC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetCRCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(ButtonResetCRC, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel3, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(105, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(105, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Resets PPr CRC counters.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetCRCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetCRCActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoCRC -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableResetCRC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetCRCActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetCRCActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetCRCActionPerformed

    /**
     * Reads the PPr CRC error registers for all minidrawers and all links.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadCRCErrorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadCRCErrorsActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoCRC -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadCRC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadCRCErrorsActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadCRCErrorsActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadCRCErrorsActionPerformed

    /**
     * Reads the PPr frames registers for all minidrawers.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadFramesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadFramesActionPerformed
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoCRC -> no IPbus PPr connection.");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadFrames);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadFramesActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadFramesActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadFramesActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for reset_CRC_counters() method.
     */
    Runnable RunnableResetCRC;

    /**
     * Declares Runnable for read_CRC_counters() method.
     */
    Runnable RunnableReadCRC;

    /**
     * Declares Runnable for read_Frames() method.
     */
    Runnable RunnableReadFrames;

    String[] link_strings;

    String[] side_strings;

    private static javax.swing.JTextField[] TextFieldMD1;
    private static javax.swing.JTextField[] TextFieldMD2;
    private static javax.swing.JTextField[] TextFieldMD3;
    private static javax.swing.JTextField[] TextFieldMD4;

    private static javax.swing.JTextField[][] TextFieldMD;

    private static javax.swing.JTextField[] TextFieldMD1Tot;
    private static javax.swing.JTextField[] TextFieldMD2Tot;
    private static javax.swing.JTextField[] TextFieldMD3Tot;
    private static javax.swing.JTextField[] TextFieldMD4Tot;

    private static javax.swing.JTextField[][] TextFieldMDTot;

    private static javax.swing.JTextField[] TextFieldFramesMD;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonReadCRCErrors;
    private javax.swing.JButton ButtonReadFrames;
    private javax.swing.JButton ButtonResetCRC;
    private javax.swing.JTextField TextFieldFramesMD1;
    private javax.swing.JTextField TextFieldFramesMD2;
    private javax.swing.JTextField TextFieldFramesMD3;
    private javax.swing.JTextField TextFieldFramesMD4;
    private javax.swing.JTextField TextFieldMD1A0;
    private javax.swing.JTextField TextFieldMD1A1;
    private javax.swing.JTextField TextFieldMD1B0;
    private javax.swing.JTextField TextFieldMD1B1;
    private javax.swing.JTextField TextFieldMD1TotA;
    private javax.swing.JTextField TextFieldMD1TotB;
    private javax.swing.JTextField TextFieldMD2A0;
    private javax.swing.JTextField TextFieldMD2A1;
    private javax.swing.JTextField TextFieldMD2B0;
    private javax.swing.JTextField TextFieldMD2B1;
    private javax.swing.JTextField TextFieldMD2TotA;
    private javax.swing.JTextField TextFieldMD2TotB;
    private javax.swing.JTextField TextFieldMD3A0;
    private javax.swing.JTextField TextFieldMD3A1;
    private javax.swing.JTextField TextFieldMD3B0;
    private javax.swing.JTextField TextFieldMD3B1;
    private javax.swing.JTextField TextFieldMD3TotA;
    private javax.swing.JTextField TextFieldMD3TotB;
    private javax.swing.JTextField TextFieldMD4A0;
    private javax.swing.JTextField TextFieldMD4A1;
    private javax.swing.JTextField TextFieldMD4B0;
    private javax.swing.JTextField TextFieldMD4B1;
    private javax.swing.JTextField TextFieldMD4TotA;
    private javax.swing.JTextField TextFieldMD4TotB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
