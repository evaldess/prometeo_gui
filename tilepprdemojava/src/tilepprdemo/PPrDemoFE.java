/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JTextField;

import tilepprjavalibrary.lib.PPrLib;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrDemoFE extends javax.swing.JPanel {

    /**
     * Creates new form PPrDemoFE
     *
     * @param pprlib
     */
    public PPrDemoFE(PPrLib pprlib) {
        System.out.println("PPrDemoFE -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        // Defines ArrayList with selected minidrawers.
        selected_MD = new ArrayList<>();

        // Gets default minidrawer selection.
        get_selected_MDs();

        // Gets default DF FPGA selection.
        get_selected_DB_FPGA_side();

        //get_selected_MDs();
        // Defines JTextField array with JTextFields for the FEBs Switches.
        TextFieldSwitches = new JTextField[]{
            TextFieldSwitches0, TextFieldSwitches1, TextFieldSwitches2,
            TextFieldSwitches3, TextFieldSwitches4, TextFieldSwitches5,
            TextFieldSwitches6, TextFieldSwitches7, TextFieldSwitches8,
            TextFieldSwitches9, TextFieldSwitches10, TextFieldSwitches11};

        // Defines JTextField array with JTextFields for the FEBs CIS DACs.
        TextFieldCISDACREAD = new JTextField[]{
            TextFieldDAC0, TextFieldDAC1, TextFieldDAC2,
            TextFieldDAC3, TextFieldDAC4, TextFieldDAC5,
            TextFieldDAC6, TextFieldDAC7, TextFieldDAC8,
            TextFieldDAC9, TextFieldDAC10, TextFieldDAC11};

        // Defines JTextField array with JTextFields for the FEBs Ped HG pos read values.
        TextFieldPedHGposREAD = new JTextField[]{
            TextFieldPedHGpos0, TextFieldPedHGpos1, TextFieldPedHGpos2,
            TextFieldPedHGpos3, TextFieldPedHGpos4, TextFieldPedHGpos5,
            TextFieldPedHGpos6, TextFieldPedHGpos7, TextFieldPedHGpos8,
            TextFieldPedHGpos9, TextFieldPedHGpos10, TextFieldPedHGpos11};

        // Defines JTextField array with JTextFields for the FEBs Ped HG neg read values.
        TextFieldPedHGnegREAD = new JTextField[]{
            TextFieldPedHGneg0, TextFieldPedHGneg1, TextFieldPedHGneg2,
            TextFieldPedHGneg3, TextFieldPedHGneg4, TextFieldPedHGneg5,
            TextFieldPedHGneg6, TextFieldPedHGneg7, TextFieldPedHGneg8,
            TextFieldPedHGneg9, TextFieldPedHGneg10, TextFieldPedHGneg11};

        // Defines JTextField array with JTextFields for the FEBs Ped LG pos read values.
        TextFieldPedLGposREAD = new JTextField[]{
            TextFieldPedLGpos0, TextFieldPedLGpos1, TextFieldPedLGpos2,
            TextFieldPedLGpos3, TextFieldPedLGpos4, TextFieldPedLGpos5,
            TextFieldPedLGpos6, TextFieldPedLGpos7, TextFieldPedLGpos8,
            TextFieldPedLGpos9, TextFieldPedLGpos10, TextFieldPedLGpos11};

        // Defines JTextField array with JTextFields for the FEBs Ped LG pos read values.
        TextFieldPedLGnegREAD = new JTextField[]{
            TextFieldPedLGneg0, TextFieldPedLGneg1, TextFieldPedLGneg2,
            TextFieldPedLGneg3, TextFieldPedLGneg4, TextFieldPedLGneg5,
            TextFieldPedLGneg6, TextFieldPedLGneg7, TextFieldPedLGneg8,
            TextFieldPedLGneg9, TextFieldPedLGneg10, TextFieldPedLGneg11};

        Switches_read = new Integer[4][12];
        CIS_DAC_read = new Integer[4][12];
        pedHGpos_read = new Integer[4][12];
        pedHGneg_read = new Integer[4][12];
        pedLGpos_read = new Integer[4][12];
        pedLGneg_read = new Integer[4][12];
        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                Switches_read[md][adc] = 0;
                CIS_DAC_read[md][adc] = 0;
                pedHGpos_read[md][adc] = 0;
                pedHGneg_read[md][adc] = 0;
                pedLGpos_read[md][adc] = 0;
                pedLGneg_read[md][adc] = 0;
            }
        }

        show_MD = 0;

        LabelMDShown.setText("Data read shown for MD1");
        LabelMDShown.setForeground(Color.blue);

        LabelWriteAllFEBs.setForeground(Color.blue);

        ButtonShowMD1.setBackground(Color.GRAY);
        ButtonShowMD2.setBackground(null);
        ButtonShowMD3.setBackground(null);
        ButtonShowMD4.setBackground(null);

        // Define Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.   
        executor = new ThreadPoolExecutor(
                5,
                5,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("PPrDemoDB"));
    }

    /**
     * Defines Runnables for threads. A Runnable object is an interface designed
     * to provide a common protocol for objects that wish to execute code while
     * they are active
     */
    private void initDefineRunnables() {
        RunnableWriteSwitches = () -> {
            write_Switches();
        };

        RunnableWriteCISDAC = () -> {
            write_CIS_DAC();
        };

        RunnableWritePedHGpos = () -> {
            write_PedHGpos();
        };

        RunnableWritePedHGneg = () -> {
            write_PedHGneg();
        };

        RunnableWritePedLGpos = () -> {
            write_PedLGpos();
        };

        RunnableWritePedLGneg = () -> {
            write_PedLGneg();
        };

        RunnableReadSwitches = () -> {
            read_Switches();
        };

        RunnableReadCISDAC = () -> {
            read_CIS_DAC();
        };

        RunnableReadPedHGpos = () -> {
            read_PedHGpos();
        };

        RunnableReadPedHGneg = () -> {
            read_PedHGneg();
        };

        RunnableReadPedLGpos = () -> {
            read_PedLGpos();
        };

        RunnableReadPedLGneg = () -> {
            read_PedLGneg();
        };
    }

    /**
     * Gets minidrawers to process.
     */
    private synchronized void get_selected_MDs() {
        selected_MD.clear();
        selected_MD.add(CheckBoxMD1.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD2.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD3.isSelected() ? 1 : 0);
        selected_MD.add(CheckBoxMD4.isSelected() ? 1 : 0);

        selected_MD_tot = 0;
        for (Integer md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {
                selected_MD_tot++;
            }
        }

        broadcast_MD = (CheckBoxBroadcastMD.isSelected() ? 1 : 0);
    }

    /**
     * Gets Daughterboard FPGA side to process.
     */
    private synchronized void get_selected_DB_FPGA_side() {
        String side_strings[] = new String[]{"Broadcast", "Side A", "Side B", "Unknown"};

        Object selected = ComboBoxDBSide.getSelectedItem();
        String DBSideStr = selected.toString();
        switch (DBSideStr) {
            case "Broadcast":
                dbside = 0;
                dbsidestring = side_strings[0];
                break;
            case "Side A":
                dbside = 1;
                dbsidestring = side_strings[1];
                break;
            case "Side B":
                dbside = 2;
                dbsidestring = side_strings[2];
                break;
            default:
                dbsidestring = side_strings[3];
                break;
        }
    }

    /**
     * Transmits and readsback switches for all FEBs of active MDs.
     */
    private synchronized void write_Switches() {
        Boolean ret;
        String fs;
        int MDmin, MDmax;

        get_selected_MDs();
        get_selected_DB_FPGA_side();

        switches_write = Integer.valueOf(TextFieldSwitchesW.getText(), 10);

        fs = "PPrDemoFE.write_Switches() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, switches_write, switches_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = " DB FPGA Side: %d (%s)";

        Console.print_cr(String.format(fs, dbside, dbsidestring));
        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }
        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_switches(md, dbside, adc, switches_write);
                    if (DebugSettings.getVerbose()) {
                        fs = " transmit:  %s  MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                (ret ? "Ok" : "Fail"), md, adc,
                                switches_write, switches_write));
                    }

                    ret = pprlib.feb.readback_CIS_DAC(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = " readback: %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs,
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     * Transmits and readsback CIS DAC for all FEBs of active MDs.
     */
    private synchronized void write_CIS_DAC() {
        String fs;
        Boolean ret;
        int MDmin, MDmax;

        CIS_DAC_write = Integer.valueOf(TextFieldCISDACWRITE.getText(), 10);
        if (CIS_DAC_write > 4095) {
            CIS_DAC_write = 4095;
            TextFieldCISDACWRITE.setText("4095");
        }
        if (CIS_DAC_write < 0) {
            CIS_DAC_write = 0;
            TextFieldCISDACWRITE.setText("0");
        }

        fs = "PPrDemoFE.write_CIS_DAC() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, CIS_DAC_write, CIS_DAC_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = " DB FPGA Side: %d (%s)";
        Console.print_cr(String.format(fs, dbside, dbsidestring));

        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_CIS_DAC(md, dbside, adc, CIS_DAC_write);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs, " transmit: ",
                                (ret ? "Ok" : "Fail"), md, adc,
                                CIS_DAC_write, CIS_DAC_write));
                    }

                    ret = pprlib.feb.readback_CIS_DAC(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs, " readback: ",
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     * Transmits and readsback Ped HG positive for all FEBs of active MDs.
     */
    private synchronized void write_PedHGpos() {
        String fs;
        Boolean ret;
        int MDmin, MDmax;

        pedHGpos_write = Integer.valueOf(TextFieldPedHGposWRITE.getText(), 10);
        if (pedHGpos_write > 4095) {
            pedHGpos_write = 4095;
            TextFieldPedHGposWRITE.setText("4095");
        }
        if (pedHGpos_write < 0) {
            pedHGpos_write = 0;
            TextFieldPedHGposWRITE.setText("0");
        }

        fs = "PPrDemoFE.write_PedHGpos() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, pedHGpos_write, pedHGpos_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = "%s %d (%s)";
        Console.print_cr(String.format(fs, " DB FPGA Side: ", dbside, dbsidestring));

        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_ped_HG_pos(md, dbside, adc, pedHGpos_write);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs, " transmit: ",
                                (ret ? "Ok" : "Fail"), md, adc,
                                pedHGpos_write, pedHGpos_write));
                    }

                    ret = pprlib.feb.readback_ped_HG_pos(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs, " readback: ",
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     * Transmits and readsback Ped HG negative for all FEBs of active MDs.
     */
    private synchronized void write_PedHGneg() {
        String fs;
        Boolean ret;
        int MDmin, MDmax;

        pedHGneg_write = Integer.valueOf(TextFieldPedHGnegWRITE.getText(), 10);
        if (pedHGneg_write > 4095) {
            pedHGneg_write = 4095;
            TextFieldPedHGnegWRITE.setText("4095");
        }
        if (pedHGneg_write < 0) {
            pedHGneg_write = 0;
            TextFieldPedHGnegWRITE.setText("0");
        }

        fs = "PPrDemoFE.write_PedHGneg() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, pedHGneg_write, pedHGneg_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = "%s %d (%s)";
        Console.print_cr(String.format(fs, " DB FPGA Side: ", dbside, dbsidestring));

        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_ped_HG_neg(md, dbside, adc, pedHGneg_write);
                    if (DebugSettings.getVerbose()) {
                        fs = " transmit: %s MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                (ret ? "Ok" : "Fail"), md, adc,
                                pedHGneg_write, pedHGneg_write));
                    }

                    ret = pprlib.feb.readback_ped_HG_neg(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = " readback: %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs,
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     * Transmits and readsback Ped LG positive for all FEBs of active MDs.
     */
    private synchronized void write_PedLGpos() {
        String fs;
        Boolean ret;
        int MDmin, MDmax;

        pedLGpos_write = Integer.valueOf(TextFieldPedLGposWRITE.getText(), 10);
        if (pedLGpos_write > 4095) {
            pedLGpos_write = 4095;
            TextFieldPedLGposWRITE.setText("4095");
        }
        if (pedLGpos_write < 0) {
            pedLGpos_write = 0;
            TextFieldPedLGposWRITE.setText("0");
        }

        fs = "PPrDemoFE.write_PedLGpos() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, pedLGpos_write, pedLGpos_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = "%s %d (%s)";
        Console.print_cr(String.format(fs, " DB FPGA Side: ", dbside, dbsidestring));

        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_ped_LG_pos(md, dbside, adc, pedLGpos_write);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs, " transmit: ",
                                (ret ? "Ok" : "Fail"), md, adc,
                                pedLGpos_write, pedLGpos_write));
                    }

                    ret = pprlib.feb.readback_ped_LG_pos(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs, " readback: ",
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     * Transmits and readsback Ped LG negative for all FEBs of active MDs.
     */
    private synchronized void write_PedLGneg() {
        String fs;
        Boolean ret;
        int MDmin, MDmax;

        pedLGneg_write = Integer.valueOf(TextFieldPedLGnegWRITE.getText(), 10);
        if (pedLGneg_write > 4095) {
            pedLGneg_write = 4095;
            TextFieldPedLGnegWRITE.setText("4095");
        }
        if (pedLGneg_write < 0) {
            pedLGneg_write = 0;
            TextFieldPedLGnegWRITE.setText("0");
        }

        fs = "PPrDemoFE.write_PedLGneg() -> value: 0x%X (%d)";
        Console.print_cr(String.format(fs, pedLGneg_write, pedLGneg_write));

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Gets selected DB FPGA side.
        get_selected_DB_FPGA_side();
        fs = "%s %d (%s)";
        Console.print_cr(String.format(fs, " DB FPGA Side: ", dbside, dbsidestring));

        if (broadcast_MD == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        // Loop on active minidrawers.
        int mdidx = 0;
        for (int md = MDmin; md < MDmax; md++) {
            if ((broadcast_MD == 1) | ((broadcast_MD == 0) & (selected_MD.get(mdidx) == 1))) {
                for (int adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.transmit_ped_LG_neg(md, dbside, adc, pedLGneg_write);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d value: 0x%X (%d)";
                        Console.print_cr(String.format(fs, " transmit: ",
                                (ret ? "Ok" : "Fail"), md, adc,
                                pedLGneg_write, pedLGneg_write));
                    }

                    ret = pprlib.feb.readback_ped_LG_neg(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "%s %s  MD %d FEB %2d";
                        Console.print_cr(String.format(fs, " readback: ",
                                (ret ? "Ok" : "Fail"), md, adc));
                    }
                }
            }
            mdidx++;
        }
    }

    /**
     *
     */
    private synchronized void read_Switches() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_Switches() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers.
        for (int md = 0; md < 4; md++) {
            if (1 == selected_MD.get(md)) {
                for (int adc = 0; adc < 12; adc++) {
                    Switches_read[md][adc] = pprlib.ppr.read_feb_switches(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, Switches_read[md][adc], Switches_read[md][adc]));
                    }
                    if (Switches_read[show_MD][adc] == -1) {
                        TextFieldSwitches[adc].setText("Error");
                        TextFieldSwitches[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", Switches_read[show_MD][adc]);
                    TextFieldSwitches[adc].setText(output);
                }
            }
        }

        show_Switches();
    }

    /**
     *
     */
    private synchronized void read_CIS_DAC() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_CIS_DAC() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers
        for (int md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {
                for (int adc = 0; adc < 12; adc++) {
                    CIS_DAC_read[md][adc] = pprlib.ppr.read_feb_CIS_DAC(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, CIS_DAC_read[md][adc], CIS_DAC_read[md][adc]));
                    }
                    if (CIS_DAC_read[show_MD][adc] == -1) {
                        TextFieldCISDACREAD[adc].setText("Error");
                        TextFieldCISDACREAD[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", CIS_DAC_read[show_MD][adc]);
                    TextFieldCISDACREAD[adc].setText(output);
                }
            }
        }
    }

    /**
     *
     */
    private synchronized void read_PedHGpos() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_PedHGpos() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers
        for (int md = 0; md < 4; md++) {
            if (1 == selected_MD.get(md)) {
                for (int adc = 0; adc < 12; adc++) {
                    pedHGpos_read[md][adc] = pprlib.ppr.read_feb_ped_HG_pos(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, pedHGpos_read[md][adc], pedHGpos_read[md][adc]));
                    }
                    if (pedHGpos_read[show_MD][adc] == -1) {
                        TextFieldPedHGposREAD[adc].setText("Error");
                        TextFieldPedHGposREAD[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", pedHGpos_read[show_MD][adc]);
                    TextFieldPedHGposREAD[adc].setText(output);
                }
            }
        }
    }

    /**
     *
     */
    private synchronized void read_PedHGneg() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_PedHGneg() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers
        for (int md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {
                for (int adc = 0; adc < 12; adc++) {
                    pedHGneg_read[md][adc] = pprlib.ppr.read_feb_ped_HG_neg(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, pedHGneg_read[md][adc], pedHGneg_read[md][adc]));
                    }
                    if (pedHGneg_read[show_MD][adc] == -1) {
                        TextFieldPedHGnegREAD[adc].setText("Error");
                        TextFieldPedHGnegREAD[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", pedHGneg_read[show_MD][adc]);
                    TextFieldPedHGnegREAD[adc].setText(output);
                }
            }
        }
    }

    /**
     *
     */
    private synchronized void read_PedLGpos() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_PedLGpos() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers
        for (int md = 0; md < 4; md++) {
            if (selected_MD.get(md) == 1) {
                for (int adc = 0; adc < 12; adc++) {
                    pedLGpos_read[md][adc] = pprlib.ppr.read_feb_ped_LG_pos(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, pedLGpos_read[md][adc], pedLGpos_read[md][adc]));
                    }
                    if (pedLGpos_read[show_MD][adc] == -1) {
                        TextFieldPedLGposREAD[adc].setText("Error");
                        TextFieldPedLGposREAD[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", pedLGpos_read[show_MD][adc]);
                    TextFieldPedLGposREAD[adc].setText(output);
                }
            }
        }
    }

    /**
     *
     */
    private synchronized void read_PedLGneg() {
        String fs;
        String output;

        Console.print_cr("PPrDemoFE.read_PedLGneg() -> ");

        // Gets selected minidrawers.
        get_selected_MDs();
        fs = " Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((broadcast_MD == 1) ? "True" : "False")));
        if (broadcast_MD == 0) {
            fs = " Total MDs: %d (%d %d %d %d)";
            Console.print_cr(String.format(fs,
                    selected_MD_tot,
                    selected_MD.get(0),
                    selected_MD.get(1),
                    selected_MD.get(2),
                    selected_MD.get(3)));
        }

        if (selected_MD_tot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            return;
        }

        // Loop on active minidrawers
        for (int md = 0; md < 4; md++) {
            if (1 == selected_MD.get(md)) {
                for (int adc = 0; adc < 12; adc++) {
                    pedLGneg_read[md][adc] = pprlib.ppr.read_feb_ped_LG_neg(md, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD %d FEB %2d value read: 0x%X (%d)";
                        Console.print_cr(String.format(fs,
                                md + 1, adc, pedLGneg_read[md][adc], pedLGneg_read[md][adc]));
                    }
                    if (pedLGneg_read[show_MD][adc] == -1) {
                        TextFieldPedLGnegREAD[adc].setText("Error");
                        TextFieldPedLGnegREAD[adc].setForeground(Color.red);
                    }
                    output = String.format("%d", pedLGneg_read[show_MD][adc]);
                    TextFieldPedLGnegREAD[adc].setText(output);
                }
            }
        }
    }

    /**
     *
     */
    private synchronized void show_Switches() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (Switches_read[show_MD][adc] == -1) {
                TextFieldSwitches[adc].setText("Error");
                TextFieldSwitches[adc].setForeground(Color.red);
            }
            output = String.format("%d", Switches_read[show_MD][adc]);
            TextFieldSwitches[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_CIS_DAC() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (CIS_DAC_read[show_MD][adc] == -1) {
                TextFieldCISDACREAD[adc].setText("Error");
                TextFieldCISDACREAD[adc].setForeground(Color.red);
            }
            output = String.format("%d", CIS_DAC_read[show_MD][adc]);
            TextFieldCISDACREAD[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_PedHGpos() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (pedHGpos_read[show_MD][adc] == -1) {
                TextFieldPedHGposREAD[adc].setText("Error");
                TextFieldPedHGposREAD[adc].setForeground(Color.red);
            }
            output = String.format("%d", pedHGpos_read[show_MD][adc]);
            TextFieldPedHGposREAD[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_PedHGneg() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (pedHGneg_read[show_MD][adc] == -1) {
                TextFieldPedHGnegREAD[adc].setText("Error");
                TextFieldPedHGnegREAD[adc].setForeground(Color.red);
            }
            output = String.format("%d", pedHGneg_read[show_MD][adc]);
            TextFieldPedHGnegREAD[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_PedLGpos() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (pedLGpos_read[show_MD][adc] == -1) {
                TextFieldPedLGposREAD[adc].setText("Error");
                TextFieldPedLGposREAD[adc].setForeground(Color.red);
            }
            output = String.format("%d", pedLGpos_read[show_MD][adc]);
            TextFieldPedLGposREAD[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_PedLGneg() {
        String output;

        for (int adc = 0; adc < 12; adc++) {
            if (pedLGneg_read[show_MD][adc] == -1) {
                TextFieldPedLGnegREAD[adc].setText("Error");
                TextFieldPedLGnegREAD[adc].setForeground(Color.red);
            }
            output = String.format("%d", pedLGneg_read[show_MD][adc]);
            TextFieldPedLGnegREAD[adc].setText(output);
        }
    }

    /**
     *
     */
    private synchronized void show_All() {
        show_Switches();
        show_CIS_DAC();
        show_PedHGpos();
        show_PedHGneg();
        show_PedLGpos();
        show_PedLGneg();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        ComboBoxDBSide = new javax.swing.JComboBox<>();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        ButtonShowMD1 = new javax.swing.JButton();
        ButtonShowMD2 = new javax.swing.JButton();
        ButtonShowMD3 = new javax.swing.JButton();
        ButtonShowMD4 = new javax.swing.JButton();
        LabelMDShown = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        CheckBoxBroadcastMD = new javax.swing.JCheckBox();
        jPanel24 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        TextFieldSwitches0 = new javax.swing.JTextField();
        TextFieldSwitches1 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        TextFieldDAC0 = new javax.swing.JTextField();
        TextFieldPedHGpos0 = new javax.swing.JTextField();
        TextFieldPedHGneg0 = new javax.swing.JTextField();
        TextFieldDAC1 = new javax.swing.JTextField();
        TextFieldPedHGpos1 = new javax.swing.JTextField();
        TextFieldPedHGneg1 = new javax.swing.JTextField();
        TextFieldPedLGpos0 = new javax.swing.JTextField();
        TextFieldPedLGneg0 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        TextFieldPedLGneg1 = new javax.swing.JTextField();
        TextFieldPedLGpos1 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        TextFieldSwitches2 = new javax.swing.JTextField();
        TextFieldSwitches3 = new javax.swing.JTextField();
        TextFieldSwitches4 = new javax.swing.JTextField();
        TextFieldSwitches5 = new javax.swing.JTextField();
        TextFieldDAC2 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField30 = new javax.swing.JTextField();
        jTextField31 = new javax.swing.JTextField();
        TextFieldPedHGpos2 = new javax.swing.JTextField();
        TextFieldPedHGneg2 = new javax.swing.JTextField();
        TextFieldDAC3 = new javax.swing.JTextField();
        TextFieldDAC4 = new javax.swing.JTextField();
        TextFieldDAC5 = new javax.swing.JTextField();
        TextFieldPedHGpos3 = new javax.swing.JTextField();
        TextFieldPedHGneg3 = new javax.swing.JTextField();
        TextFieldPedHGpos4 = new javax.swing.JTextField();
        TextFieldPedHGpos5 = new javax.swing.JTextField();
        TextFieldPedHGneg5 = new javax.swing.JTextField();
        TextFieldPedLGpos2 = new javax.swing.JTextField();
        TextFieldPedLGneg2 = new javax.swing.JTextField();
        jTextField44 = new javax.swing.JTextField();
        jTextField45 = new javax.swing.JTextField();
        TextFieldPedLGpos3 = new javax.swing.JTextField();
        TextFieldPedLGneg3 = new javax.swing.JTextField();
        jTextField48 = new javax.swing.JTextField();
        jTextField49 = new javax.swing.JTextField();
        TextFieldPedHGneg4 = new javax.swing.JTextField();
        TextFieldPedLGneg4 = new javax.swing.JTextField();
        jTextField52 = new javax.swing.JTextField();
        TextFieldPedLGneg5 = new javax.swing.JTextField();
        jTextField54 = new javax.swing.JTextField();
        jTextField55 = new javax.swing.JTextField();
        TextFieldPedLGpos4 = new javax.swing.JTextField();
        TextFieldPedLGpos5 = new javax.swing.JTextField();
        jTextField58 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        ButtonWriteSwitches = new javax.swing.JButton();
        TextFieldSwitchesW = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        ButtonWriteCISDAC = new javax.swing.JButton();
        TextFieldCISDACWRITE = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        ButtonWritePedHGpos = new javax.swing.JButton();
        TextFieldPedHGposWRITE = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        ButtonWritePedHGneg = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        TextFieldPedHGnegWRITE = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        ButtonWritePedLGpos = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        TextFieldPedLGposWRITE = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        ButtonWritePedLGneg = new javax.swing.JButton();
        jLabel30 = new javax.swing.JLabel();
        TextFieldPedLGnegWRITE = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        ButtonWritetest1 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        ButtonWritetest2 = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        ButtonWritetest3 = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton42 = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        ButtonReadSwitches = new javax.swing.JButton();
        ButtonReadCISDAC = new javax.swing.JButton();
        ButtonReadPedHGpos = new javax.swing.JButton();
        ButtonReadPedHGneg = new javax.swing.JButton();
        ButtonReadPedLGpos = new javax.swing.JButton();
        ButtonReadPedLGneg = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        LabelWriteAllFEBs = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        TextFieldSwitches6 = new javax.swing.JTextField();
        TextFieldSwitches7 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        TextFieldDAC6 = new javax.swing.JTextField();
        TextFieldPedHGpos6 = new javax.swing.JTextField();
        TextFieldPedHGneg6 = new javax.swing.JTextField();
        TextFieldDAC7 = new javax.swing.JTextField();
        TextFieldPedHGpos7 = new javax.swing.JTextField();
        TextFieldPedHGneg7 = new javax.swing.JTextField();
        TextFieldPedLGpos6 = new javax.swing.JTextField();
        TextFieldPedLGneg6 = new javax.swing.JTextField();
        jTextField61 = new javax.swing.JTextField();
        TextFieldPedLGneg7 = new javax.swing.JTextField();
        TextFieldPedLGpos7 = new javax.swing.JTextField();
        jTextField64 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        TextFieldSwitches8 = new javax.swing.JTextField();
        TextFieldSwitches9 = new javax.swing.JTextField();
        TextFieldSwitches10 = new javax.swing.JTextField();
        TextFieldSwitches11 = new javax.swing.JTextField();
        TextFieldDAC8 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField66 = new javax.swing.JTextField();
        jTextField67 = new javax.swing.JTextField();
        TextFieldPedHGpos8 = new javax.swing.JTextField();
        TextFieldPedHGneg8 = new javax.swing.JTextField();
        TextFieldDAC9 = new javax.swing.JTextField();
        TextFieldDAC10 = new javax.swing.JTextField();
        TextFieldDAC11 = new javax.swing.JTextField();
        TextFieldPedHGpos9 = new javax.swing.JTextField();
        TextFieldPedHGneg9 = new javax.swing.JTextField();
        TextFieldPedHGpos10 = new javax.swing.JTextField();
        TextFieldPedHGpos11 = new javax.swing.JTextField();
        TextFieldPedHGneg11 = new javax.swing.JTextField();
        TextFieldPedLGpos8 = new javax.swing.JTextField();
        TextFieldPedLGneg8 = new javax.swing.JTextField();
        jTextField80 = new javax.swing.JTextField();
        jTextField81 = new javax.swing.JTextField();
        TextFieldPedLGpos9 = new javax.swing.JTextField();
        TextFieldPedLGneg9 = new javax.swing.JTextField();
        jTextField84 = new javax.swing.JTextField();
        jTextField85 = new javax.swing.JTextField();
        TextFieldPedHGneg10 = new javax.swing.JTextField();
        TextFieldPedLGneg10 = new javax.swing.JTextField();
        jTextField88 = new javax.swing.JTextField();
        TextFieldPedLGneg11 = new javax.swing.JTextField();
        jTextField90 = new javax.swing.JTextField();
        jTextField91 = new javax.swing.JTextField();
        TextFieldPedLGpos10 = new javax.swing.JTextField();
        TextFieldPedLGpos11 = new javax.swing.JTextField();
        jTextField94 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField95 = new javax.swing.JTextField();
        jTextField96 = new javax.swing.JTextField();
        jTextField97 = new javax.swing.JTextField();
        jTextField98 = new javax.swing.JTextField();
        jTextField99 = new javax.swing.JTextField();
        jTextField100 = new javax.swing.JTextField();

        java.awt.GridBagLayout jPanel4Layout1 = new java.awt.GridBagLayout();
        jPanel4Layout1.columnWidths = new int[] {0};
        jPanel4Layout1.rowHeights = new int[] {0};
        jPanel4.setLayout(jPanel4Layout1);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0};
        jPanel2Layout.rowHeights = new int[] {0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
        jPanel2.setLayout(jPanel2Layout);

        java.awt.GridBagLayout jPanel16Layout1 = new java.awt.GridBagLayout();
        jPanel16Layout1.columnWidths = new int[] {0, 20, 0, 20, 0};
        jPanel16Layout1.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout1);

        java.awt.GridBagLayout jPanel17Layout2 = new java.awt.GridBagLayout();
        jPanel17Layout2.columnWidths = new int[] {0};
        jPanel17Layout2.rowHeights = new int[] {0, 5, 0};
        jPanel17.setLayout(jPanel17Layout2);

        jLabel40.setText("DB FPGA Side");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel40, gridBagConstraints);

        ComboBoxDBSide.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Broadcast", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel17.add(ComboBoxDBSide, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel17, gridBagConstraints);

        jPanel18.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel19Layout1 = new java.awt.GridBagLayout();
        jPanel19Layout1.columnWidths = new int[] {0, 30, 0, 30, 0, 30, 0, 30, 0};
        jPanel19Layout1.rowHeights = new int[] {0, 5, 0};
        jPanel19.setLayout(jPanel19Layout1);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("MD1");
        CheckBoxMD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setText("MD2");
        CheckBoxMD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel19.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setText("MD3");
        CheckBoxMD3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel19.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setText("MD4");
        CheckBoxMD4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel19.add(CheckBoxMD4, gridBagConstraints);

        ButtonShowMD1.setText("Show MD1");
        ButtonShowMD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowMD1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonShowMD1, gridBagConstraints);

        ButtonShowMD2.setText("Show MD2");
        ButtonShowMD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowMD2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonShowMD2, gridBagConstraints);

        ButtonShowMD3.setText("Show MD3");
        ButtonShowMD3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowMD3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonShowMD3, gridBagConstraints);

        ButtonShowMD4.setText("Show MD4");
        ButtonShowMD4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowMD4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel19.add(ButtonShowMD4, gridBagConstraints);

        LabelMDShown.setText("Data for ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel19.add(LabelMDShown, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel19, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel18, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        CheckBoxBroadcastMD.setText("MD Broadcast");
        CheckBoxBroadcastMD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxBroadcastMDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(CheckBoxBroadcastMD, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jPanel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel16, gridBagConstraints);

        java.awt.GridBagLayout jPanel24Layout3 = new java.awt.GridBagLayout();
        jPanel24Layout3.columnWidths = new int[] {0};
        jPanel24Layout3.rowHeights = new int[] {0};
        jPanel24.setLayout(jPanel24Layout3);

        java.awt.GridBagLayout jPanel30Layout = new java.awt.GridBagLayout();
        jPanel30Layout.columnWidths = new int[] {0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0};
        jPanel30Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel30.setLayout(jPanel30Layout);

        jLabel21.setText("FEB 0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel21, gridBagConstraints);

        jLabel36.setText("FEB 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel36, gridBagConstraints);

        TextFieldSwitches0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches0.setText("0");
        TextFieldSwitches0.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches0, gridBagConstraints);

        TextFieldSwitches1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches1.setText("0");
        TextFieldSwitches1.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches1, gridBagConstraints);

        jLabel15.setText("Switches:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel15, gridBagConstraints);

        jLabel14.setText("CIS DAC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel14, gridBagConstraints);

        jLabel16.setText("Ped HG pos:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel16, gridBagConstraints);

        jLabel17.setText("Ped HG neg:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel17, gridBagConstraints);

        jLabel18.setText("Ped LG pos:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel18, gridBagConstraints);

        jLabel19.setText("Ped LG neg:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel19, gridBagConstraints);

        jLabel37.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel37, gridBagConstraints);

        TextFieldDAC0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC0.setText("0");
        TextFieldDAC0.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC0, gridBagConstraints);

        TextFieldPedHGpos0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos0.setText("0");
        TextFieldPedHGpos0.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos0, gridBagConstraints);

        TextFieldPedHGneg0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg0.setText("0");
        TextFieldPedHGneg0.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg0, gridBagConstraints);

        TextFieldDAC1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC1.setText("0");
        TextFieldDAC1.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC1, gridBagConstraints);

        TextFieldPedHGpos1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos1.setText("0");
        TextFieldPedHGpos1.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos1, gridBagConstraints);

        TextFieldPedHGneg1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg1.setText("0");
        TextFieldPedHGneg1.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg1, gridBagConstraints);

        TextFieldPedLGpos0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos0.setText("0");
        TextFieldPedLGpos0.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos0, gridBagConstraints);

        TextFieldPedLGneg0.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg0.setText("0");
        TextFieldPedLGneg0.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg0, gridBagConstraints);

        jTextField11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField11.setText("0");
        jTextField11.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField11, gridBagConstraints);

        TextFieldPedLGneg1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg1.setText("0");
        TextFieldPedLGneg1.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg1, gridBagConstraints);

        TextFieldPedLGpos1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos1.setText("0");
        TextFieldPedLGpos1.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos1, gridBagConstraints);

        jTextField14.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField14.setText("0");
        jTextField14.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField14, gridBagConstraints);

        jLabel1.setText("FEF 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel1, gridBagConstraints);

        jLabel2.setText("FEB 3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel2, gridBagConstraints);

        jLabel3.setText("FEB 4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel3, gridBagConstraints);

        jLabel4.setText("FEB 5");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel30.add(jLabel4, gridBagConstraints);

        TextFieldSwitches2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches2.setText("0");
        TextFieldSwitches2.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches2, gridBagConstraints);

        TextFieldSwitches3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches3.setText("0");
        TextFieldSwitches3.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches3, gridBagConstraints);

        TextFieldSwitches4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches4.setText("0");
        TextFieldSwitches4.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches4, gridBagConstraints);

        TextFieldSwitches5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches5.setText("0");
        TextFieldSwitches5.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel30.add(TextFieldSwitches5, gridBagConstraints);

        TextFieldDAC2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC2.setText("0");
        TextFieldDAC2.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC2, gridBagConstraints);

        jLabel9.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel9, gridBagConstraints);

        jTextField30.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField30.setText("0");
        jTextField30.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField30, gridBagConstraints);

        jTextField31.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField31.setText("0");
        jTextField31.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField31, gridBagConstraints);

        TextFieldPedHGpos2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos2.setText("0");
        TextFieldPedHGpos2.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos2, gridBagConstraints);

        TextFieldPedHGneg2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg2.setText("0");
        TextFieldPedHGneg2.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg2, gridBagConstraints);

        TextFieldDAC3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC3.setText("0");
        TextFieldDAC3.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC3, gridBagConstraints);

        TextFieldDAC4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC4.setText("0");
        TextFieldDAC4.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC4, gridBagConstraints);

        TextFieldDAC5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC5.setText("0");
        TextFieldDAC5.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel30.add(TextFieldDAC5, gridBagConstraints);

        TextFieldPedHGpos3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos3.setText("0");
        TextFieldPedHGpos3.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos3, gridBagConstraints);

        TextFieldPedHGneg3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg3.setText("0");
        TextFieldPedHGneg3.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg3, gridBagConstraints);

        TextFieldPedHGpos4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos4.setText("0");
        TextFieldPedHGpos4.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos4, gridBagConstraints);

        TextFieldPedHGpos5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos5.setText("0");
        TextFieldPedHGpos5.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel30.add(TextFieldPedHGpos5, gridBagConstraints);

        TextFieldPedHGneg5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg5.setText("0");
        TextFieldPedHGneg5.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg5, gridBagConstraints);

        TextFieldPedLGpos2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos2.setText("0");
        TextFieldPedLGpos2.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos2, gridBagConstraints);

        TextFieldPedLGneg2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg2.setText("0");
        TextFieldPedLGneg2.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg2, gridBagConstraints);

        jTextField44.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField44.setText("0");
        jTextField44.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField44, gridBagConstraints);

        jTextField45.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField45.setText("0");
        jTextField45.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField45, gridBagConstraints);

        TextFieldPedLGpos3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos3.setText("0");
        TextFieldPedLGpos3.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos3, gridBagConstraints);

        TextFieldPedLGneg3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg3.setText("0");
        TextFieldPedLGneg3.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg3, gridBagConstraints);

        jTextField48.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField48.setText("0");
        jTextField48.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField48, gridBagConstraints);

        jTextField49.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField49.setText("0");
        jTextField49.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField49, gridBagConstraints);

        TextFieldPedHGneg4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg4.setText("0");
        TextFieldPedHGneg4.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel30.add(TextFieldPedHGneg4, gridBagConstraints);

        TextFieldPedLGneg4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg4.setText("0");
        TextFieldPedLGneg4.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg4, gridBagConstraints);

        jTextField52.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField52.setText("0");
        jTextField52.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField52, gridBagConstraints);

        TextFieldPedLGneg5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg5.setText("0");
        TextFieldPedLGneg5.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 12;
        jPanel30.add(TextFieldPedLGneg5, gridBagConstraints);

        jTextField54.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField54.setText("0");
        jTextField54.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField54, gridBagConstraints);

        jTextField55.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField55.setText("0");
        jTextField55.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jTextField55, gridBagConstraints);

        TextFieldPedLGpos4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos4.setText("0");
        TextFieldPedLGpos4.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos4, gridBagConstraints);

        TextFieldPedLGpos5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos5.setText("0");
        TextFieldPedLGpos5.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 10;
        jPanel30.add(TextFieldPedLGpos5, gridBagConstraints);

        jTextField58.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField58.setText("0");
        jTextField58.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jTextField58, gridBagConstraints);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel5Layout.rowHeights = new int[] {0};
        jPanel5.setLayout(jPanel5Layout);

        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel28.setText("Switches:");
        jLabel28.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jLabel28, gridBagConstraints);

        ButtonWriteSwitches.setText("Write:");
        ButtonWriteSwitches.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteSwitchesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel5.add(ButtonWriteSwitches, gridBagConstraints);

        TextFieldSwitchesW.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitchesW.setText("0");
        TextFieldSwitchesW.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel5.add(TextFieldSwitchesW, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel30.add(jPanel5, gridBagConstraints);

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel3Layout.rowHeights = new int[] {0};
        jPanel3.setLayout(jPanel3Layout);

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel42.setText("CIS DAC:");
        jLabel42.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel42, gridBagConstraints);

        ButtonWriteCISDAC.setText("Write");
        ButtonWriteCISDAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWriteCISDACActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel3.add(ButtonWriteCISDAC, gridBagConstraints);

        TextFieldCISDACWRITE.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldCISDACWRITE.setText("0");
        TextFieldCISDACWRITE.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(TextFieldCISDACWRITE, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel30.add(jPanel3, gridBagConstraints);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel6Layout.rowHeights = new int[] {0};
        jPanel6.setLayout(jPanel6Layout);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel43.setText("Ped HG pos:");
        jLabel43.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel43, gridBagConstraints);

        ButtonWritePedHGpos.setText("Write");
        ButtonWritePedHGpos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritePedHGposActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel6.add(ButtonWritePedHGpos, gridBagConstraints);

        TextFieldPedHGposWRITE.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGposWRITE.setText("0");
        TextFieldPedHGposWRITE.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel6.add(TextFieldPedHGposWRITE, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 6;
        jPanel30.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel7Layout.rowHeights = new int[] {0};
        jPanel7.setLayout(jPanel7Layout);

        ButtonWritePedHGneg.setText("Write");
        ButtonWritePedHGneg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritePedHGnegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ButtonWritePedHGneg, gridBagConstraints);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel44.setText("Ped HG neg:");
        jLabel44.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel44, gridBagConstraints);

        TextFieldPedHGnegWRITE.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGnegWRITE.setText("0");
        TextFieldPedHGnegWRITE.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldPedHGnegWRITE, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 8;
        jPanel30.add(jPanel7, gridBagConstraints);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel9Layout.rowHeights = new int[] {0};
        jPanel9.setLayout(jPanel9Layout);

        ButtonWritePedLGpos.setText("Write");
        ButtonWritePedLGpos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritePedLGposActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel9.add(ButtonWritePedLGpos, gridBagConstraints);

        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("Ped LG pos:");
        jLabel29.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel9.add(jLabel29, gridBagConstraints);

        TextFieldPedLGposWRITE.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGposWRITE.setText("0");
        TextFieldPedLGposWRITE.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldPedLGposWRITE, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 10;
        jPanel30.add(jPanel9, gridBagConstraints);

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel11Layout.rowHeights = new int[] {0};
        jPanel11.setLayout(jPanel11Layout);

        ButtonWritePedLGneg.setText("Write");
        ButtonWritePedLGneg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritePedLGnegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel11.add(ButtonWritePedLGneg, gridBagConstraints);

        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel30.setText("Ped LG neg:");
        jLabel30.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(jLabel30, gridBagConstraints);

        TextFieldPedLGnegWRITE.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGnegWRITE.setText("0");
        TextFieldPedLGnegWRITE.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel11.add(TextFieldPedLGnegWRITE, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 12;
        jPanel30.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel12Layout.rowHeights = new int[] {0};
        jPanel12.setLayout(jPanel12Layout);

        ButtonWritetest1.setText("Write");
        ButtonWritetest1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritetest1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel12.add(ButtonWritetest1, gridBagConstraints);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("test:");
        jLabel31.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jLabel31, gridBagConstraints);

        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField10.setText("0");
        jTextField10.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jTextField10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jPanel12, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        ButtonWritetest2.setText("Write");
        ButtonWritetest2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritetest2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonWritetest2, gridBagConstraints);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel32.setText("test:");
        jLabel32.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jLabel32, gridBagConstraints);

        jTextField21.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField21.setText("0");
        jTextField21.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(jTextField21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jPanel13, gridBagConstraints);

        jLabel5.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel30.add(jLabel5, gridBagConstraints);

        jTextField15.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField15.setText("0");
        jTextField15.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField15, gridBagConstraints);

        jTextField16.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField16.setText("0");
        jTextField16.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField16, gridBagConstraints);

        jTextField17.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField17.setText("0");
        jTextField17.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField17, gridBagConstraints);

        jTextField18.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField18.setText("0");
        jTextField18.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField18, gridBagConstraints);

        jTextField19.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField19.setText("0");
        jTextField19.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField19, gridBagConstraints);

        jTextField20.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField20.setText("0");
        jTextField20.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jTextField20, gridBagConstraints);

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel14Layout.rowHeights = new int[] {0};
        jPanel14.setLayout(jPanel14Layout);

        ButtonWritetest3.setText("Write");
        ButtonWritetest3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonWritetest3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel14.add(ButtonWritetest3, gridBagConstraints);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("test:");
        jLabel33.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jLabel33, gridBagConstraints);

        jTextField22.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField22.setText("0");
        jTextField22.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jTextField22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jPanel14, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        jPanel8Layout.rowHeights = new int[] {0};
        jPanel8.setLayout(jPanel8Layout);

        jButton1.setText("Read All");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jButton1, gridBagConstraints);

        jButton42.setText("Write All");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jButton42, gridBagConstraints);

        jLabel34.setPreferredSize(new java.awt.Dimension(80, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jLabel34, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 22;
        jPanel30.add(jPanel15, gridBagConstraints);

        ButtonReadSwitches.setText("Read");
        ButtonReadSwitches.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadSwitchesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel30.add(ButtonReadSwitches, gridBagConstraints);

        ButtonReadCISDAC.setText("Read");
        ButtonReadCISDAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadCISDACActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel30.add(ButtonReadCISDAC, gridBagConstraints);

        ButtonReadPedHGpos.setText("Read");
        ButtonReadPedHGpos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadPedHGposActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 6;
        jPanel30.add(ButtonReadPedHGpos, gridBagConstraints);

        ButtonReadPedHGneg.setText("Read");
        ButtonReadPedHGneg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadPedHGnegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 8;
        jPanel30.add(ButtonReadPedHGneg, gridBagConstraints);

        ButtonReadPedLGpos.setText("Read");
        ButtonReadPedLGpos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadPedLGposActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 10;
        jPanel30.add(ButtonReadPedLGpos, gridBagConstraints);

        ButtonReadPedLGneg.setText("Read");
        ButtonReadPedLGneg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonReadPedLGnegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 12;
        jPanel30.add(ButtonReadPedLGneg, gridBagConstraints);

        jButton8.setText("Read");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 14;
        jPanel30.add(jButton8, gridBagConstraints);

        jButton10.setText("Read");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 16;
        jPanel30.add(jButton10, gridBagConstraints);

        jButton12.setText("Read");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 18;
        jPanel30.add(jButton12, gridBagConstraints);

        LabelWriteAllFEBs.setText("Write all FEBs for all active MDs");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel30.add(LabelWriteAllFEBs, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jPanel30, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel24, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel31Layout1 = new java.awt.GridBagLayout();
        jPanel31Layout1.columnWidths = new int[] {0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0, 20, 0};
        jPanel31Layout1.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel31.setLayout(jPanel31Layout1);

        jLabel22.setText("FEB 6");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel22, gridBagConstraints);

        jLabel38.setText("FEB 7");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel38, gridBagConstraints);

        TextFieldSwitches6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches6.setText("0");
        TextFieldSwitches6.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches6, gridBagConstraints);

        TextFieldSwitches7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches7.setText("0");
        TextFieldSwitches7.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches7, gridBagConstraints);

        jLabel20.setText("Switch:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel20, gridBagConstraints);

        jLabel23.setText("CIS DAC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel23, gridBagConstraints);

        jLabel24.setText("Ped HG pos:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel24, gridBagConstraints);

        jLabel25.setText("Ped HG neg:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel25, gridBagConstraints);

        jLabel26.setText("Ped LG pos:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel26, gridBagConstraints);

        jLabel27.setText("Ped LG neg:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        jPanel31.add(jLabel27, gridBagConstraints);

        jLabel39.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel39, gridBagConstraints);

        TextFieldDAC6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC6.setText("0");
        TextFieldDAC6.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC6, gridBagConstraints);

        TextFieldPedHGpos6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos6.setText("0");
        TextFieldPedHGpos6.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos6, gridBagConstraints);

        TextFieldPedHGneg6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg6.setText("0");
        TextFieldPedHGneg6.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg6, gridBagConstraints);

        TextFieldDAC7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC7.setText("0");
        TextFieldDAC7.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC7, gridBagConstraints);

        TextFieldPedHGpos7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos7.setText("0");
        TextFieldPedHGpos7.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos7, gridBagConstraints);

        TextFieldPedHGneg7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg7.setText("0");
        TextFieldPedHGneg7.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg7, gridBagConstraints);

        TextFieldPedLGpos6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos6.setText("0");
        TextFieldPedLGpos6.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos6, gridBagConstraints);

        TextFieldPedLGneg6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg6.setText("0");
        TextFieldPedLGneg6.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg6, gridBagConstraints);

        jTextField61.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField61.setText("0");
        jTextField61.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField61, gridBagConstraints);

        TextFieldPedLGneg7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg7.setText("0");
        TextFieldPedLGneg7.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg7, gridBagConstraints);

        TextFieldPedLGpos7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos7.setText("0");
        TextFieldPedLGpos7.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos7, gridBagConstraints);

        jTextField64.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField64.setText("0");
        jTextField64.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField64, gridBagConstraints);

        jLabel6.setText("FEB 8");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel6, gridBagConstraints);

        jLabel7.setText("FEB 9");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel7, gridBagConstraints);

        jLabel8.setText("FEB 10");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel8, gridBagConstraints);

        jLabel10.setText("FEB 11");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel31.add(jLabel10, gridBagConstraints);

        TextFieldSwitches8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches8.setText("0");
        TextFieldSwitches8.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches8, gridBagConstraints);

        TextFieldSwitches9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches9.setText("0");
        TextFieldSwitches9.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches9, gridBagConstraints);

        TextFieldSwitches10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches10.setText("0");
        TextFieldSwitches10.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches10, gridBagConstraints);

        TextFieldSwitches11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSwitches11.setText("0");
        TextFieldSwitches11.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel31.add(TextFieldSwitches11, gridBagConstraints);

        TextFieldDAC8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC8.setText("0");
        TextFieldDAC8.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC8, gridBagConstraints);

        jLabel11.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel11, gridBagConstraints);

        jTextField66.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField66.setText("0");
        jTextField66.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField66, gridBagConstraints);

        jTextField67.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField67.setText("0");
        jTextField67.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField67, gridBagConstraints);

        TextFieldPedHGpos8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos8.setText("0");
        TextFieldPedHGpos8.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos8, gridBagConstraints);

        TextFieldPedHGneg8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg8.setText("0");
        TextFieldPedHGneg8.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg8, gridBagConstraints);

        TextFieldDAC9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC9.setText("0");
        TextFieldDAC9.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC9, gridBagConstraints);

        TextFieldDAC10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC10.setText("0");
        TextFieldDAC10.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC10, gridBagConstraints);

        TextFieldDAC11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDAC11.setText("0");
        TextFieldDAC11.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel31.add(TextFieldDAC11, gridBagConstraints);

        TextFieldPedHGpos9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos9.setText("0");
        TextFieldPedHGpos9.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos9, gridBagConstraints);

        TextFieldPedHGneg9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg9.setText("0");
        TextFieldPedHGneg9.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg9, gridBagConstraints);

        TextFieldPedHGpos10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos10.setText("0");
        TextFieldPedHGpos10.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos10, gridBagConstraints);

        TextFieldPedHGpos11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGpos11.setText("0");
        TextFieldPedHGpos11.setPreferredSize(new java.awt.Dimension(94, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 6;
        jPanel31.add(TextFieldPedHGpos11, gridBagConstraints);

        TextFieldPedHGneg11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg11.setText("0");
        TextFieldPedHGneg11.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg11, gridBagConstraints);

        TextFieldPedLGpos8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos8.setText("0");
        TextFieldPedLGpos8.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos8, gridBagConstraints);

        TextFieldPedLGneg8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg8.setText("0");
        TextFieldPedLGneg8.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg8, gridBagConstraints);

        jTextField80.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField80.setText("0");
        jTextField80.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField80, gridBagConstraints);

        jTextField81.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField81.setText("0");
        jTextField81.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField81, gridBagConstraints);

        TextFieldPedLGpos9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos9.setText("0");
        TextFieldPedLGpos9.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos9, gridBagConstraints);

        TextFieldPedLGneg9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg9.setText("0");
        TextFieldPedLGneg9.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg9, gridBagConstraints);

        jTextField84.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField84.setText("0");
        jTextField84.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField84, gridBagConstraints);

        jTextField85.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField85.setText("0");
        jTextField85.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField85, gridBagConstraints);

        TextFieldPedHGneg10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedHGneg10.setText("0");
        TextFieldPedHGneg10.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 8;
        jPanel31.add(TextFieldPedHGneg10, gridBagConstraints);

        TextFieldPedLGneg10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg10.setText("0");
        TextFieldPedLGneg10.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg10, gridBagConstraints);

        jTextField88.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField88.setText("0");
        jTextField88.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField88, gridBagConstraints);

        TextFieldPedLGneg11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGneg11.setText("0");
        TextFieldPedLGneg11.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 12;
        jPanel31.add(TextFieldPedLGneg11, gridBagConstraints);

        jTextField90.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField90.setText("0");
        jTextField90.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField90, gridBagConstraints);

        jTextField91.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField91.setText("0");
        jTextField91.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 16;
        jPanel31.add(jTextField91, gridBagConstraints);

        TextFieldPedLGpos10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos10.setText("0");
        TextFieldPedLGpos10.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos10, gridBagConstraints);

        TextFieldPedLGpos11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPedLGpos11.setText("0");
        TextFieldPedLGpos11.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 10;
        jPanel31.add(TextFieldPedLGpos11, gridBagConstraints);

        jTextField94.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField94.setText("0");
        jTextField94.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 14;
        jPanel31.add(jTextField94, gridBagConstraints);

        jLabel12.setText("test:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 18;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel31.add(jLabel12, gridBagConstraints);

        jTextField95.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField95.setText("0");
        jTextField95.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField95, gridBagConstraints);

        jTextField96.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField96.setText("0");
        jTextField96.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField96, gridBagConstraints);

        jTextField97.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField97.setText("0");
        jTextField97.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField97, gridBagConstraints);

        jTextField98.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField98.setText("0");
        jTextField98.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField98, gridBagConstraints);

        jTextField99.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField99.setText("0");
        jTextField99.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField99, gridBagConstraints);

        jTextField100.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField100.setText("0");
        jTextField100.setPreferredSize(new java.awt.Dimension(90, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 18;
        jPanel31.add(jTextField100, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jPanel31, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel2.add(jPanel1, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 1112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Writes FEB switches values for all FEBs as taken from JTextlabel.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteSwitchesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteSwitchesActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteSwitches);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteSwitchesActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteSwitchesActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteSwitchesActionPerformed

    /**
     * Writes CIS DAC values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWriteCISDACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWriteCISDACActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWriteCISDAC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWriteDACActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWriteDACActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWriteCISDACActionPerformed

    /**
     * Writes Pedestal HG positive values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWritePedHGposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritePedHGposActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWritePedHGpos);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWritePedHGposActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWritePedHGposActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWritePedHGposActionPerformed

    /**
     * Writes Pedestal HG negative values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWritePedHGnegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritePedHGnegActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWritePedHGneg);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWritePedHGnegActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWritePedHGnegActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWritePedHGnegActionPerformed

    /**
     * Writes Pedestal LG positive values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWritePedLGposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritePedLGposActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWritePedLGpos);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWritePedLGposActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWritePedLGposActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWritePedLGposActionPerformed

    /**
     * Writes Pedestal LG negative values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWritetest1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritetest1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonWritetest1ActionPerformed

    private void ButtonWritetest2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritetest2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonWritetest2ActionPerformed

    private void ButtonWritetest3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritetest3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ButtonWritetest3ActionPerformed

    /**
     * Shows data read for all FEBs of MD1.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowMD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowMD1ActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        String fs = "PPrDemoFE.Show MD1 -> is selected: %s";
        Console.print_cr(String.format(fs,
                (selected_MD.get(0) == 1 ? "True" : "False")));

        if (selected_MD.get(0) == 0) {
            return;
        }

        LabelMDShown.setText("Data read shown for MD1");

        ButtonShowMD1.setBackground(Color.GRAY);
        ButtonShowMD2.setBackground(null);
        ButtonShowMD3.setBackground(null);
        ButtonShowMD4.setBackground(null);

        show_MD = 0;

        show_All();
    }//GEN-LAST:event_ButtonShowMD1ActionPerformed

    /**
     * Shows data read for all FEBs of MD2.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowMD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowMD2ActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        String fs = "PPrDemoFE.Show MD2 -> is selected: %s";
        Console.print_cr(String.format(fs,
                (selected_MD.get(1) == 1 ? "True" : "False")));

        if (selected_MD.get(1) == 0) {
            return;
        }

        LabelMDShown.setText("Data read shown for MD2");

        ButtonShowMD2.setBackground(Color.GRAY);
        ButtonShowMD1.setBackground(null);
        ButtonShowMD3.setBackground(null);
        ButtonShowMD4.setBackground(null);

        show_MD = 1;

        show_All();
    }//GEN-LAST:event_ButtonShowMD2ActionPerformed

    /**
     * Shows data read for all FEBs of MD3.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowMD3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowMD3ActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        String fs = "PPrDemoFE.Show MD3 -> is selected: %s";
        Console.print_cr(String.format(fs,
                (selected_MD.get(2) == 1 ? "True" : "False")));

        if (selected_MD.get(2) == 0) {
            return;
        }

        LabelMDShown.setText("Data read shown for MD3");

        ButtonShowMD3.setBackground(Color.GRAY);
        ButtonShowMD1.setBackground(null);
        ButtonShowMD2.setBackground(null);
        ButtonShowMD4.setBackground(null);

        show_MD = 2;

        show_All();
    }//GEN-LAST:event_ButtonShowMD3ActionPerformed

    /**
     * Shows data read for all FEBs of MD4.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowMD4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowMD4ActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        String fs = "PPrDemoFE.Show MD4 -> is selected: %s";
        Console.print_cr(String.format(fs,
                (selected_MD.get(3) == 1 ? "True" : "False")));

        if (selected_MD.get(3) == 0) {
            return;
        }

        LabelMDShown.setText("Data read shown for MD4");

        ButtonShowMD4.setBackground(Color.GRAY);
        ButtonShowMD1.setBackground(null);
        ButtonShowMD2.setBackground(null);
        ButtonShowMD3.setBackground(null);

        show_MD = 3;

        show_All();
    }//GEN-LAST:event_ButtonShowMD4ActionPerformed

    /**
     * Writes Pedestal LG negative values for all FEBs from JTextlabels.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonWritePedLGnegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonWritePedLGnegActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableWritePedLGneg);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonWritePedLGnegActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonWritePedLGnegActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonWritePedLGnegActionPerformed

    /**
     * Set MD1 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD1ActionPerformed
        selected_MD.set(0, CheckBoxMD1.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD1ActionPerformed

    /**
     * Set MD2 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD2ActionPerformed
        selected_MD.set(1, CheckBoxMD2.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD2ActionPerformed

    /**
     * Set MD3 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD3ActionPerformed
        selected_MD.set(2, CheckBoxMD3.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD3ActionPerformed

    /**
     * Set MD4 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD4ActionPerformed
        selected_MD.set(3, CheckBoxMD4.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD4ActionPerformed

    /**
     * Reads Switches data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadSwitchesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadSwitchesActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadSwitches);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadSwitchesActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadSwitchesActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadSwitchesActionPerformed

    /**
     * Reads CIS to DAC data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadCISDACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadCISDACActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadCISDAC);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadDACActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadDACActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadCISDACActionPerformed

    /**
     * Reads Ped HG positive data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadPedHGposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadPedHGposActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadPedHGpos);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadPedHGposActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadPedHGposActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadPedHGposActionPerformed

    /**
     * Sets minidrawers to process data in broadcast mode. selection.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxBroadcastMDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxBroadcastMDActionPerformed
        if (CheckBoxBroadcastMD.isSelected()) {
            broadcast_MD = 1;
        } else {
            broadcast_MD = 0;
        }
    }//GEN-LAST:event_CheckBoxBroadcastMDActionPerformed

    /**
     * Reads Ped LG positive data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadPedLGposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadPedLGposActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadPedLGpos);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadPedLGposActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadPedLGposActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadPedLGposActionPerformed

    /**
     * Reads Ped HG negative data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadPedHGnegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadPedHGnegActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadPedHGneg);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadPedHGnegActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadPedHGnegActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadPedHGnegActionPerformed

    /**
     * Reads Ped LG negative data for all FEBs of all active MDs.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonReadPedLGnegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonReadPedLGnegActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("PPrDemoFE -> no IPbus PPr connection");
            return;
        }

        // Executes the given task.
        try {
            executor.execute(RunnableReadPedLGneg);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonReadPedLGnegActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonReadPedLGnegActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonReadPedLGnegActionPerformed

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares a ThreadPoolExecutor object. It executes each submitted task
     * using one of possibly several pooled threads, normally configured using
     * Executors factory methods.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for write_Switches() method.
     */
    Runnable RunnableWriteSwitches;

    /**
     * Declares Runnable for write_CIS_DAC() method.
     */
    Runnable RunnableWriteCISDAC;

    /**
     * Declares Runnable for write_PedHGpos() method.
     */
    Runnable RunnableWritePedHGpos;

    /**
     * Declares Runnable for write_PedHGneg() method.
     */
    Runnable RunnableWritePedHGneg;

    /**
     * Declares Runnable for write_PedLGpos() method.
     */
    Runnable RunnableWritePedLGpos;

    /**
     * Declares Runnable for write_PedLGneg() method.
     */
    Runnable RunnableWritePedLGneg;

    /**
     * Declares Runnable for read_Switches() method.
     */
    Runnable RunnableReadSwitches;

    /**
     * Declares Runnable for read_CIS_DAC() method.
     */
    Runnable RunnableReadCISDAC;

    /**
     * Declares Runnable for read_PedHGpos method.
     */
    Runnable RunnableReadPedHGpos;

    /**
     * Declares Runnable for read_PedHGneg method.
     */
    Runnable RunnableReadPedHGneg;

    /**
     * Declares Runnable for read_PedLGpos method.
     */
    Runnable RunnableReadPedLGpos;

    /**
     * Declares Runnable for read_PedLGneg method.
     */
    Runnable RunnableReadPedLGneg;

    /**
     * ArrayList with selected minidrawers to be filled with data.
     */
    private final ArrayList<Integer> selected_MD;

    /**
     * Total selected minidrawers.
     */
    private Integer selected_MD_tot;

    /**
     * Minidrawers broadcast flag.
     */
    private Integer broadcast_MD;

    /**
     * Actice minidrawer to show data read.
     */
    private Integer show_MD;

    /**
     * Selected Daughterboard FPGA side.
     */
    private Integer dbside;

    /**
     * Selected Daughterboard FPGA side (as String).
     */
    private String dbsidestring;

    /**
     * Declares JTextField array with the JTextField Switches value for each
     * FEB.
     */
    private static javax.swing.JTextField[] TextFieldSwitches;

    /**
     * Declares JTextField array with the JTextField CIS DAC value for each FEB.
     */
    private static javax.swing.JTextField[] TextFieldCISDACREAD;

    /**
     * Declares JTextField array with the JTextField Ped HG pos value for each
     * FEB.
     */
    private static javax.swing.JTextField[] TextFieldPedHGposREAD;

    /**
     * Declares JTextField array with the JTextField Ped HG neg value for each
     * FEB.
     */
    private static javax.swing.JTextField[] TextFieldPedHGnegREAD;

    /**
     * Declares JTextField array with the JTextField Ped LG pos value for each
     * FEB.
     */
    private static javax.swing.JTextField[] TextFieldPedLGposREAD;

    /**
     * Declares JTextField array with the JTextField Ped LG neg value for each
     * FEB.
     */
    private static javax.swing.JTextField[] TextFieldPedLGnegREAD;

    private Integer switches_write;
    private Integer CIS_DAC_write;
    private Integer pedHGpos_write;
    private Integer pedHGneg_write;
    private Integer pedLGpos_write;
    private Integer pedLGneg_write;

    private final Integer[][] Switches_read;
    private Integer[][] CIS_DAC_read;
    private Integer[][] pedHGpos_read;
    private Integer[][] pedHGneg_read;
    private Integer[][] pedLGpos_read;
    private Integer[][] pedLGneg_read;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonReadCISDAC;
    private javax.swing.JButton ButtonReadPedHGneg;
    private javax.swing.JButton ButtonReadPedHGpos;
    private javax.swing.JButton ButtonReadPedLGneg;
    private javax.swing.JButton ButtonReadPedLGpos;
    private javax.swing.JButton ButtonReadSwitches;
    private javax.swing.JButton ButtonShowMD1;
    private javax.swing.JButton ButtonShowMD2;
    private javax.swing.JButton ButtonShowMD3;
    private javax.swing.JButton ButtonShowMD4;
    private javax.swing.JButton ButtonWriteCISDAC;
    private javax.swing.JButton ButtonWritePedHGneg;
    private javax.swing.JButton ButtonWritePedHGpos;
    private javax.swing.JButton ButtonWritePedLGneg;
    private javax.swing.JButton ButtonWritePedLGpos;
    private javax.swing.JButton ButtonWriteSwitches;
    private javax.swing.JButton ButtonWritetest1;
    private javax.swing.JButton ButtonWritetest2;
    private javax.swing.JButton ButtonWritetest3;
    private javax.swing.JCheckBox CheckBoxBroadcastMD;
    private javax.swing.JCheckBox CheckBoxMD1;
    private javax.swing.JCheckBox CheckBoxMD2;
    private javax.swing.JCheckBox CheckBoxMD3;
    private javax.swing.JCheckBox CheckBoxMD4;
    private javax.swing.JComboBox<String> ComboBoxDBSide;
    private javax.swing.JLabel LabelMDShown;
    private javax.swing.JLabel LabelWriteAllFEBs;
    private javax.swing.JTextField TextFieldCISDACWRITE;
    private javax.swing.JTextField TextFieldDAC0;
    private javax.swing.JTextField TextFieldDAC1;
    private javax.swing.JTextField TextFieldDAC10;
    private javax.swing.JTextField TextFieldDAC11;
    private javax.swing.JTextField TextFieldDAC2;
    private javax.swing.JTextField TextFieldDAC3;
    private javax.swing.JTextField TextFieldDAC4;
    private javax.swing.JTextField TextFieldDAC5;
    private javax.swing.JTextField TextFieldDAC6;
    private javax.swing.JTextField TextFieldDAC7;
    private javax.swing.JTextField TextFieldDAC8;
    private javax.swing.JTextField TextFieldDAC9;
    private javax.swing.JTextField TextFieldPedHGneg0;
    private javax.swing.JTextField TextFieldPedHGneg1;
    private javax.swing.JTextField TextFieldPedHGneg10;
    private javax.swing.JTextField TextFieldPedHGneg11;
    private javax.swing.JTextField TextFieldPedHGneg2;
    private javax.swing.JTextField TextFieldPedHGneg3;
    private javax.swing.JTextField TextFieldPedHGneg4;
    private javax.swing.JTextField TextFieldPedHGneg5;
    private javax.swing.JTextField TextFieldPedHGneg6;
    private javax.swing.JTextField TextFieldPedHGneg7;
    private javax.swing.JTextField TextFieldPedHGneg8;
    private javax.swing.JTextField TextFieldPedHGneg9;
    private javax.swing.JTextField TextFieldPedHGnegWRITE;
    private javax.swing.JTextField TextFieldPedHGpos0;
    private javax.swing.JTextField TextFieldPedHGpos1;
    private javax.swing.JTextField TextFieldPedHGpos10;
    private javax.swing.JTextField TextFieldPedHGpos11;
    private javax.swing.JTextField TextFieldPedHGpos2;
    private javax.swing.JTextField TextFieldPedHGpos3;
    private javax.swing.JTextField TextFieldPedHGpos4;
    private javax.swing.JTextField TextFieldPedHGpos5;
    private javax.swing.JTextField TextFieldPedHGpos6;
    private javax.swing.JTextField TextFieldPedHGpos7;
    private javax.swing.JTextField TextFieldPedHGpos8;
    private javax.swing.JTextField TextFieldPedHGpos9;
    private javax.swing.JTextField TextFieldPedHGposWRITE;
    private javax.swing.JTextField TextFieldPedLGneg0;
    private javax.swing.JTextField TextFieldPedLGneg1;
    private javax.swing.JTextField TextFieldPedLGneg10;
    private javax.swing.JTextField TextFieldPedLGneg11;
    private javax.swing.JTextField TextFieldPedLGneg2;
    private javax.swing.JTextField TextFieldPedLGneg3;
    private javax.swing.JTextField TextFieldPedLGneg4;
    private javax.swing.JTextField TextFieldPedLGneg5;
    private javax.swing.JTextField TextFieldPedLGneg6;
    private javax.swing.JTextField TextFieldPedLGneg7;
    private javax.swing.JTextField TextFieldPedLGneg8;
    private javax.swing.JTextField TextFieldPedLGneg9;
    private javax.swing.JTextField TextFieldPedLGnegWRITE;
    private javax.swing.JTextField TextFieldPedLGpos0;
    private javax.swing.JTextField TextFieldPedLGpos1;
    private javax.swing.JTextField TextFieldPedLGpos10;
    private javax.swing.JTextField TextFieldPedLGpos11;
    private javax.swing.JTextField TextFieldPedLGpos2;
    private javax.swing.JTextField TextFieldPedLGpos3;
    private javax.swing.JTextField TextFieldPedLGpos4;
    private javax.swing.JTextField TextFieldPedLGpos5;
    private javax.swing.JTextField TextFieldPedLGpos6;
    private javax.swing.JTextField TextFieldPedLGpos7;
    private javax.swing.JTextField TextFieldPedLGpos8;
    private javax.swing.JTextField TextFieldPedLGpos9;
    private javax.swing.JTextField TextFieldPedLGposWRITE;
    private javax.swing.JTextField TextFieldSwitches0;
    private javax.swing.JTextField TextFieldSwitches1;
    private javax.swing.JTextField TextFieldSwitches10;
    private javax.swing.JTextField TextFieldSwitches11;
    private javax.swing.JTextField TextFieldSwitches2;
    private javax.swing.JTextField TextFieldSwitches3;
    private javax.swing.JTextField TextFieldSwitches4;
    private javax.swing.JTextField TextFieldSwitches5;
    private javax.swing.JTextField TextFieldSwitches6;
    private javax.swing.JTextField TextFieldSwitches7;
    private javax.swing.JTextField TextFieldSwitches8;
    private javax.swing.JTextField TextFieldSwitches9;
    private javax.swing.JTextField TextFieldSwitchesW;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton42;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField100;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField22;
    private javax.swing.JTextField jTextField30;
    private javax.swing.JTextField jTextField31;
    private javax.swing.JTextField jTextField44;
    private javax.swing.JTextField jTextField45;
    private javax.swing.JTextField jTextField48;
    private javax.swing.JTextField jTextField49;
    private javax.swing.JTextField jTextField52;
    private javax.swing.JTextField jTextField54;
    private javax.swing.JTextField jTextField55;
    private javax.swing.JTextField jTextField58;
    private javax.swing.JTextField jTextField61;
    private javax.swing.JTextField jTextField64;
    private javax.swing.JTextField jTextField66;
    private javax.swing.JTextField jTextField67;
    private javax.swing.JTextField jTextField80;
    private javax.swing.JTextField jTextField81;
    private javax.swing.JTextField jTextField84;
    private javax.swing.JTextField jTextField85;
    private javax.swing.JTextField jTextField88;
    private javax.swing.JTextField jTextField90;
    private javax.swing.JTextField jTextField91;
    private javax.swing.JTextField jTextField94;
    private javax.swing.JTextField jTextField95;
    private javax.swing.JTextField jTextField96;
    private javax.swing.JTextField jTextField97;
    private javax.swing.JTextField jTextField98;
    private javax.swing.JTextField jTextField99;
    // End of variables declaration//GEN-END:variables
}
