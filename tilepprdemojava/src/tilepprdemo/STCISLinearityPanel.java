/* 
 *  TilePPrDemoJava Project
 *  2019 (c) IFIC-CERN
 */
package tilepprdemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Collections;
import javax.swing.JPanel;

import histogram.JScan;
import tilepprjavalibrary.lib.PPrLib;
import utilities.DataArrayList;

/**
 * CIS linearity scan panel controls window.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class STCISLinearityPanel extends javax.swing.JPanel {

    /**
     * Creates new form STCISLinearityPanel
     *
     * @param pprlib
     */
    public STCISLinearityPanel(PPrLib pprlib) {
        System.out.println("STCISLinearityPanel -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        // Defines JPanel array with JPanels channels.
        PanelPlots = new JPanel[]{PanelPlot1, PanelPlot2, PanelPlot3,
            PanelPlot4, PanelPlot5, PanelPlot6,
            PanelPlot7, PanelPlot8, PanelPlot9,
            PanelPlot10, PanelPlot11, PanelPlot12};

        // Flag to indicate CIS linearity run status through Atomic Integer. 
        CISLinearityRunStatus = new AtomicInteger(0);

        // Flag to indicate Find BC run status through Atomic Integer. 
        FindBCRunStatus = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed step.
        ProcScanPoint = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed events per step.
        ProcEventsPerStep = new AtomicInteger(0);

        // Defines ArrayList with selected minidrawers.
        MDsel = new ArrayList<>();

        // Default selected MD to show plots.
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);

        // Default selected gain to show plots.
        selectedGainPlot = "HG";
        ButtonSetHGPlots.setBackground(Color.GRAY);

        LabelCISlinearityRunStatus.setForeground(Color.RED);
        LabelFindBCRunStatus.setForeground(Color.RED);

        LabelScanPointCounter.setForeground(Color.blue);
        LabelDACCounter.setForeground(Color.blue);
        LabelpCCounter.setForeground(Color.blue);
        LabelEventsPerStepCounter.setForeground(Color.blue);

        // Define Integer 2d arrays of ArrayList with data.
        dataLG = new DataArrayList[4][12];
        dataHG = new DataArrayList[4][12];

        // Initialize the 2d array so that it is filled with Empty ArrayList<>'s'.
        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                dataHG[md][adc] = new DataArrayList();
                dataLG[md][adc] = new DataArrayList();
            }
        }

        // Defines Integer two-dimensional arrays with maximum sample value.
        dataMaxLG = new Integer[4][12];
        dataMaxHG = new Integer[4][12];

        // Defines Integer two-dimensional arrays with with maximum sample index.
        dataIdxMaxLG = new Integer[4][12];
        dataIdxMaxHG = new Integer[4][12];

        hxlow_DAC = 0.f;
        hxhigh_DAC = 4095.f;

        hxlow_pC_HG = 0.f;
        hxhigh_pC_HG = 20.9f;

        hxlow_pC_LG = 0.f;
        hxhigh_pC_LG = 819.f;

        min_DAC = 0.f;
        max_DAC = 4095.f;

        min_pC_HG = 0.f;
        max_pC_HG = 20.9f;

        min_pC_LG = 0.f;
        max_pC_LG = 819.f;

        // Set field values with NetBeans default text widgets.
        // Needed to define and set initial field values.
        get_all_parameters();

        setPed = "DAC";
        ButtonSetPedDACtoADC.setBackground(Color.GRAY);
        ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
        TextFieldADCped.setText(Integer.toString(ADCped));

        // Defines Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.
        // This class implements Executor and ExecutorService interfaces.
        executor = new ThreadPoolExecutor(
                10,
                10,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("STCISLinearityPanel")
        );
    }

    /**
     * Gets all field values from text widgets.
     */
    private synchronized void get_all_parameters() {
        get_DB_FPGA_side();
        get_MDs();
        get_gain();
        get_scan_units();
        get_charge_discharge_BC();
        get_BC_to_read();
        get_steps();
        get_ped_DAC();
        get_ped_ADC();
    }

    /**
     * Fills all text widgets with field values.
     */
    public static synchronized void set_all_parameters() {
        set_DB_FPGA_side();
        set_MDs();
        set_gain();
        set_scan_units();
        set_charge_discharge_BC();
        set_BC_to_read();
        set_steps();
        set_ped();
        set_ped_DAC();
        set_ped_ADC();
    }

    /**
     * Define Runnables through lambda expressions.
     */
    private void initDefineRunnables() {
        RunnableStartRun = () -> {
            run_start_CIS_linearity();
        };

        RunnableFindBC = () -> {
            run_start_find_BC();
        };
    }

    /**
     * Configures and starts BC finding algorithm.
     */
    private synchronized void run_start_find_BC() {
        Boolean ret;
        String fs;
        Integer foundBCflag, bc, chargedBC = 0;

        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        Integer charge = 3800;

        Console.print_cr("==> Find BC run preparing... ");

        LabelFindBCRunStatus.setText("preparing...");
        LabelFindBCRunStatus.setForeground(Color.blue);

        Console.print_cr("Getting Find BC run parameters... ");

        // Gets selected minidrawers.
        // --------------------------
        get_MDs();
        fs = "  Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((MDbroadcast == 1) ? "True" : "False")));
        if (MDbroadcast == 0) {
            fs = "  Total minidrawers: %d Active: %d %d %d %d";
            Console.print_cr(String.format(fs,
                    MDtot,
                    MDsel.get(0),
                    MDsel.get(1),
                    MDsel.get(2),
                    MDsel.get(3)));
        }

        if (MDtot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            run_terminate_find_BC();
            return;
        }

        // Gets selected DB FPGA side.
        // ---------------------------
        get_DB_FPGA_side();
        fs = "  DB FPGA Side: %d (%s)";
        Console.print_cr(String.format(fs, dbside, dbsidestring));

        // Gets BCs for charge and discharge capacitors.
        // ---------------------------------------------
        get_charge_discharge_BC();
        fs = "  BCID_charge: %d  BCID_discharge: %d";
        Console.print_cr(String.format(fs, BCID_charge, BCID_discharge));
        fs = "  Charge: %d";
        Console.print_cr(String.format(fs, charge));

        // Gets gain.
        // ----------
        get_gain();
        fs = "  Gain: %d (%s)";
        Console.print_cr(String.format(fs, gain, ((gain == 0) ? "LG" : "HG")));

        // Resets CRC counters.
        // --------------------
        Console.print_nr("Resetting CRC counters... ");
        ret = pprlib.ppr.reset_CRC_counters();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Enables deadtime bit in global trigger configuration register
        // -------------------------------------------------------------
        Console.print_cr("Setting PPr enable deadtime bit in Global Trigger Conf... ");
        ret = pprlib.ppr.set_global_trigger_deadtime(0);
        fs = "  set bit to 0: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));
        ret = pprlib.ppr.set_global_trigger_deadtime(1);
        fs = "  set bit to 1: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));

        // Configures FEBs pedestal DAC bias offsets.
        // ------------------------------------------
        Console.print_cr("Setting FEB DACs bias offsets... ");
        if ("DAC".equals(setPed)) {
            get_ped_DAC();
            ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
            TextFieldADCped.setText(Integer.toString(ADCped));
            fs = "  DAC Vp: %d. DAC Vn: %d. ADC counts: %d";
            Console.print_cr(String.format(fs, DACbiasP, DACbiasN, ADCped));
        } else if ("ADC".equals(setPed)) {
            get_ped_ADC();
            VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
            DACbiasP = VinDACs.get(0);
            DACbiasN = VinDACs.get(1);
            TextFieldDACbiasP.setText(Integer.toString(DACbiasP));
            TextFieldDACbiasN.setText(Integer.toString(DACbiasN));
            fs = "  ADC counts: %d. DAC Vp: %d. DAC Vn: %d";
            Console.print_cr(String.format(fs, ADCped, DACbiasP, DACbiasN));
        }
        ret = set_FEB_ADC_bias_offsets_DACs();
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configures FEBs to load DACss bias offsets.
        // -------------------------------------------
        Console.print_cr("Loading FEB DACs  bias offsets... ");
        ret = set_FEB_load_ADC_DACs();
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configures FEB switches.
        // ------------------------
        Console.print_cr("Setting FEB switches...");
        ret = set_FEB_switches();
        fs = "   Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Setting FEB CIS DACs
        // ---------------------
        Console.print_cr("Setting FEB CIS DACs... ");
        ret = set_FEB_CIS_DACs(charge);
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Setting CIS settings on minidrawers.
        // ------------------------------------
        Console.print_cr("Setting CIS on minidrawers... ");
        ret = set_CIS_BCID_settings();
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        Console.print_cr("==> Starts loop on BCs...");

        LabelFindBCRunStatus.setText("processing...");
        LabelFindBCRunStatus.setForeground(Color.blue);

        // Loops while:
        // 1) Run is not stopped.
        // 2) IPbus connection has not dropped.
        // 3) BCs are less that 4096.
        // 4) the BC with charge is not found.
        foundBCflag = 0;
        bc = 0;
        while (true) {
            if (!IPbusPanel.getConnectFlag()
                    || FindBCRunStatus.get() == 0
                    || bc > 4095
                    || foundBCflag == 1) {
                break;
            }

            // Sends L1A only to those BCs sometime after the capacitor discharge BC.
            if ((bc > BCID_discharge + 0) & (bc < BCID_discharge + 300)) {

                // Reads last Event BCID (disables busy to read pipelines).
                LastEvtBCID = pprlib.ppr.get_counter_last_event_BCID();

                // Reads last Event BCID (disables busy to read pipelines).
                LastEvtL1ID = pprlib.ppr.get_counter_last_event_L1ID();

                fs = "  BC: %d (BC discharge + %d  LastEvt: %d %d)";
                Console.print_cr(String.format(fs, bc, bc - BCID_discharge, LastEvtBCID, LastEvtL1ID));

                // Sends a PPr L1A.
                pprlib.feb.send_L1A(bc, 3);

                for (Integer md = 0; md < 4; md++) {
                    if (MDsel.get(md) == 1) {
                        for (Integer adc = 0; adc < 12; adc++) {
                            dataHG[md][adc].getData().clear();
                            dataLG[md][adc].getData().clear();

                            dataHG[md][adc].setData(pprlib.ppr.get_data_HG(md, adc, 16));
                            dataLG[md][adc].setData(pprlib.ppr.get_data_LG(md, adc, 16));

                            if (dataHG[md][adc].getData().get(7) > 2500
                                    || dataLG[md][adc].getData().get(7) > 600) {
                                fs = "    Charge found for MD: %d ADC: %d";
                                Console.print_cr(String.format(fs, md + 1, adc));
                                chargedBC = bc;
                                foundBCflag = 1;
                            }
                        }
                    }
                }
            }

            bc++;
        }

        if (foundBCflag == 1) {
            fs = "  Found BC with charge: %d (BC discharge + %d)";
            Console.print_cr(String.format(fs, chargedBC, (chargedBC - BCID_discharge)));

            for (Integer md = 0; md < 4; md++) {
                if (MDsel.get(md) == 1) {
                    for (Integer adc = 0; adc < 12; adc++) {
                        fs = "  MD: %d ADC: %d Data HG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                        Console.print_cr(String.format(fs,
                                md + 1,
                                adc,
                                dataHG[md][adc].getData().get(0),
                                dataHG[md][adc].getData().get(1),
                                dataHG[md][adc].getData().get(2),
                                dataHG[md][adc].getData().get(3),
                                dataHG[md][adc].getData().get(4),
                                dataHG[md][adc].getData().get(5),
                                dataHG[md][adc].getData().get(6),
                                dataHG[md][adc].getData().get(7),
                                dataHG[md][adc].getData().get(8),
                                dataHG[md][adc].getData().get(9),
                                dataHG[md][adc].getData().get(10),
                                dataHG[md][adc].getData().get(11)));
                        fs = "  MD: %d ADC: %d Data LG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                        Console.print_cr(String.format(fs,
                                md + 1,
                                adc,
                                dataLG[md][adc].getData().get(0),
                                dataLG[md][adc].getData().get(1),
                                dataLG[md][adc].getData().get(2),
                                dataLG[md][adc].getData().get(3),
                                dataLG[md][adc].getData().get(4),
                                dataLG[md][adc].getData().get(5),
                                dataLG[md][adc].getData().get(6),
                                dataLG[md][adc].getData().get(7),
                                dataLG[md][adc].getData().get(8),
                                dataLG[md][adc].getData().get(9),
                                dataLG[md][adc].getData().get(10),
                                dataLG[md][adc].getData().get(11)));
                    }
                }
            }

            TextFieldBCtoRead.setText(Integer.toString(chargedBC));
            TextFieldBCtoRead.setForeground(Color.black);
        }

        run_terminate_find_BC();
    }

    /**
     * Configures and starts CIS linearity run.
     */
    private synchronized void run_start_CIS_linearity() {
        Boolean ret;
        String fs;
        Integer step;
        Float charge_DAC, charge_pC_LG, charge_pC_HG;
        Integer max, idxmax;

        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        Console.print_cr("==> CIS linearity run preparing... ");

        LabelCISlinearityRunStatus.setText("preparing...");
        LabelCISlinearityRunStatus.setForeground(Color.blue);

        // Change label run status in MainFrame.
        MainFrame.LabelCISLinearityRunMainFrame.setText("running");
        MainFrame.LabelCISLinearityRunMainFrame.setForeground(Color.GREEN.darker());

        // Initialize field for number of steps.
        ProcScanPoint.set(0);
        LabelScanPointCounter.setText(Integer.toString(ProcScanPoint.get()));

        // Initialize field for DAC counts scan value.
        LabelDACCounter.setText(Integer.toString(0));

        // Initialize field for pC scan value.
        LabelpCCounter.setText("0.0/0.0");

        // Initialize field for events per step.
        ProcEventsPerStep.set(0);
        LabelEventsPerStepCounter.setText(Integer.toString(ProcEventsPerStep.get()));

        Console.print_cr("Getting CIS linearity scan parameters... ");

        // Get selected minidrawers.
        // -------------------------
        get_MDs();
        fs = "  Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((MDbroadcast == 1) ? "True" : "False")));
        if (MDbroadcast == 0) {
            fs = "  Total minidrawers: %d Active: %d %d %d %d";
            Console.print_cr(String.format(fs,
                    MDtot,
                    MDsel.get(0),
                    MDsel.get(1),
                    MDsel.get(2),
                    MDsel.get(3)));
        }

        if (MDtot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            run_terminate_CIS_linearity();
            return;
        }

        // Get selected DB FPGA side.
        // -------------------------
        get_DB_FPGA_side();
        fs = "  DB FPGA Side: %d (%s)";
        Console.print_cr(String.format(fs, dbside, dbsidestring));

        // Get BCs for charge and discharge capacitors.
        // --------------------------------------------
        get_charge_discharge_BC();
        fs = "  BCID_charge: %d  BCID_discharge: %d";
        Console.print_cr(String.format(fs, BCID_charge, BCID_discharge));

        // Get gain.
        // ---------
        get_gain();
        fs = "  Gain: %d (%s)";
        Console.print_cr(String.format(fs, gain, ((gain == 0) ? "LG" : "HG")));

        // Get BCID to read pipeline data from.
        // ------------------------------------
        get_BC_to_read();
        fs = "  BC to read: %d";
        Console.print_cr(String.format(fs, BCID));

        // Get charge units (DACs or pC).
        // ------------------------------
        get_scan_units();
        fs = "  Charge units: %s";
        Console.print_cr(String.format(fs, scanUnits));

        // Get steps.
        // ----------
        get_steps();
        fs = "  Steps: %d. Evts/step: %d. Lengths-> %.1f DACs; %.1f pC(LG); %.1f pC(HG)";
        Console.print_cr(String.format(fs,
                nbSteps,
                stepEvents,
                step_length_DAC,
                step_length_pC_LG,
                step_length_pC_HG));
        for (Integer i = 0; i < nbSteps + 1; i++) {
            charge_DAC = i * step_length_DAC;
            charge_pC_LG = i * step_length_pC_LG;
            charge_pC_HG = i * step_length_pC_HG;
            fs = "    Step: %d. DACs: %.1f. pC(LG): %.1f. pC(HG): %.1f";
            Console.print_cr(String.format(fs, (i + 1), charge_DAC, charge_pC_LG, charge_pC_HG));
        }

        // Redefine JScan settings according to selected DAC step
        // or charge step for the scan (DACs or pC).
        // ------------------------------------------------------
        fs = "  Setting series binning...";
        Console.print_cr(String.format(fs));
        plot_ResetHistograms();

        // Set internal TTC.
        // -----------------
        Console.print_nr("Setting TTC internal... ");
        ret = pprlib.ppr.set_global_TTC_internal();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Reset CRC counters.
        // -------------------
        Console.print_nr("Resetting CRC counters... ");
        ret = pprlib.ppr.reset_CRC_counters();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Enable deadtime bit in global trigger configuration register
        // ------------------------------------------------------------
        Console.print_cr("Setting PPr enable deadtime bit in Global Trigger Conf... ");
        ret = pprlib.ppr.set_global_trigger_deadtime(0);
        fs = "  set bit to 0: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));
        ret = pprlib.ppr.set_global_trigger_deadtime(1);
        fs = "  set bit to 1: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));

        // Configure FEBs pedestal settings.
        // ----------------------------------
        Console.print_cr("Setting FEB ADC bias offsets... ");
        if ("DAC".equals(setPed)) {
            get_ped_DAC();
            ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
            TextFieldADCped.setText(Integer.toString(ADCped));
            fs = "  DAC Vp: %d. DAC Vn: %d. ADC counts: %d";
            Console.print_cr(String.format(fs, DACbiasP, DACbiasN, ADCped));
        } else if ("ADC".equals(setPed)) {
            get_ped_ADC();
            VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
            DACbiasP = VinDACs.get(0);
            DACbiasN = VinDACs.get(1);
            TextFieldDACbiasP.setText(Integer.toString(DACbiasP));
            TextFieldDACbiasN.setText(Integer.toString(DACbiasN));
            fs = "  ADC counts: %d. DAC Vp: %d. DAC Vn: %d";
            Console.print_cr(String.format(fs, ADCped, DACbiasP, DACbiasN));
        }
        ret = set_FEB_ADC_bias_offsets_DACs();
        fs = "Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configure FEBs to load ADC pedestal DACs.
        // ------------------------------------------
        Console.print_cr("Loading FEB ADC DACs... ");
        ret = set_FEB_load_ADC_DACs();
        fs = "Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configures FEB switches.
        // ------------------------
        Console.print_cr("Setting FEB switches...");
        ret = set_FEB_switches();
        fs = "Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Setting CIS settings on minidrawers.
        // ------------------------------------
        Console.print_cr("Setting CIS on minidrawers... ");
        ret = set_CIS_BCID_settings();
        fs = "Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        Console.print_cr("==> Starts loop on CIS linearity scan...");
        LabelCISlinearityRunStatus.setText("processing...");
        LabelCISlinearityRunStatus.setForeground(Color.blue);

        // Loops while:
        // 1) Run is not stopped.
        // 2) IPbus connection has not dropped.
        // 3) Processed step not reached the selected value.
        step = 0;
        while (true) {
            if (!IPbusPanel.getConnectFlag()
                    || CISLinearityRunStatus.get() == 0
                    || step > nbSteps) {
                break;
            }

            // Setting FEB CIS DACs
            // --------------------
            charge_DAC = step * step_length_DAC;
            charge_pC_LG = step * step_length_pC_LG;
            charge_pC_HG = step * step_length_pC_HG;

            fs = "Step: %d";
            Console.print_cr(String.format(fs, step));
            fs = "Setting FEB CIS DACs: %.1f (LG: %.1f pC  HG: %.1f pC)";
            Console.print_cr(String.format(fs, charge_DAC, charge_pC_LG, charge_pC_HG));
            ret = set_FEB_CIS_DACs(charge_DAC.intValue());
            fs = "Result: %s";
            Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

            // Update DAC counter.
            LabelDACCounter.setText(Integer.toString(charge_DAC.intValue()));

            // Update pC counter.
            LabelpCCounter.setText(String.format("%.1f/%.1f", charge_pC_HG, charge_pC_LG));

            // Update step counter.
            ProcScanPoint.addAndGet(1);
            LabelScanPointCounter.setText(Integer.toString(ProcScanPoint.get()));

            // Initializes field for proccessed events per step.
            ProcEventsPerStep.set(0);

            // Loops over events for a particular CIS amplitude value.
            // -------------------------------------------------------
            for (Integer event = 0; event < stepEvents; event++) {
                if (!IPbusPanel.getConnectFlag()
                        || CISLinearityRunStatus.get() == 0) {
                    run_terminate_CIS_linearity();
                    return;
                }

                // Updates events per step counter.
                ProcEventsPerStep.addAndGet(1);
                LabelEventsPerStepCounter.setText(Integer.toString(ProcEventsPerStep.get()));

                // Reads last Event BCID (disables busy to read pipelines).
                LastEvtBCID = pprlib.ppr.get_counter_last_event_BCID();

                // Reads last Event L1ID (disables busy to read pipelines).
                LastEvtL1ID = pprlib.ppr.get_counter_last_event_L1ID();

                // Sends a PPr L1A.
                pprlib.feb.send_L1A(BCID, 3);

                if (DebugSettings.getVerbose()) {
                    fs = "Event: %d";
                    Console.print_cr(String.format(fs, event + 1));
                }

                // Reads pipeline data.
                for (Integer md = 0; md < 4; md++) {
                    if (MDsel.get(md) == 1) {
                        for (Integer adc = 0; adc < 12; adc++) {

                            dataHG[md][adc].getData().clear();
                            dataLG[md][adc].getData().clear();

                            dataHG[md][adc].setData(pprlib.ppr.get_data_HG(md, adc, 16));
                            dataLG[md][adc].setData(pprlib.ppr.get_data_LG(md, adc, 16));

                            max = Collections.max(dataHG[md][adc].getData());
                            idxmax = dataHG[md][adc].getData().indexOf(Collections.max(dataHG[md][adc].getData()));
                            dataMaxHG[md][adc] = max;
                            dataIdxMaxHG[md][adc] = idxmax;

                            max = Collections.max(dataLG[md][adc].getData());
                            idxmax = dataLG[md][adc].getData().indexOf(Collections.max(dataLG[md][adc].getData()));
                            dataMaxLG[md][adc] = max;
                            dataIdxMaxLG[md][adc] = idxmax;

                            if (DebugSettings.getVerbose()) {
                                if (adc == 0) {
                                    fs = "  MD: %d ADC: %d Data HG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                                    Console.print_cr(String.format(fs,
                                            md + 1,
                                            adc,
                                            dataHG[md][adc].getData().get(0),
                                            dataHG[md][adc].getData().get(1),
                                            dataHG[md][adc].getData().get(2),
                                            dataHG[md][adc].getData().get(3),
                                            dataHG[md][adc].getData().get(4),
                                            dataHG[md][adc].getData().get(5),
                                            dataHG[md][adc].getData().get(6),
                                            dataHG[md][adc].getData().get(7),
                                            dataHG[md][adc].getData().get(8),
                                            dataHG[md][adc].getData().get(9),
                                            dataHG[md][adc].getData().get(10),
                                            dataHG[md][adc].getData().get(11)));
                                    fs = "  Max sample index: %d Max sample value: %d";
                                    Console.print_cr(String.format(fs, dataIdxMaxHG[md][adc], dataMaxHG[md][adc]));

                                    fs = "  MD: %d ADC: %d Data LG: %d %d %d %d %d %d %d %d %d %d %d %d ";
                                    Console.print_cr(String.format(fs,
                                            md + 1,
                                            adc,
                                            dataLG[md][adc].getData().get(0),
                                            dataLG[md][adc].getData().get(1),
                                            dataLG[md][adc].getData().get(2),
                                            dataLG[md][adc].getData().get(3),
                                            dataLG[md][adc].getData().get(4),
                                            dataLG[md][adc].getData().get(5),
                                            dataLG[md][adc].getData().get(6),
                                            dataLG[md][adc].getData().get(7),
                                            dataLG[md][adc].getData().get(8),
                                            dataLG[md][adc].getData().get(9),
                                            dataLG[md][adc].getData().get(10),
                                            dataLG[md][adc].getData().get(11)));
                                    fs = "  Max sample index: %d Max sample value: %d";
                                    Console.print_cr(String.format(fs, dataIdxMaxLG[md][adc], dataMaxLG[md][adc]));
                                }
                            }
                        }
                    }
                }

                // Fills and Draws histogram plots.
                for (int md = 0; md < 4; md++) {
                    if (MDsel.get(md) == 1) {
                        for (int adc = 0; adc < 12; adc++) {
                            h_plot_HG[md][adc].fill(step, dataMaxHG[md][adc]);
                            h_plot_LG[md][adc].fill(step, dataMaxLG[md][adc]);

                            h_plot_HG[md][adc].draw();
                            h_plot_LG[md][adc].draw();
                        }
                    }
                }
            }

            step++;
        }

        // Terminates run.
        run_terminate_CIS_linearity();
    }

    /**
     * Terminates run.
     */
    private synchronized void run_terminate_CIS_linearity() {
        // Sets run status flag as run terminated.
        CISLinearityRunStatus.set(0);

        // Updates CIS linearity run status.
        LabelCISlinearityRunStatus.setText("stopped");
        LabelCISlinearityRunStatus.setForeground(Color.RED);

        // Updates MainFrame label for CIS linearity run status.
        MainFrame.LabelCISLinearityRunMainFrame.setText("not running");
        MainFrame.LabelCISLinearityRunMainFrame.setForeground(Color.RED);

        Console.print_cr("==> CIS linearity run terminated");
    }

    /**
     * Terminates Find BC run.
     */
    private synchronized void run_terminate_find_BC() {
        FindBCRunStatus.set(0);

        LabelFindBCRunStatus.setText("stopped");
        LabelFindBCRunStatus.setForeground(Color.RED);

        Console.print_cr("==> Find BC run terminated");
    }

    /**
     * Gets minidrawers to process and minidrawer broadcast mode.
     */
    private synchronized void get_MDs() {
        MDsel.clear();
        MDsel.add(CheckBoxMD1.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD2.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD3.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD4.isSelected() ? 1 : 0);

        MDtot = 0;
        for (Integer md = 0; md < 4; md++) {
            if (MDsel.get(md) == 1) {
                MDtot++;
            }
        }

        MDbroadcast = (CheckBoxBroadcastMD.isSelected() ? 1 : 0);
    }

    /**
     * Sets minidrawers to process and minidrawer broadcast mode.
     */
    public static synchronized void set_MDs() {
        if (MDsel.get(0) == 0) {
            CheckBoxMD1.setSelected(false);
        } else {
            CheckBoxMD1.setSelected(true);
        }

        if (MDsel.get(1) == 0) {
            CheckBoxMD2.setSelected(false);
        } else {
            CheckBoxMD2.setSelected(true);
        }

        if (MDsel.get(2) == 0) {
            CheckBoxMD3.setSelected(false);
        } else {
            CheckBoxMD3.setSelected(true);
        }

        if (MDsel.get(3) == 0) {
            CheckBoxMD4.setSelected(false);
        } else {
            CheckBoxMD4.setSelected(true);
        }

        if (MDbroadcast == 0) {
            CheckBoxBroadcastMD.setSelected(false);
        } else {
            CheckBoxBroadcastMD.setSelected(true);
        }
    }

    /**
     * Gets DB FPGA side.
     */
    private synchronized void get_DB_FPGA_side() {
        String side_strings[] = new String[]{"Broadcast", "Side A", "Side B", "Unknown"};

        Object selected = ComboBoxDBSide.getSelectedItem();
        String DBSideStr = selected.toString();
        switch (DBSideStr) {
            case "Broadcast":
                dbside = 0;
                dbsidestring = side_strings[0];
                break;
            case "Side A":
                dbside = 1;
                dbsidestring = side_strings[1];
                break;
            case "Side B":
                dbside = 2;
                dbsidestring = side_strings[2];
                break;
            default:
                dbsidestring = side_strings[3];
                break;
        }
    }

    /**
     * Sets DB FPGA side ComboBox with field value.
     */
    public static synchronized void set_DB_FPGA_side() {
        switch (dbsidestring) {
            case "Broadcast":
                dbside = 0;
                break;
            case "Side A":
                dbside = 1;
                break;
            case "Side B":
                dbside = 2;
                break;
        }

        ComboBoxDBSide.setSelectedIndex(dbside);
    }

    /**
     * Gets BCID for L1A.
     */
    private synchronized void get_BC_to_read() {
        BCID = Integer.valueOf(TextFieldBCtoRead.getText(), 10);
        if (BCID > 4095) {
            BCID = 4095;
            TextFieldBCtoRead.setText("4095");
        }
        if (BCID < 0) {
            BCID = 0;
            TextFieldBCtoRead.setText("0");
        }
    }

    /**
     * Sets BCID for L1A.
     */
    public static synchronized void set_BC_to_read() {
        TextFieldBCtoRead.setText(String.valueOf(BCID));
    }

    /**
     * Gets DAC pedestal positive and negative offsets.
     */
    private synchronized void get_ped_DAC() {
        DACbiasP = Integer.valueOf(TextFieldDACbiasP.getText(), 10);
        if (DACbiasP > 2261) {
            DACbiasP = 2261;
            TextFieldDACbiasP.setText("2261");
        }
        if (DACbiasP < 1278) {
            DACbiasP = 1278;
            TextFieldDACbiasP.setText("1278");
        }

        DACbiasN = Integer.valueOf(TextFieldDACbiasN.getText(), 10);
        if (DACbiasN > 2261) {
            DACbiasN = 2261;
            TextFieldDACbiasN.setText("2261");
        }
        if (DACbiasN < 1278) {
            DACbiasN = 1278;
            TextFieldDACbiasN.setText("1278");
        }
    }

    /**
     * Sets DAC pedestal positive and negative offsets.
     */
    public static synchronized void set_ped_DAC() {
        TextFieldDACbiasP.setText(String.valueOf(DACbiasP));
        TextFieldDACbiasN.setText(String.valueOf(DACbiasN));
    }

    /**
     * Gets ADC pedestal counts.
     */
    private synchronized void get_ped_ADC() {
        ADCped = Integer.valueOf(TextFieldADCped.getText(), 10);
        if (ADCped > 4095) {
            ADCped = 4095;
            TextFieldADCped.setText("4095");
        }
        if (ADCped < 0) {
            ADCped = 0;
            TextFieldADCped.setText("0");
        }
    }

    /**
     * Sets ADC pedestal counts.
     */
    public static synchronized void set_ped_ADC() {
        TextFieldADCped.setText(String.valueOf(ADCped));
    }

    /**
     * Get gain for CIS linearity scan. 0: LG, 1: HG.
     */
    private synchronized void get_gain() {
        Object selected = ComboBoxGain.getSelectedItem();
        String strGain = selected.toString();

        if ("HG".equals(strGain)) {
            gain = 1;
        } else if ("LG".equals(strGain)) {
            gain = 0;
        }
    }

    /**
     * Set gain for CIS linearity scan. 0: LG, 1: HG.
     */
    public static synchronized void set_gain() {
        ComboBoxGain.setSelectedIndex(gain);
    }

    /**
     * Set pedestal calculation mode.
     */
    public static synchronized void set_ped() {
        if ("DAC".equals(setPed)) {
            ButtonSetPedDACtoADC.setBackground(Color.GRAY);
            ButtonSetPedADCtoDAC.setBackground(null);
        } else if ("ADC".equals(setPed)) {
            ButtonSetPedDACtoADC.setBackground(null);
            ButtonSetPedADCtoDAC.setBackground(Color.GRAY);
        }
    }

    /**
     * Gets charge units for CIS linearity scan (DACs or pC).
     */
    private synchronized void get_scan_units() {
        Object selected = ComboBoxScanUnits.getSelectedItem();
        scanUnits = selected.toString();
    }

    /**
     * Sets gain.
     */
    public static synchronized void set_scan_units() {
        ComboBoxScanUnits.setSelectedItem(scanUnits);
    }

    /**
     * Gets charge and discharge BCs.
     */
    private synchronized void get_charge_discharge_BC() {
        BCID_charge = Integer.valueOf(TextFieldChargeBC.getText(), 10);
        if (BCID_charge > 4095) {
            BCID_charge = 4095;
            TextFieldChargeBC.setText("4095");
        }
        if (BCID_charge < 0) {
            BCID_charge = 0;
            TextFieldChargeBC.setText("0");
        }

        BCID_discharge = Integer.valueOf(TextFieldDischargeBC.getText(), 10);
        if (BCID_discharge > 4095) {
            BCID_discharge = 4095;
            TextFieldDischargeBC.setText("4095");
        }
        if (BCID_discharge < 0) {
            BCID_discharge = 0;
            TextFieldDischargeBC.setText("0");
        }
    }

    /**
     * Set charge and discharge BCs.
     */
    public static synchronized void set_charge_discharge_BC() {
        TextFieldChargeBC.setText(String.valueOf(BCID_charge));
        TextFieldDischargeBC.setText(String.valueOf(BCID_discharge));
    }

    /**
     * Get number of step and calculate point charge values (in DACs and pC).
     */
    private synchronized void get_steps() {
        nbSteps = Integer.valueOf(TextFieldSteps.getText(), 10);
        if (nbSteps > 200) {
            nbSteps = 200;
            TextFieldSteps.setText("200");
        }
        if (nbSteps < 1) {
            nbSteps = 1;
            TextFieldSteps.setText("1");
        }

        // Calculate step length.
        step_length_DAC = (max_DAC - min_DAC) / nbSteps;
        step_length_pC_HG = (max_pC_HG - min_pC_HG) / nbSteps;
        step_length_pC_LG = (max_pC_LG - min_pC_LG) / nbSteps;

        stepEvents = Integer.valueOf(TextFieldEvtsStep.getText(), 10);
        if (stepEvents < 1) {
            stepEvents = 1;
            TextFieldEvtsStep.setText("1");
        }
    }

    /**
     * Sets DAC step.
     */
    public static synchronized void set_steps() {
        TextFieldSteps.setText(String.valueOf(nbSteps));
        TextFieldEvtsStep.setText(String.valueOf(stepEvents));
    }

    /**
     * Sets pedestal settings for selected FEBs (HG, LG, positive, negative).
     */
    private synchronized Boolean set_FEB_ADC_bias_offsets_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_ped_HG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_HG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Loads ADC bias offsets DACS (HG and LG) for selected FEBs.
     */
    private synchronized Boolean set_FEB_load_ADC_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.load_ped_HG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load HG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.load_ped_LG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load LG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Sets switches for noise for selected FEBs.
     */
    private synchronized Boolean set_FEB_switches() {
        Boolean ret = false;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_switches_noise(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set switches result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Sets FEB CIS DACs.
     */
    private synchronized Boolean set_FEB_CIS_DACs(Integer charge) {
        Boolean ret = false;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_CIS_DAC(md, dbside, adc, charge);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set CIS DAC: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, charge, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Sets minidrawer CIS configuration.
     */
    private synchronized Boolean set_CIS_BCID_settings() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                ret = pprlib.feb.set_CIS_BCID_settings(md, dbside, BCID_charge, BCID_discharge, gain);
                if (DebugSettings.getVerbose()) {
                    fs = "  MD: %d CIS configuration result: %s";
                    Console.print_cr(String.format(fs, md, (ret ? "Ok" : "Fail")));
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel28 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        TextFieldChargeBC = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        TextFieldDischargeBC = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        ButtonStartRunFindBC = new javax.swing.JButton();
        ButtonStopRunFindBC = new javax.swing.JButton();
        LabelFindBCRunStatus = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        ComboBoxGain = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        TextFieldBCtoRead = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        TextFieldDACbiasP = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextFieldDACbiasN = new javax.swing.JTextField();
        ButtonSetPedDACtoADC = new javax.swing.JButton();
        ButtonSetPedADCtoDAC = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        TextFieldADCped = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        CheckBoxBroadcastMD = new javax.swing.JCheckBox();
        jPanel24 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ComboBoxDBSide = new javax.swing.JComboBox<>();
        jPanel11 = new javax.swing.JPanel();
        ButtonSetMD1Plots = new javax.swing.JButton();
        ButtonSetMD2Plots = new javax.swing.JButton();
        ButtonSetMD3Plots = new javax.swing.JButton();
        ButtonSetMD4Plots = new javax.swing.JButton();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        jPanel13 = new javax.swing.JPanel();
        ButtonSetLGPlots = new javax.swing.JButton();
        ButtonSetHGPlots = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ComboBoxScanUnits = new javax.swing.JComboBox<>();
        jPanel20 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldSteps = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        TextFieldEvtsStep = new javax.swing.JTextField();
        jPanel16 = new javax.swing.JPanel();
        ButtonStartRunCISLinearity = new javax.swing.JButton();
        ButtonStopRunCISLinearity = new javax.swing.JButton();
        LabelCISlinearityRunStatus = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        LabelEventsPerStepCounter = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        LabelScanPointCounter = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        LabelDACCounter = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        LabelpCCounter = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        PanelPlot1 = new javax.swing.JPanel();
        PanelPlot2 = new javax.swing.JPanel();
        PanelPlot3 = new javax.swing.JPanel();
        PanelPlot4 = new javax.swing.JPanel();
        PanelPlot5 = new javax.swing.JPanel();
        PanelPlot6 = new javax.swing.JPanel();
        PanelPlot7 = new javax.swing.JPanel();
        PanelPlot8 = new javax.swing.JPanel();
        PanelPlot9 = new javax.swing.JPanel();
        PanelPlot10 = new javax.swing.JPanel();
        PanelPlot11 = new javax.swing.JPanel();
        PanelPlot12 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        ButtonSaveAsDefaults = new javax.swing.JButton();
        ButtonLoadDefaults = new javax.swing.JButton();
        ButtonShowDefaults = new javax.swing.JButton();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 30, 0, 30, 0, 30, 0};
        jPanel2Layout.rowHeights = new int[] {0};
        jPanel2.setLayout(jPanel2Layout);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0};
        jPanel5Layout.rowHeights = new int[] {0};
        jPanel5.setLayout(jPanel5Layout);

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0};
        jPanel10Layout.rowHeights = new int[] {0, 7, 0, 7, 0, 7, 0};
        jPanel10.setLayout(jPanel10Layout);

        jPanel28.setLayout(new java.awt.GridBagLayout());

        jLabel15.setText("<html><b>Algorithm to find BC with charge for L1 trigger</b></html>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel28.add(jLabel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jPanel28, gridBagConstraints);

        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel18Layout.rowHeights = new int[] {0};
        jPanel18.setLayout(jPanel18Layout);

        jLabel8.setText("BC charge:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel8, gridBagConstraints);

        TextFieldChargeBC.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldChargeBC.setText("500");
        TextFieldChargeBC.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel18.add(TextFieldChargeBC, gridBagConstraints);

        jLabel10.setText("BC disch:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jLabel10, gridBagConstraints);

        TextFieldDischargeBC.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDischargeBC.setText("2200");
        TextFieldDischargeBC.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel18.add(TextFieldDischargeBC, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel10.add(jPanel18, gridBagConstraints);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        jPanel6Layout.rowHeights = new int[] {0};
        jPanel6.setLayout(jPanel6Layout);

        ButtonStartRunFindBC.setText("Find BC");
        ButtonStartRunFindBC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStartRunFindBCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(ButtonStartRunFindBC, gridBagConstraints);

        ButtonStopRunFindBC.setText("Stop");
        ButtonStopRunFindBC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStopRunFindBCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel6.add(ButtonStopRunFindBC, gridBagConstraints);

        LabelFindBCRunStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelFindBCRunStatus.setText("stopped");
        LabelFindBCRunStatus.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel6.add(LabelFindBCRunStatus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel10.add(jPanel6, gridBagConstraints);

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel7Layout.rowHeights = new int[] {0};
        jPanel7.setLayout(jPanel7Layout);

        jLabel6.setText("Gain:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel6, gridBagConstraints);

        ComboBoxGain.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "LG", "HG" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel7.add(ComboBoxGain, gridBagConstraints);

        jLabel7.setText("BC to read:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jLabel7, gridBagConstraints);

        TextFieldBCtoRead.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldBCtoRead.setText("2238");
        TextFieldBCtoRead.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel7.add(TextFieldBCtoRead, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel10.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel5, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0};
        jPanel8Layout.rowHeights = new int[] {0};
        jPanel8.setLayout(jPanel8Layout);

        jPanel21.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel22Layout = new java.awt.GridBagLayout();
        jPanel22Layout.columnWidths = new int[] {0, 10, 0};
        jPanel22Layout.rowHeights = new int[] {0, 7, 0};
        jPanel22.setLayout(jPanel22Layout);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("DAC Vp:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel9.add(jLabel1, gridBagConstraints);

        TextFieldDACbiasP.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDACbiasP.setText("1328");
        TextFieldDACbiasP.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldDACbiasP, gridBagConstraints);

        jLabel2.setText("DAC Vn:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel9.add(jLabel2, gridBagConstraints);

        TextFieldDACbiasN.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDACbiasN.setText("2210");
        TextFieldDACbiasN.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel9.add(TextFieldDACbiasN, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel22.add(jPanel9, gridBagConstraints);

        ButtonSetPedDACtoADC.setText("<html><div style='text-align: center;'><body>Set DACs<br>Bias Offsets</body></div></html>");
        ButtonSetPedDACtoADC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetPedDACtoADCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel22.add(ButtonSetPedDACtoADC, gridBagConstraints);

        ButtonSetPedADCtoDAC.setText("<html><div style='text-align: center;'><body>Set ADC<br>Pedestal</body></div></html>");
        ButtonSetPedADCtoDAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetPedADCtoDACActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel22.add(ButtonSetPedADCtoDAC, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        jLabel16.setText("ADC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel23.add(jLabel16, gridBagConstraints);

        TextFieldADCped.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldADCped.setText("210");
        TextFieldADCped.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel23.add(TextFieldADCped, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel22.add(jPanel23, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(jPanel22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel21, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel8, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0};
        jPanel12Layout.rowHeights = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel12.setLayout(jPanel12Layout);

        java.awt.GridBagLayout jPanel4Layout = new java.awt.GridBagLayout();
        jPanel4Layout.columnWidths = new int[] {0, 10, 0};
        jPanel4Layout.rowHeights = new int[] {0};
        jPanel4.setLayout(jPanel4Layout);

        CheckBoxBroadcastMD.setText("MD Broadcast");
        CheckBoxBroadcastMD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxBroadcastMDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel4.add(CheckBoxBroadcastMD, gridBagConstraints);

        jPanel24.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0, 5, 0};
        jPanel14Layout.rowHeights = new int[] {0};
        jPanel14.setLayout(jPanel14Layout);

        jLabel4.setText("DB FPGA Side:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jLabel4, gridBagConstraints);

        ComboBoxDBSide.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Broadcast", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel14.add(ComboBoxDBSide, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel24, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel11Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel11.setLayout(jPanel11Layout);

        ButtonSetMD1Plots.setText("MD 1");
        ButtonSetMD1Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD1PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD1Plots, gridBagConstraints);

        ButtonSetMD2Plots.setText("MD 2");
        ButtonSetMD2Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD2PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD2Plots, gridBagConstraints);

        ButtonSetMD3Plots.setText("MD 3");
        ButtonSetMD3Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD3PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD3Plots, gridBagConstraints);

        ButtonSetMD4Plots.setText("MD 4");
        ButtonSetMD4Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD4PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD4Plots, gridBagConstraints);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("On/Off");
        CheckBoxMD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setText("On/Off");
        CheckBoxMD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setText("On/Off");
        CheckBoxMD3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setText("On/Off");
        CheckBoxMD4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel12.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 3, 0, 3, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        ButtonSetLGPlots.setText("Low Gain");
        ButtonSetLGPlots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetLGPlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetLGPlots, gridBagConstraints);

        ButtonSetHGPlots.setText("High Gain");
        ButtonSetHGPlots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetHGPlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetHGPlots, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel12.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel12, gridBagConstraints);

        java.awt.GridBagLayout jPanel15Layout = new java.awt.GridBagLayout();
        jPanel15Layout.columnWidths = new int[] {0};
        jPanel15Layout.rowHeights = new int[] {0, 3, 0, 3, 0, 3, 0, 3, 0};
        jPanel15.setLayout(jPanel15Layout);

        java.awt.GridBagLayout jPanel27Layout = new java.awt.GridBagLayout();
        jPanel27Layout.columnWidths = new int[] {0, 5, 0};
        jPanel27Layout.rowHeights = new int[] {0};
        jPanel27.setLayout(jPanel27Layout);

        jLabel5.setText("Charge: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel27.add(jLabel5, gridBagConstraints);

        ComboBoxScanUnits.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DACs", "pC" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel27.add(ComboBoxScanUnits, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel15.add(jPanel27, gridBagConstraints);

        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel20Layout.rowHeights = new int[] {0};
        jPanel20.setLayout(jPanel20Layout);

        jLabel12.setText("Steps:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel12, gridBagConstraints);

        TextFieldSteps.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldSteps.setText("10");
        TextFieldSteps.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel20.add(TextFieldSteps, gridBagConstraints);

        jLabel13.setText("Evts/step:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel13, gridBagConstraints);

        TextFieldEvtsStep.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldEvtsStep.setText("10");
        TextFieldEvtsStep.setPreferredSize(new java.awt.Dimension(50, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel20.add(TextFieldEvtsStep, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel15.add(jPanel20, gridBagConstraints);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel16Layout.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout);

        ButtonStartRunCISLinearity.setText("Start Run");
        ButtonStartRunCISLinearity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStartRunCISLinearityActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStartRunCISLinearity, gridBagConstraints);

        ButtonStopRunCISLinearity.setText("Stop Run");
        ButtonStopRunCISLinearity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStopRunCISLinearityActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStopRunCISLinearity, gridBagConstraints);

        LabelCISlinearityRunStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCISlinearityRunStatus.setText("stopped");
        LabelCISlinearityRunStatus.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel16.add(LabelCISlinearityRunStatus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel15.add(jPanel16, gridBagConstraints);

        java.awt.GridBagLayout jPanel17Layout = new java.awt.GridBagLayout();
        jPanel17Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel17Layout.rowHeights = new int[] {0, 3, 0, 3, 0};
        jPanel17.setLayout(jPanel17Layout);

        jLabel9.setText("Events/Step:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel9, gridBagConstraints);

        LabelEventsPerStepCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelEventsPerStepCounter.setText("0");
        LabelEventsPerStepCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelEventsPerStepCounter, gridBagConstraints);

        jLabel3.setText("Scan point:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel3, gridBagConstraints);

        LabelScanPointCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelScanPointCounter.setText("0");
        LabelScanPointCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelScanPointCounter, gridBagConstraints);

        jLabel14.setText("DAC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel14, gridBagConstraints);

        LabelDACCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelDACCounter.setText("0");
        LabelDACCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelDACCounter, gridBagConstraints);

        jLabel17.setText("pC (HG/LG):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel17, gridBagConstraints);

        LabelpCCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelpCCounter.setText("0.0/0.0");
        LabelpCCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel17.add(LabelpCCounter, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel15.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel2, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Profile Histograms"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        PanelPlot1.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot1.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot1, gridBagConstraints);

        PanelPlot2.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot2.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot2, gridBagConstraints);

        PanelPlot3.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot3.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot3, gridBagConstraints);

        PanelPlot4.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot4.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot4, gridBagConstraints);

        PanelPlot5.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot5.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot5, gridBagConstraints);

        PanelPlot6.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot6.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot6, gridBagConstraints);

        PanelPlot7.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot7.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot7, gridBagConstraints);

        PanelPlot8.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot8.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot8, gridBagConstraints);

        PanelPlot9.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot9.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot9, gridBagConstraints);

        PanelPlot10.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot10.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot10, gridBagConstraints);

        PanelPlot11.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot11.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot11, gridBagConstraints);

        PanelPlot12.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot12.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jPanel25.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel26Layout = new java.awt.GridBagLayout();
        jPanel26Layout.columnWidths = new int[] {0, 10, 0, 10, 0};
        jPanel26Layout.rowHeights = new int[] {0};
        jPanel26.setLayout(jPanel26Layout);

        ButtonSaveAsDefaults.setText("<html><div style='text-align: center;'><body>Save As Defaults<br>Settings</body></div></html>");
        ButtonSaveAsDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSaveAsDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonSaveAsDefaults, gridBagConstraints);

        ButtonLoadDefaults.setText("<html><div style='text-align: center;'><body>Load Defaults<br>Settings</body></div></html>");
        ButtonLoadDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonLoadDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonLoadDefaults, gridBagConstraints);

        ButtonShowDefaults.setText("<html><div style='text-align: center;'><body>Show Defaults<br>Settings</body></div></html>");
        ButtonShowDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel26.add(ButtonShowDefaults, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jPanel26, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel19.add(jPanel25, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        jPanel1.add(jPanel19, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Sets minidrawers to process data in broadcast mode.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxBroadcastMDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxBroadcastMDActionPerformed
        if (CheckBoxBroadcastMD.isSelected()) {
            MDbroadcast = 1;
        } else {
            MDbroadcast = 0;
        }
    }//GEN-LAST:event_CheckBoxBroadcastMDActionPerformed

    /**
     * Sets MD1 to process data. Unsets MD broadcast.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD1ActionPerformed
        MDsel.set(0, CheckBoxMD1.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD1ActionPerformed

    /**
     * Sets MD2 to process data. Unsets MD broadcast.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD2ActionPerformed
        MDsel.set(1, CheckBoxMD2.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD2ActionPerformed

    /**
     * Sets MD3 to process data. Unsets MD broadcast.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD3ActionPerformed
        MDsel.set(2, CheckBoxMD3.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD3ActionPerformed

    /**
     * Sets MD4 to process data. Unsets MD broadcast.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD4ActionPerformed
        MDsel.set(3, CheckBoxMD4.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD4ActionPerformed

    /**
     * Starts CIS lineartity run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStartRunCISLinearityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStartRunCISLinearityActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("STCISLinearityPanel -> no IPbus PPrconnection");
            return;
        }

        // Run in progress: do nothing...
        if ((CISLinearityRunStatus.get() == 1)
                || (FindBCRunStatus.get() == 1)
                || (STADCLinearityPanel.ADCLinearityRunStatus.get() == 1)
                || (STCISShapePanel.CISShapeRunStatus.get() == 1)
                || (STCISShapePanel.FindBCRunStatus.get() == 1)
                || (STPedestalPanel.PedestalRunStatusFlag.get() == 1)) {
            return;
        }

        // Updates the run status flag.
        CISLinearityRunStatus.set(1);

        // Executes the given task.
        try {
            executor.execute(RunnableStartRun);
        } catch (RejectedExecutionException e) {
            System.out.println("STCISLinearityPanel -> RejectedExecutionException: " + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("STCISLinearityPanel -> NullPointerException: " + e.getMessage());
        }
    }//GEN-LAST:event_ButtonStartRunCISLinearityActionPerformed

    /**
     * Stops CIS linearity run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStopRunCISLinearityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStopRunCISLinearityActionPerformed
        // If no IPbus PPr connection do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            return;
        }

        // If run in progress, terminates it by setting the corresponding flag.
        // Otherwise do nothing.
        if (CISLinearityRunStatus.get() == 1) {
            CISLinearityRunStatus.set(0);

            // Updates label for CIS linearity run status.
            LabelCISlinearityRunStatus.setText("terminating...");
            LabelCISlinearityRunStatus.setForeground(Color.blue);

            // Updates MainFrame label for CIS linearity run status.
            MainFrame.LabelCISLinearityRunMainFrame.setText("not running");
            MainFrame.LabelCISLinearityRunMainFrame.setForeground(Color.RED);
        }
    }//GEN-LAST:event_ButtonStopRunCISLinearityActionPerformed

    /**
     * Finds
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStartRunFindBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStartRunFindBCActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("STCISLinearityPanel -> no IPbus PPr connection");
            return;
        }

        // Run in progress: do nothing...
        if ((FindBCRunStatus.get() == 1)
                || (CISLinearityRunStatus.get() == 1)
                || (STCISShapePanel.CISShapeRunStatus.get() == 1)
                || (STCISShapePanel.FindBCRunStatus.get() == 1)
                || (STPedestalPanel.PedestalRunStatusFlag.get() == 1)) {
            return;
        }

        // Updates the run status flag.
        FindBCRunStatus.set(1);

        // Executes the given task.
        try {
            executor.execute(RunnableFindBC);
        } catch (RejectedExecutionException e) {
            System.out.println("ButtonFindBCActionPerformed -> RejectedExecutionException: " + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("ButtonFindBCActionPerformed -> NullPointerException: " + e.getMessage());
        }
    }//GEN-LAST:event_ButtonStartRunFindBCActionPerformed

    /**
     * Stop
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStopRunFindBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStopRunFindBCActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("STCISLinearityPanel -> no IPbus PPr connection");
            return;
        }

        // Flag to indicated Find BC run is in progress.
        if (FindBCRunStatus.get() == 1) {
            FindBCRunStatus.set(0);

            LabelFindBCRunStatus.setText("terminating...");
            LabelFindBCRunStatus.setForeground(Color.blue);
        }
    }//GEN-LAST:event_ButtonStopRunFindBCActionPerformed

    /**
     * Sets MD1 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD1PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD1PlotsActionPerformed
        if (MDsel.get(0) == 0) {
            return;
        }
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD1PlotsActionPerformed

    /**
     * Sets MD2 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD2PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD2PlotsActionPerformed
        if (MDsel.get(1) == 0) {
            return;
        }
        selectedMDPlot = 1;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(Color.GRAY);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD2PlotsActionPerformed

    /**
     * Sets MD3 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD3PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD3PlotsActionPerformed
        if (MDsel.get(2) == 0) {
            return;
        }
        selectedMDPlot = 2;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(Color.GRAY);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD3PlotsActionPerformed

    /**
     * Sets MD4 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD4PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD4PlotsActionPerformed
        if (MDsel.get(3) == 0) {
            return;
        }
        selectedMDPlot = 3;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetMD4PlotsActionPerformed

    /**
     * Sets LG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetLGPlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetLGPlotsActionPerformed
        selectedGainPlot = "LG";
        ButtonSetLGPlots.setBackground(Color.GRAY);
        ButtonSetHGPlots.setBackground(null);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetLGPlotsActionPerformed

    /**
     * Sets HG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetHGPlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetHGPlotsActionPerformed
        selectedGainPlot = "HG";
        ButtonSetLGPlots.setBackground(null);
        ButtonSetHGPlots.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
    }//GEN-LAST:event_ButtonSetHGPlotsActionPerformed

    /**
     * Sets pedestal DAC bias offsets from pedestal ADC counts.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetPedDACtoADCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetPedDACtoADCActionPerformed
        setPed = "DAC";

        ButtonSetPedDACtoADC.setBackground(Color.GRAY);
        ButtonSetPedADCtoDAC.setBackground(null);

        get_ped_DAC();
        Console.print_cr("Pedestal DAC bias positive offset: " + DACbiasP);
        Console.print_cr("Pedestal DAC bias negative offset: " + DACbiasN);

        // Converts pedestal DAC bias offsets to pedestal ADC counts.
        ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
        Console.print_cr("Pedestal ADC counts: " + ADCped);
        TextFieldADCped.setText(Integer.toString(ADCped));
    }//GEN-LAST:event_ButtonSetPedDACtoADCActionPerformed

    /**
     * Sets pedestal ADC counts from pedestal DAC bias offsets.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetPedADCtoDACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetPedADCtoDACActionPerformed
        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        setPed = "ADC";

        ButtonSetPedDACtoADC.setBackground(null);
        ButtonSetPedADCtoDAC.setBackground(Color.GRAY);

        get_ped_ADC();
        Console.print_cr("Pedestal ADC counts: " + ADCped);

        // Converts pedestal ADC counts to pedestal DAC bias offsets.
        VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
        DACbiasP = VinDACs.get(0);
        DACbiasN = VinDACs.get(1);

        Console.print_cr("Pedestal DAC bias positive offset: " + DACbiasP);
        Console.print_cr("Pedestal DAC bias negative offset: " + DACbiasN);

        TextFieldDACbiasP.setText(Integer.toString(DACbiasP));
        TextFieldDACbiasN.setText(Integer.toString(DACbiasN));
    }//GEN-LAST:event_ButtonSetPedADCtoDACActionPerformed

    /**
     * First gets all field values from text widgets. Then sets field values as
     * default values in properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSaveAsDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSaveAsDefaultsActionPerformed
        Console.print_cr("STCISLinearityPanel -> saving field values in properties file: " + Defaults.propsFile);
        get_all_parameters();
        Defaults.saveCISLinearityScanProperties();
    }//GEN-LAST:event_ButtonSaveAsDefaultsActionPerformed

    /**
     * Loads default pedestal run parameter values from properties configuration
     * file into field values. Then fills all text widgets with field values.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonLoadDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonLoadDefaultsActionPerformed
        Console.print_cr("STCISLinearityPanel -> loading default values from properties file: " + Defaults.propsFile);
        Defaults.loadCISLinearityProperties();
        set_all_parameters();
    }//GEN-LAST:event_ButtonLoadDefaultsActionPerformed

    /**
     * Shows default values from properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowDefaultsActionPerformed
        Defaults.showProperties();
    }//GEN-LAST:event_ButtonShowDefaultsActionPerformed

    /**
     * Sets default setting for CIS linearity scan plots.
     */
    public static void plot_BookHistograms() {
        String mdstr;

        // Defines 2d-array (4 minidrawers, 12 FEBs) of histogram JProfiles with HG/LG.
        h_plot_LG = new JScan[4][12];
        h_plot_HG = new JScan[4][12];

        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);
            for (int adc = 0; adc < 12; adc++) {
                if ("DACs".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);DAC counts;", mdstr, adc);
                    h_plot_HG[md][adc] = new JScan(htitle, nbSteps, min_DAC, max_DAC);
                    h_plot_HG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_HG[md][adc].setMarkerSize(4);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(75);

                    htitle = String.format("%s Ch %1d (LG);DAC counts;", mdstr, adc);
                    h_plot_LG[md][adc] = new JScan(htitle, nbSteps, min_DAC, max_DAC);
                    h_plot_LG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_LG[md][adc].setMarkerSize(4);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(75);

                } else if ("pC".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);pC;", mdstr, adc);
                    h_plot_HG[md][adc] = new JScan(htitle, nbSteps, min_pC_HG, max_pC_HG);
                    h_plot_HG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_pC_HG, hxhigh_pC_HG);
                    h_plot_HG[md][adc].setMarkerSize(4);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(40);

                    htitle = String.format("%s Ch %1d (LG);pC;", mdstr, adc);
                    h_plot_LG[md][adc] = new JScan(htitle, nbSteps, min_pC_LG, max_pC_LG);
                    h_plot_LG[md][adc].setYAxisMinMax(0, 4096);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_pC_LG, hxhigh_pC_LG);
                    h_plot_LG[md][adc].setMarkerSize(4);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(50);
                }
            }
        }

        // The JPanel where to place the histogram has to be added the property
        // of Set Layout to Border Layout by right-clicking on the Panel and 
        // select Set Layout.
        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }
        }

        // validate() method inherited from Java Container class.
        // Validates (lays out its subcomponents) this container and all 
        // of its subcomponents.
        for (int i = 0; i < 12; i++) {
            PanelPlots[i].validate();
        }
    }

    /**
     * Refresh plots.
     */
    private void plot_RefreshPanelPlots() {
        // Removes all present JProfile histogram components from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }
    }

    /**
     * Resets histogram settings.
     */
    private void plot_ResetHistograms() {
        String fs;
        String mdstr;

        // Removes all present JScan components from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int md = 0; md < 4; md++) {
            for (int adc = 0; adc < 12; adc++) {
                h_plot_LG[md][adc].reset();
                h_plot_HG[md][adc].reset();
            }
        }

        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);
            for (int adc = 0; adc < 12; adc++) {
                if ("DACs".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);DAC counts;", mdstr, adc);
                    h_plot_HG[md][adc].setTitle(htitle);
                    h_plot_HG[md][adc].setBins(nbSteps, min_DAC, max_DAC);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(75);
                    h_plot_HG[md][adc].draw();

                    htitle = String.format("%s Ch %1d (LG);DAC counts;", mdstr, adc);
                    h_plot_LG[md][adc].setTitle(htitle);
                    h_plot_LG[md][adc].setBins(nbSteps, min_DAC, max_DAC);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_DAC, hxhigh_DAC);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(75);
                    h_plot_LG[md][adc].draw();

                } else if ("pC".equals(scanUnits)) {
                    htitle = String.format("%s Ch %1d (HG);pC;", mdstr, adc);
                    h_plot_HG[md][adc].setTitle(htitle);
                    h_plot_HG[md][adc].setBins(nbSteps, min_pC_HG, max_pC_HG);
                    h_plot_HG[md][adc].setXAxisMinMax(hxlow_pC_HG, hxhigh_pC_HG);
                    h_plot_HG[md][adc].setXAxisTickMarkSpacing(40);
                    h_plot_HG[md][adc].draw();

                    htitle = String.format("%s Ch %1d (LG);pC;", mdstr, adc);
                    h_plot_LG[md][adc].setTitle(htitle);
                    h_plot_LG[md][adc].setBins(nbSteps, min_pC_LG, max_pC_LG);
                    h_plot_LG[md][adc].setXAxisMinMax(hxlow_pC_LG, hxhigh_pC_LG);
                    h_plot_LG[md][adc].setXAxisTickMarkSpacing(50);
                    h_plot_LG[md][adc].draw();
                }
            }
        }

        if (DebugSettings.getVerbose()) {
            fs = "    Total steps (HG): %d ";
            Console.print_cr(String.format(fs, nbSteps));
            fs = "    Total steps (LG): %d ";
            Console.print_cr(String.format(fs, nbSteps));
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            //PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }
    }

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares an ExecutorService interface (extends Executor interface), an
     * asynchronous execution mechanism which is capable of executing tasks
     * concurrently in the background. ExecutorService is created and return
     * from methods of the Executors Class.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for run_start_CIS_linearity() method.
     */
    private Runnable RunnableStartRun;

    /**
     * Declares Runnable for run_start_find_BC() method.
     */
    private Runnable RunnableFindBC;

    /**
     * Flag to define the CIS linearity run status. Atomic Java variables are
     * lock-free thread-safe programming variables whose values may be updated
     * atomically. They allow concurrent accesses (used by many threads
     * concurrently).
     */
    public static AtomicInteger CISLinearityRunStatus;

    /**
     * Flag to indicate Find BC run is in progress.
     */
    public static AtomicInteger FindBCRunStatus;

    /**
     * Selected DAC bias positive pedestal.
     */
    public static Integer DACbiasP;

    /**
     * Selected DAC bias negative pedestal.
     */
    public static Integer DACbiasN;

    /**
     * Selected ADC pedestal counts.
     */
    public static Integer ADCped;

    /**
     * Selected BCID charge.
     */
    public static Integer BCID_charge;

    /**
     * Selected BCID discharge.
     */
    public static Integer BCID_discharge;

    /**
     * Selected BCID for L1A.
     */
    public static Integer BCID;

    /**
     * Selected gain.
     */
    public static Integer gain;

    /**
     * Selected units for the scan (DACs or pC).
     */
    public static String scanUnits;

    /**
     * Selected Daughterboard FPGA side.
     */
    public static Integer dbside;

    /**
     * Selected Daughterboard FPGA side (as String).
     */
    public static String dbsidestring;

    /**
     * Selected ArrayList with minidrawers to be filled with data.
     */
    public static ArrayList<Integer> MDsel;

    /**
     * Selected minidrawers broadcast flag.
     */
    public static Integer MDbroadcast;

    /**
     * Selected number of DAC step.
     */
    public static Integer nbSteps;

    /**
     * Selected events per DAC step.
     */
    public static Integer stepEvents;

    /**
     * Selected way to set pedestal (DACs or ADC counts)
     */
    public static String setPed;

    /**
     * Step length in DACs (both for LG and HG).
     */
    private Float step_length_DAC;

    /**
     * Step length in pC for LG.
     */
    private Float step_length_pC_LG;

    /**
     * Step length in pC for HG.
     */
    private Float step_length_pC_HG;

    /**
     * Total selected MDs.
     */
    private Integer MDtot;

    /**
     * Declares AtomicInteger counter for proccessed step.
     */
    AtomicInteger ProcScanPoint;

    /**
     * Declares AtomicInteger counter for proccessed events per step.
     */
    AtomicInteger ProcEventsPerStep;

    private static javax.swing.JPanel[] PanelPlots;

    private static String htitle;

    private static Float hxlow_DAC;
    private static Float hxhigh_DAC;

    private static Float min_DAC;
    private static Float max_DAC;

    private static Float hxlow_pC_HG;
    private static Float hxhigh_pC_HG;

    private static Float min_pC_HG;
    private static Float max_pC_HG;

    private static Float hxlow_pC_LG;
    private static Float hxhigh_pC_LG;

    private static Float min_pC_LG;
    private static Float max_pC_LG;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of JScan plots with HG (high
     * gain) CIS values.
     */
    private static JScan[][] h_plot_HG;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of Jscan plots with LG (low
     * gain) CIS values.
     */
    private static JScan[][] h_plot_LG;

    /**
     * Selected minidrawer for plots.
     */
    public static Integer selectedMDPlot;

    /**
     * Selected gain for plots.
     */
    public static String selectedGainPlot;

    /**
     * Declares object with HG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataHG;

    /**
     * Declares object with LG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataLG;

    private Integer[][] dataMaxLG;
    private Integer[][] dataMaxHG;

    private Integer[][] dataIdxMaxLG;
    private Integer[][] dataIdxMaxHG;

    public Integer LastEvtBCID;
    public Integer LastEvtL1ID;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonLoadDefaults;
    private javax.swing.JButton ButtonSaveAsDefaults;
    private javax.swing.JButton ButtonSetHGPlots;
    private javax.swing.JButton ButtonSetLGPlots;
    private javax.swing.JButton ButtonSetMD1Plots;
    private javax.swing.JButton ButtonSetMD2Plots;
    private javax.swing.JButton ButtonSetMD3Plots;
    private javax.swing.JButton ButtonSetMD4Plots;
    public static javax.swing.JButton ButtonSetPedADCtoDAC;
    public static javax.swing.JButton ButtonSetPedDACtoADC;
    private javax.swing.JButton ButtonShowDefaults;
    private javax.swing.JButton ButtonStartRunCISLinearity;
    private javax.swing.JButton ButtonStartRunFindBC;
    private javax.swing.JButton ButtonStopRunCISLinearity;
    private javax.swing.JButton ButtonStopRunFindBC;
    public static javax.swing.JCheckBox CheckBoxBroadcastMD;
    public static javax.swing.JCheckBox CheckBoxMD1;
    public static javax.swing.JCheckBox CheckBoxMD2;
    public static javax.swing.JCheckBox CheckBoxMD3;
    public static javax.swing.JCheckBox CheckBoxMD4;
    public static javax.swing.JComboBox<String> ComboBoxDBSide;
    public static javax.swing.JComboBox<String> ComboBoxGain;
    public static javax.swing.JComboBox<String> ComboBoxScanUnits;
    private javax.swing.JLabel LabelCISlinearityRunStatus;
    private javax.swing.JLabel LabelDACCounter;
    private javax.swing.JLabel LabelEventsPerStepCounter;
    private javax.swing.JLabel LabelFindBCRunStatus;
    private javax.swing.JLabel LabelScanPointCounter;
    private javax.swing.JLabel LabelpCCounter;
    private javax.swing.JPanel PanelPlot1;
    private javax.swing.JPanel PanelPlot10;
    private javax.swing.JPanel PanelPlot11;
    private javax.swing.JPanel PanelPlot12;
    private javax.swing.JPanel PanelPlot2;
    private javax.swing.JPanel PanelPlot3;
    private javax.swing.JPanel PanelPlot4;
    private javax.swing.JPanel PanelPlot5;
    private javax.swing.JPanel PanelPlot6;
    private javax.swing.JPanel PanelPlot7;
    private javax.swing.JPanel PanelPlot8;
    private javax.swing.JPanel PanelPlot9;
    public static javax.swing.JTextField TextFieldADCped;
    public static javax.swing.JTextField TextFieldBCtoRead;
    public static javax.swing.JTextField TextFieldChargeBC;
    public static javax.swing.JTextField TextFieldDACbiasN;
    public static javax.swing.JTextField TextFieldDACbiasP;
    public static javax.swing.JTextField TextFieldDischargeBC;
    public static javax.swing.JTextField TextFieldEvtsStep;
    public static javax.swing.JTextField TextFieldSteps;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
