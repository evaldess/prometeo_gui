package tilepprdemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.JPanel;
import javax.swing.JLabel;

import histogram.JH1;
import tilepprjavalibrary.lib.PPrLib;
import utilities.DataArrayList;

/**
 * Pedestal panel controls window.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class STPedestalPanel extends javax.swing.JPanel {

    /**
     * Constructor. Creates new form PedestalPanel.
     *
     * @param pprlib Input PPrLib object.
     */
    public STPedestalPanel(PPrLib pprlib) {
        System.out.println("STPedestalPanel -> constructor");

        // NetBeans components settings.
        initComponents();

        // Set PPrLib class object as given by constructor argument.
        this.pprlib = pprlib;

        // Defines JPanel array with JPanel channels for plots.
        PanelPlots = new JPanel[]{
            PanelPlot1, PanelPlot2, PanelPlot3,
            PanelPlot4, PanelPlot5, PanelPlot6,
            PanelPlot7, PanelPlot8, PanelPlot9,
            PanelPlot10, PanelPlot11, PanelPlot12};

        // Define array of JLabels for pedestal channel means to be shown.
        LabelMean = new JLabel[]{
            LabelMeanCh0, LabelMeanCh1, LabelMeanCh2,
            LabelMeanCh3, LabelMeanCh4, LabelMeanCh5,
            LabelMeanCh6, LabelMeanCh7, LabelMeanCh8,
            LabelMeanCh9, LabelMeanCh10, LabelMeanCh11};

        // Define array of JLabels for pedestal channel RMS to be shown
        LabelRMS = new JLabel[]{
            LabelRMSCh0, LabelRMSCh1, LabelRMSCh2,
            LabelRMSCh3, LabelRMSCh4, LabelRMSCh5,
            LabelRMSCh6, LabelRMSCh7, LabelRMSCh8,
            LabelRMSCh9, LabelRMSCh10, LabelRMSCh11};

        for (Integer adc = 0; adc < 12; adc++) {
            LabelMean[adc].setForeground(Color.blue);
            LabelRMS[adc].setForeground(Color.blue);
        }

        // Defines Run Status active flag through Atomic Integer. 
        PedestalRunStatusFlag = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed events.
        ProcEvents = new AtomicInteger(0);

        // Defines Atomic Integer variable with number of processed entries (events x samples).
        ProcEntries = new AtomicInteger(0);

        fillHistosPossible = new AtomicInteger(0);

        smartBinning = false;

        // Define ArrayList with selected minidrawers.
        MDsel = new ArrayList<>();

        // Define Integer two-dimensional arrays of ArrayList with data.
        dataLG = new DataArrayList[4][12];
        dataHG = new DataArrayList[4][12];

        // Initialize the 2d array so that it is filled with Empty ArrayList<>'s'.
        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                dataHG[md][adc] = new DataArrayList();
                dataLG[md][adc] = new DataArrayList();
            }
        }

        // Define Integer two-dimensional arrays with mean and RMS values.
        dataMeanHG = new Double[4][12];
        dataRMSHG = new Double[4][12];

        dataMeanLG = new Double[4][12];
        dataRMSLG = new Double[4][12];

        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                dataMeanHG[md][adc] = 0.0;
                dataMeanLG[md][adc] = 0.0;
                dataRMSHG[md][adc] = 0.0;
                dataRMSLG[md][adc] = 0.0;
            }
        }

        // Default selected MD to show plots.
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);

        // Default selected gain to show plots.
        selectedGainPlot = "HG";
        ButtonSetHGPlot.setBackground(Color.GRAY);

        LabelRunStatus.setText("stopped");
        LabelRunStatus.setForeground(Color.RED);

        LabelEventsCounter.setForeground(Color.blue);
        LabelEntriesCounter.setForeground(Color.blue);
        LabelLastEvtBCIDCounter.setForeground(Color.blue);
        LabelLastEvtL1IDCounter.setForeground(Color.blue);

        LabelSmartBinning.setText("");

        // Set field values with NetBeans default text widgets.
        // Needed to define and set initial field values.
        get_all_parameters();

        setPed = "DAC";
        ButtonSetPedDACtoADC.setBackground(Color.GRAY);
        ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
        TextFieldADCped.setText(Integer.toString(ADCped));

        // Defines Runnables through lambda expressions.
        initDefineRunnables();

        // Defines instance of ThreadPoolExecutor class to create a thread pool.
        // A thread pool manages a collection of Runnable threads.
        // This class implements Executor and ExecutorService interfaces.
        executor = new ThreadPoolExecutor(
                10,
                10,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(),
                new TBThreadFactory("STPedestalPanel")
        );
    }

    /**
     * Gets all field values from text widgets.
     */
    private synchronized void get_all_parameters() {
        get_TotEvents();
        get_BCID();
        get_samples();
        get_DB_FPGA_side();
        get_MDs();
        get_ped_DAC();
        get_ped_ADC();
        get_bins();
    }

    // Set text widgets with present field values.
    public static synchronized void set_all_parameters() {
        set_TotEvents();
        set_BCID();
        set_samples();
        set_DB_FPGA_side();
        set_MDs();
        set_ped();
        set_ped_DAC();
        set_ped_ADC();
        set_bins();
        set_smart_binning();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ComboBoxSamples = new javax.swing.JComboBox<>();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        TextFieldBCID = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        TextFieldDACbiasP = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextFieldDACbiasN = new javax.swing.JTextField();
        ButtonSetPedDACtoADC = new javax.swing.JButton();
        ButtonSetPedADCtoDAC = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        TextFieldADCped = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        CheckBoxBroadcastMD = new javax.swing.JCheckBox();
        jPanel22 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ComboBoxDBSide = new javax.swing.JComboBox<>();
        jPanel11 = new javax.swing.JPanel();
        ButtonSetMD1Plots = new javax.swing.JButton();
        ButtonSetMD2Plots = new javax.swing.JButton();
        ButtonSetMD3Plots = new javax.swing.JButton();
        ButtonSetMD4Plots = new javax.swing.JButton();
        CheckBoxMD1 = new javax.swing.JCheckBox();
        CheckBoxMD2 = new javax.swing.JCheckBox();
        CheckBoxMD3 = new javax.swing.JCheckBox();
        CheckBoxMD4 = new javax.swing.JCheckBox();
        jPanel13 = new javax.swing.JPanel();
        ButtonSetLGPlot = new javax.swing.JButton();
        ButtonSetHGPlot = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        TextFieldEventsToProcess = new javax.swing.JTextField();
        ButtonStartPedestalRun = new javax.swing.JButton();
        ButtonStopPedestalRun = new javax.swing.JButton();
        LabelRunStatus = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        LabelEventsCounter = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        LabelEntriesCounter = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        LabelLastEvtBCIDCounter = new javax.swing.JLabel();
        LabelLastEvtL1IDCounter = new javax.swing.JLabel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 0), new java.awt.Dimension(15, 32767));
        jPanel3 = new javax.swing.JPanel();
        PanelPlot1 = new javax.swing.JPanel();
        PanelPlot2 = new javax.swing.JPanel();
        PanelPlot3 = new javax.swing.JPanel();
        PanelPlot4 = new javax.swing.JPanel();
        PanelPlot5 = new javax.swing.JPanel();
        PanelPlot6 = new javax.swing.JPanel();
        PanelPlot7 = new javax.swing.JPanel();
        PanelPlot8 = new javax.swing.JPanel();
        PanelPlot9 = new javax.swing.JPanel();
        PanelPlot10 = new javax.swing.JPanel();
        PanelPlot11 = new javax.swing.JPanel();
        PanelPlot12 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        ButtonSaveAsDefaults = new javax.swing.JButton();
        ButtonLoadDefaults = new javax.swing.JButton();
        ButtonShowDefaults = new javax.swing.JButton();
        jPanel24 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        TextFieldBins = new javax.swing.JTextField();
        jPanel26 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldMinBin = new javax.swing.JTextField();
        jPanel27 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        TextFieldMaxBin = new javax.swing.JTextField();
        ButtonResetPlots = new javax.swing.JButton();
        ButtonSmartBinning = new javax.swing.JButton();
        LabelSmartBinning = new javax.swing.JLabel();
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(30, 0), new java.awt.Dimension(70, 0), new java.awt.Dimension(30, 32767));
        jPanel18 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        LabelRMSCh0 = new javax.swing.JLabel();
        LabelRMSCh1 = new javax.swing.JLabel();
        LabelMeanCh0 = new javax.swing.JLabel();
        LabelMeanCh1 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        LabelMeanCh2 = new javax.swing.JLabel();
        LabelRMSCh2 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        LabelMeanCh3 = new javax.swing.JLabel();
        LabelMeanCh4 = new javax.swing.JLabel();
        LabelMeanCh5 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        LabelRMSCh3 = new javax.swing.JLabel();
        LabelRMSCh4 = new javax.swing.JLabel();
        LabelRMSCh5 = new javax.swing.JLabel();
        LabelRMSCh6 = new javax.swing.JLabel();
        LabelMeanCh6 = new javax.swing.JLabel();
        LabelMeanCh7 = new javax.swing.JLabel();
        LabelMeanCh8 = new javax.swing.JLabel();
        LabelMeanCh9 = new javax.swing.JLabel();
        LabelMeanCh10 = new javax.swing.JLabel();
        LabelRMSCh7 = new javax.swing.JLabel();
        LabelRMSCh8 = new javax.swing.JLabel();
        LabelRMSCh9 = new javax.swing.JLabel();
        LabelRMSCh10 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        LabelMeanCh11 = new javax.swing.JLabel();
        LabelRMSCh11 = new javax.swing.JLabel();

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0};
        jPanel1Layout.rowHeights = new int[] {0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0};
        jPanel1.setLayout(jPanel1Layout);

        java.awt.GridBagLayout jPanel2Layout = new java.awt.GridBagLayout();
        jPanel2Layout.columnWidths = new int[] {0, 35, 0, 35, 0, 35, 0};
        jPanel2Layout.rowHeights = new int[] {0};
        jPanel2.setLayout(jPanel2Layout);

        java.awt.GridBagLayout jPanel5Layout = new java.awt.GridBagLayout();
        jPanel5Layout.columnWidths = new int[] {0};
        jPanel5Layout.rowHeights = new int[] {0, 7, 0};
        jPanel5.setLayout(jPanel5Layout);

        java.awt.GridBagLayout jPanel14Layout = new java.awt.GridBagLayout();
        jPanel14Layout.columnWidths = new int[] {0, 5, 0};
        jPanel14Layout.rowHeights = new int[] {0};
        jPanel14.setLayout(jPanel14Layout);

        jLabel7.setText("Samples:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel14.add(jLabel7, gridBagConstraints);

        ComboBoxSamples.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16" }));
        ComboBoxSamples.setSelectedIndex(15);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel14.add(ComboBoxSamples, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(jPanel14, gridBagConstraints);

        java.awt.GridBagLayout jPanel6Layout = new java.awt.GridBagLayout();
        jPanel6Layout.columnWidths = new int[] {0, 5, 0};
        jPanel6Layout.rowHeights = new int[] {0};
        jPanel6.setLayout(jPanel6Layout);

        jLabel6.setText("BC to read:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel6.add(jLabel6, gridBagConstraints);

        TextFieldBCID.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldBCID.setText("3200");
        TextFieldBCID.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel6.add(TextFieldBCID, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel5, gridBagConstraints);

        java.awt.GridBagLayout jPanel8Layout = new java.awt.GridBagLayout();
        jPanel8Layout.columnWidths = new int[] {0, 10, 0};
        jPanel8Layout.rowHeights = new int[] {0, 7, 0};
        jPanel8.setLayout(jPanel8Layout);

        java.awt.GridBagLayout jPanel9Layout = new java.awt.GridBagLayout();
        jPanel9Layout.columnWidths = new int[] {0, 5, 0};
        jPanel9Layout.rowHeights = new int[] {0, 0, 0};
        jPanel9.setLayout(jPanel9Layout);

        jLabel1.setText("DAC Vp:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel9.add(jLabel1, gridBagConstraints);

        TextFieldDACbiasP.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDACbiasP.setText("1328");
        TextFieldDACbiasP.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel9.add(TextFieldDACbiasP, gridBagConstraints);

        jLabel2.setText("DAC Vn:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel9.add(jLabel2, gridBagConstraints);

        TextFieldDACbiasN.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldDACbiasN.setText("2210");
        TextFieldDACbiasN.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel9.add(TextFieldDACbiasN, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jPanel9, gridBagConstraints);

        ButtonSetPedDACtoADC.setText("<html><div style='text-align: center;'><body>Set DACs<br>Bias Offsets</body></div></html>");
        ButtonSetPedDACtoADC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetPedDACtoADCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(ButtonSetPedDACtoADC, gridBagConstraints);

        ButtonSetPedADCtoDAC.setText("<html><div style='text-align: center;'><body>Set ADC<br>Pedestal</body></div></html>");
        ButtonSetPedADCtoDAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetPedADCtoDACActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel8.add(ButtonSetPedADCtoDAC, gridBagConstraints);

        java.awt.GridBagLayout jPanel19Layout = new java.awt.GridBagLayout();
        jPanel19Layout.columnWidths = new int[] {0, 5, 0};
        jPanel19Layout.rowHeights = new int[] {0};
        jPanel19.setLayout(jPanel19Layout);

        jLabel16.setText("ADC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel19.add(jLabel16, gridBagConstraints);

        TextFieldADCped.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldADCped.setText("210");
        TextFieldADCped.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel19.add(TextFieldADCped, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel8.add(jPanel19, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel8, gridBagConstraints);

        java.awt.GridBagLayout jPanel12Layout = new java.awt.GridBagLayout();
        jPanel12Layout.columnWidths = new int[] {0};
        jPanel12Layout.rowHeights = new int[] {0, 7, 0, 7, 0, 7, 0, 7, 0};
        jPanel12.setLayout(jPanel12Layout);

        java.awt.GridBagLayout jPanel7Layout = new java.awt.GridBagLayout();
        jPanel7Layout.columnWidths = new int[] {0, 10, 0};
        jPanel7Layout.rowHeights = new int[] {0};
        jPanel7.setLayout(jPanel7Layout);

        CheckBoxBroadcastMD.setText("MD Broadcast");
        CheckBoxBroadcastMD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxBroadcastMDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel7.add(CheckBoxBroadcastMD, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel10Layout = new java.awt.GridBagLayout();
        jPanel10Layout.columnWidths = new int[] {0, 5, 0};
        jPanel10Layout.rowHeights = new int[] {0};
        jPanel10.setLayout(jPanel10Layout);

        jLabel4.setText("DB FPGA Side:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel10.add(jLabel4, gridBagConstraints);

        ComboBoxDBSide.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Broadcast", "Side A", "Side B" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel10.add(ComboBoxDBSide, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel22.add(jPanel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel7.add(jPanel22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel12.add(jPanel7, gridBagConstraints);

        java.awt.GridBagLayout jPanel11Layout = new java.awt.GridBagLayout();
        jPanel11Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel11Layout.rowHeights = new int[] {0, 5, 0, 5, 0};
        jPanel11.setLayout(jPanel11Layout);

        ButtonSetMD1Plots.setText("MD 1");
        ButtonSetMD1Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD1PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD1Plots, gridBagConstraints);

        ButtonSetMD2Plots.setText("MD 2");
        ButtonSetMD2Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD2PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD2Plots, gridBagConstraints);

        ButtonSetMD3Plots.setText("MD 3");
        ButtonSetMD3Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD3PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD3Plots, gridBagConstraints);

        ButtonSetMD4Plots.setText("MD 4");
        ButtonSetMD4Plots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetMD4PlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel11.add(ButtonSetMD4Plots, gridBagConstraints);

        CheckBoxMD1.setSelected(true);
        CheckBoxMD1.setText("On/Off");
        CheckBoxMD1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD1, gridBagConstraints);

        CheckBoxMD2.setText("On/Off");
        CheckBoxMD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD2, gridBagConstraints);

        CheckBoxMD3.setText("On/Off");
        CheckBoxMD3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD3, gridBagConstraints);

        CheckBoxMD4.setText("On/Off");
        CheckBoxMD4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxMD4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel11.add(CheckBoxMD4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel12.add(jPanel11, gridBagConstraints);

        java.awt.GridBagLayout jPanel13Layout = new java.awt.GridBagLayout();
        jPanel13Layout.columnWidths = new int[] {0, 3, 0, 3, 0};
        jPanel13Layout.rowHeights = new int[] {0};
        jPanel13.setLayout(jPanel13Layout);

        ButtonSetLGPlot.setText("Low Gain");
        ButtonSetLGPlot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetLGPlotActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetLGPlot, gridBagConstraints);

        ButtonSetHGPlot.setText("High Gain");
        ButtonSetHGPlot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSetHGPlotActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(ButtonSetHGPlot, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel12.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel12, gridBagConstraints);

        java.awt.GridBagLayout jPanel15Layout = new java.awt.GridBagLayout();
        jPanel15Layout.columnWidths = new int[] {0};
        jPanel15Layout.rowHeights = new int[] {0, 10, 0};
        jPanel15.setLayout(jPanel15Layout);

        java.awt.GridBagLayout jPanel16Layout = new java.awt.GridBagLayout();
        jPanel16Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel16Layout.rowHeights = new int[] {0};
        jPanel16.setLayout(jPanel16Layout);

        jLabel3.setText("Events:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel16.add(jLabel3, gridBagConstraints);

        TextFieldEventsToProcess.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldEventsToProcess.setText("100");
        TextFieldEventsToProcess.setPreferredSize(new java.awt.Dimension(80, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel16.add(TextFieldEventsToProcess, gridBagConstraints);

        ButtonStartPedestalRun.setText("Start Run");
        ButtonStartPedestalRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStartPedestalRunActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStartPedestalRun, gridBagConstraints);

        ButtonStopPedestalRun.setText("Stop Run");
        ButtonStopPedestalRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonStopPedestalRunActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel16.add(ButtonStopPedestalRun, gridBagConstraints);

        LabelRunStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRunStatus.setText("stopped");
        LabelRunStatus.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel16.add(LabelRunStatus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel15.add(jPanel16, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        jLabel9.setText("Events:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel9, gridBagConstraints);

        LabelEventsCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelEventsCounter.setText("0");
        LabelEventsCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel17.add(LabelEventsCounter, gridBagConstraints);

        jLabel8.setText("Total entries:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jLabel8, gridBagConstraints);

        LabelEntriesCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelEntriesCounter.setText("0");
        LabelEntriesCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel17.add(LabelEntriesCounter, gridBagConstraints);

        jLabel18.setText("Last Evt BCID:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel17.add(jLabel18, gridBagConstraints);

        jLabel19.setText("Last Evt L1ID:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel17.add(jLabel19, gridBagConstraints);

        LabelLastEvtBCIDCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelLastEvtBCIDCounter.setText("0");
        LabelLastEvtBCIDCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel17.add(LabelLastEvtBCIDCounter, gridBagConstraints);

        LabelLastEvtL1IDCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelLastEvtL1IDCounter.setText("0");
        LabelLastEvtL1IDCounter.setPreferredSize(new java.awt.Dimension(70, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel17.add(LabelLastEvtL1IDCounter, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        jPanel17.add(filler1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel17.add(filler2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel15.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel1.add(jPanel2, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Plots"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        PanelPlot1.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot1.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot1, gridBagConstraints);

        PanelPlot2.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot2.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot2, gridBagConstraints);

        PanelPlot3.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot3.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot3, gridBagConstraints);

        PanelPlot4.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot4.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot4, gridBagConstraints);

        PanelPlot5.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot5.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot5, gridBagConstraints);

        PanelPlot6.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot6.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        jPanel3.add(PanelPlot6, gridBagConstraints);

        PanelPlot7.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot7.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot7, gridBagConstraints);

        PanelPlot8.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot8.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot8, gridBagConstraints);

        PanelPlot9.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot9.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot9, gridBagConstraints);

        PanelPlot10.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot10.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot10, gridBagConstraints);

        PanelPlot11.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot11.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot11, gridBagConstraints);

        PanelPlot12.setPreferredSize(new java.awt.Dimension(200, 200));
        PanelPlot12.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        jPanel3.add(PanelPlot12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel21Layout = new java.awt.GridBagLayout();
        jPanel21Layout.columnWidths = new int[] {0, 10, 0, 10, 0};
        jPanel21Layout.rowHeights = new int[] {0};
        jPanel21.setLayout(jPanel21Layout);

        ButtonSaveAsDefaults.setText("<html><div style='text-align: center;'><body>Save As Defaults<br>Settings</body></div></html>");
        ButtonSaveAsDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSaveAsDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel21.add(ButtonSaveAsDefaults, gridBagConstraints);

        ButtonLoadDefaults.setText("<html><div style='text-align: center;'><body>Load Defaults<br>Settings</body></div></html>");
        ButtonLoadDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonLoadDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel21.add(ButtonLoadDefaults, gridBagConstraints);

        ButtonShowDefaults.setText("<html><div style='text-align: center;'><body>Show Defaults<br>Settings</body></div></html>");
        ButtonShowDefaults.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonShowDefaultsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel21.add(ButtonShowDefaults, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel21, gridBagConstraints);

        java.awt.GridBagLayout jPanel24Layout = new java.awt.GridBagLayout();
        jPanel24Layout.columnWidths = new int[] {0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0, 13, 0};
        jPanel24Layout.rowHeights = new int[] {0, 3, 0};
        jPanel24.setLayout(jPanel24Layout);

        jPanel25.setLayout(new java.awt.GridBagLayout());

        jLabel10.setText("Bins:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel25.add(jLabel10, gridBagConstraints);

        TextFieldBins.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldBins.setText("100");
        TextFieldBins.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel25.add(TextFieldBins, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jPanel25, gridBagConstraints);

        jPanel26.setLayout(new java.awt.GridBagLayout());

        jLabel12.setText("Min ADC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel26.add(jLabel12, gridBagConstraints);

        TextFieldMinBin.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMinBin.setText("200");
        TextFieldMinBin.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel26.add(TextFieldMinBin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jPanel26, gridBagConstraints);

        jPanel27.setLayout(new java.awt.GridBagLayout());

        jLabel15.setText("Max ADC:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel27.add(jLabel15, gridBagConstraints);

        TextFieldMaxBin.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldMaxBin.setText("300");
        TextFieldMaxBin.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel27.add(TextFieldMaxBin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel24.add(jPanel27, gridBagConstraints);

        ButtonResetPlots.setText("Reset Plots");
        ButtonResetPlots.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonResetPlotsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel24.add(ButtonResetPlots, gridBagConstraints);

        ButtonSmartBinning.setText("<html><div style='text-align: center;'><body>Automatic<br>Binning</body></div></html>");
        ButtonSmartBinning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSmartBinningActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel24.add(ButtonSmartBinning, gridBagConstraints);

        LabelSmartBinning.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelSmartBinning.setText("not used");
        LabelSmartBinning.setPreferredSize(new java.awt.Dimension(80, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel24.add(LabelSmartBinning, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel4.add(jPanel24, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel4.add(filler4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        jPanel1.add(jPanel4, gridBagConstraints);

        java.awt.GridBagLayout jPanel18Layout = new java.awt.GridBagLayout();
        jPanel18Layout.columnWidths = new int[] {0};
        jPanel18Layout.rowHeights = new int[] {0};
        jPanel18.setLayout(jPanel18Layout);

        java.awt.GridBagLayout jPanel20Layout = new java.awt.GridBagLayout();
        jPanel20Layout.columnWidths = new int[] {0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
        jPanel20Layout.rowHeights = new int[] {0, 3, 0, 3, 0};
        jPanel20.setLayout(jPanel20Layout);

        jLabel13.setText("Mean:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel20.add(jLabel13, gridBagConstraints);

        jLabel14.setText("RMS:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel20.add(jLabel14, gridBagConstraints);

        LabelRMSCh0.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh0.setText("0,0");
        LabelRMSCh0.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh0, gridBagConstraints);

        LabelRMSCh1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh1.setText("0,0");
        LabelRMSCh1.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh1, gridBagConstraints);

        LabelMeanCh0.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh0.setText("0,0");
        LabelMeanCh0.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh0, gridBagConstraints);

        LabelMeanCh1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh1.setText("0,0");
        LabelMeanCh1.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh1, gridBagConstraints);

        jLabel23.setText("Ch 0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel23, gridBagConstraints);

        jLabel24.setText("Ch 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel24, gridBagConstraints);

        jLabel25.setText("Ch 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel25, gridBagConstraints);

        LabelMeanCh2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh2.setText("0,0");
        LabelMeanCh2.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh2, gridBagConstraints);

        LabelRMSCh2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh2.setText("0,0");
        LabelRMSCh2.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh2, gridBagConstraints);

        jLabel20.setText("Ch 3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel20, gridBagConstraints);

        jLabel21.setText("Ch 4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel21, gridBagConstraints);

        jLabel22.setText("Ch 5");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel22, gridBagConstraints);

        LabelMeanCh3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh3.setText("0,0");
        LabelMeanCh3.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh3, gridBagConstraints);

        LabelMeanCh4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh4.setText("0,0");
        LabelMeanCh4.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh4, gridBagConstraints);

        LabelMeanCh5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh5.setText("0,0");
        LabelMeanCh5.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh5, gridBagConstraints);

        jLabel29.setText("Ch 6");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel29, gridBagConstraints);

        jLabel30.setText("Ch 7");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel30, gridBagConstraints);

        jLabel31.setText("Ch 8");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel31, gridBagConstraints);

        jLabel32.setText("Ch 9");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel32, gridBagConstraints);

        jLabel33.setText("Ch 10");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel33, gridBagConstraints);

        LabelRMSCh3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh3.setText("0,0");
        LabelRMSCh3.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh3, gridBagConstraints);

        LabelRMSCh4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh4.setText("0,0");
        LabelRMSCh4.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh4, gridBagConstraints);

        LabelRMSCh5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh5.setText("0,0");
        LabelRMSCh5.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh5, gridBagConstraints);

        LabelRMSCh6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh6.setText("0,0");
        LabelRMSCh6.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh6, gridBagConstraints);

        LabelMeanCh6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh6.setText("0,0");
        LabelMeanCh6.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh6, gridBagConstraints);

        LabelMeanCh7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh7.setText("0,0");
        LabelMeanCh7.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh7, gridBagConstraints);

        LabelMeanCh8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh8.setText("0,0");
        LabelMeanCh8.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh8, gridBagConstraints);

        LabelMeanCh9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh9.setText("0,0");
        LabelMeanCh9.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh9, gridBagConstraints);

        LabelMeanCh10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh10.setText("0,0");
        LabelMeanCh10.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh10, gridBagConstraints);

        LabelRMSCh7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh7.setText("0,0");
        LabelRMSCh7.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh7, gridBagConstraints);

        LabelRMSCh8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh8.setText("0,0");
        LabelRMSCh8.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 20;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh8, gridBagConstraints);

        LabelRMSCh9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh9.setText("0,0");
        LabelRMSCh9.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 22;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh9, gridBagConstraints);

        LabelRMSCh10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh10.setText("0,0");
        LabelRMSCh10.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 24;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh10, gridBagConstraints);

        jLabel26.setText("Ch 11");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 0;
        jPanel20.add(jLabel26, gridBagConstraints);

        LabelMeanCh11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMeanCh11.setText("0,0");
        LabelMeanCh11.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 2;
        jPanel20.add(LabelMeanCh11, gridBagConstraints);

        LabelRMSCh11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelRMSCh11.setText("0,0");
        LabelRMSCh11.setPreferredSize(new java.awt.Dimension(50, 16));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 26;
        gridBagConstraints.gridy = 4;
        jPanel20.add(LabelRMSCh11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel18.add(jPanel20, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        jPanel1.add(jPanel18, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }//GEN-END:initComponents

    /**
     * Starts run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStartPedestalRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStartPedestalRunActionPerformed
        // If no IPbus connection. Do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            Console.print_cr("STPedestalPanel -> no IPbus PPr connection");
            return;
        }

        // Run in progress: do nothing...
        if ((PedestalRunStatusFlag.get() == 1)
                || (STADCLinearityPanel.ADCLinearityRunStatus.get() == 1)
                || (STCISLinearityPanel.CISLinearityRunStatus.get() == 1)
                || (STCISLinearityPanel.FindBCRunStatus.get() == 1)
                || (STCISShapePanel.CISShapeRunStatus.get() == 1)
                || (STCISShapePanel.FindBCRunStatus.get() == 1)) {
            return;
        }

        // Updates the run status flag.
        PedestalRunStatusFlag.set(1);

        // Executes the given task.
        try {
            executor.execute(RunnableStartRun);
        } catch (RejectedExecutionException e) {
            System.out.println("STPedestalPanel -> RejectedExecutionException: " + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("STPedestalPanel -> NullPointerException: " + e.getMessage());
        }
    }//GEN-LAST:event_ButtonStartPedestalRunActionPerformed

    /**
     * Stops run.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonStopPedestalRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonStopPedestalRunActionPerformed
        // If no IPbus connection do nothing.
        if (!IPbusPanel.getConnectFlag()) {
            return;
        }

        // If run in progress, terminates it by setting the corresponding flag.
        // Otherwise do nothing.
        if (PedestalRunStatusFlag.get() == 1) {
            PedestalRunStatusFlag.set(0);

            // Updates label for pedestal run run status.
            LabelRunStatus.setText("terminating...");
            LabelRunStatus.setForeground(Color.blue);

            // Updates MainFrame label for pedestal run run status.
            MainFrame.LabelPedestalRunMainFrame.setText("not running");
            MainFrame.LabelPedestalRunMainFrame.setForeground(Color.RED);
        }
    }//GEN-LAST:event_ButtonStopPedestalRunActionPerformed

    /**
     * Sets LG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetLGPlotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetLGPlotActionPerformed
        selectedGainPlot = "LG";
        ButtonSetLGPlot.setBackground(Color.GRAY);
        ButtonSetHGPlot.setBackground(null);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetLGPlotActionPerformed

    /**
     * Sets HG plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetHGPlotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetHGPlotActionPerformed
        selectedGainPlot = "HG";
        ButtonSetLGPlot.setBackground(null);
        ButtonSetHGPlot.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetHGPlotActionPerformed

    /**
     * Resets plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonResetPlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonResetPlotsActionPerformed
        try {
            executor.execute(RunnableResetHistos);
        } catch (RejectedExecutionException e) {
            Console.print_cr("ButtonResetPlotsActionPerformed() -> RejectedExecutionException = "
                    + e.getMessage());
        } catch (NullPointerException e) {
            Console.print_cr("ButtonResetPlotsActionPerformed() -> NullPointerException = "
                    + e.getMessage());
        }
    }//GEN-LAST:event_ButtonResetPlotsActionPerformed

    /**
     * Sets MD1 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD1PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD1PlotsActionPerformed
        if (MDsel.get(0) == 0) {
            return;
        }
        selectedMDPlot = 0;
        ButtonSetMD1Plots.setBackground(Color.GRAY);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetMD1PlotsActionPerformed

    /**
     * Sets MD2 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD2PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD2PlotsActionPerformed
        if (MDsel.get(1) == 0) {
            return;
        }
        selectedMDPlot = 1;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(Color.GRAY);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetMD2PlotsActionPerformed

    /**
     * Sets MD3 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD3PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD3PlotsActionPerformed
        if (MDsel.get(2) == 0) {
            return;
        }
        selectedMDPlot = 2;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(Color.GRAY);
        ButtonSetMD4Plots.setBackground(null);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetMD3PlotsActionPerformed

    /**
     * Sets MD4 plots.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetMD4PlotsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetMD4PlotsActionPerformed
        if (MDsel.get(3) == 0) {
            return;
        }
        selectedMDPlot = 3;
        ButtonSetMD1Plots.setBackground(null);
        ButtonSetMD2Plots.setBackground(null);
        ButtonSetMD3Plots.setBackground(null);
        ButtonSetMD4Plots.setBackground(Color.GRAY);
        plot_RefreshPanelPlots();
        if (PedestalRunStatusFlag.get() == 0) {
            update_Mean_and_RMS();
        }
    }//GEN-LAST:event_ButtonSetMD4PlotsActionPerformed

    /**
     * Sets minidrawers to process data in broadcast mode.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxBroadcastMDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxBroadcastMDActionPerformed
        if (CheckBoxBroadcastMD.isSelected()) {
            MDbroadcast = 1;
        } else {
            MDbroadcast = 0;
        }
    }//GEN-LAST:event_CheckBoxBroadcastMDActionPerformed

    /**
     * Sets MD1 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD1ActionPerformed
        MDsel.set(0, CheckBoxMD1.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD1ActionPerformed

    /**
     * Sets MD2 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD2ActionPerformed
        MDsel.set(1, CheckBoxMD2.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD2ActionPerformed

    /**
     * Sets MD3 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD3ActionPerformed
        MDsel.set(2, CheckBoxMD3.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD3ActionPerformed

    /**
     * Sets MD4 to process data.
     *
     * @param evt Input ActionEvent object.
     */
    private void CheckBoxMD4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxMD4ActionPerformed
        MDsel.set(3, CheckBoxMD4.isSelected() ? 1 : 0);
    }//GEN-LAST:event_CheckBoxMD4ActionPerformed

    /**
     * Sets pedestal ADC counts from pedestal DAC bias offsets.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetPedADCtoDACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetPedADCtoDACActionPerformed
        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        setPed = "ADC";

        ButtonSetPedDACtoADC.setBackground(null);
        ButtonSetPedADCtoDAC.setBackground(Color.GRAY);

        get_ped_ADC();
        Console.print_cr("Pedestal ADC counts: " + ADCped);

        // Converts pedestal ADC counts to pedestal DAC bias offsets.
        VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
        DACbiasP = VinDACs.get(0);
        DACbiasN = VinDACs.get(1);

        Console.print_cr("Pedestal DAC bias positive offset: " + DACbiasP);
        Console.print_cr("Pedestal DAC bias negative offset: " + DACbiasN);

        TextFieldDACbiasP.setText(Integer.toString(DACbiasP));
        TextFieldDACbiasN.setText(Integer.toString(DACbiasN));
    }//GEN-LAST:event_ButtonSetPedADCtoDACActionPerformed

    /**
     * Sets pedestal DAC bias offsets from pedestal ADC counts.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSetPedDACtoADCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSetPedDACtoADCActionPerformed
        setPed = "DAC";

        ButtonSetPedDACtoADC.setBackground(Color.GRAY);
        ButtonSetPedADCtoDAC.setBackground(null);

        get_ped_DAC();
        Console.print_cr("Pedestal DAC bias positive offset: " + DACbiasP);
        Console.print_cr("Pedestal DAC bias negative offset: " + DACbiasN);

        // Converts pedestal DAC bias offsets to pedestal ADC counts.
        ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
        Console.print_cr("Pedestal ADC counts: " + ADCped);
        TextFieldADCped.setText(Integer.toString(ADCped));
    }//GEN-LAST:event_ButtonSetPedDACtoADCActionPerformed

    /**
     * First gets all field values from text widgets. Then sets field values in
     * properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSaveAsDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSaveAsDefaultsActionPerformed
        Console.print_cr("STPedestalPanel -> saving field values in properties file: " + Defaults.propsFile);
        get_all_parameters();
        Defaults.savePedestalRunProperties();
    }//GEN-LAST:event_ButtonSaveAsDefaultsActionPerformed

    /**
     * Loads default pedestal run parameter values from properties configuration
     * file into field values. Then fills all text widgets with field values.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonLoadDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonLoadDefaultsActionPerformed
        Console.print_cr("STPedestalPanel -> loading default values from properties file: " + Defaults.propsFile);
        Defaults.loadPedestalProperties();
        set_all_parameters();
    }//GEN-LAST:event_ButtonLoadDefaultsActionPerformed

    /**
     * Shows default values from properties configuration file.
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonShowDefaultsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonShowDefaultsActionPerformed
        Defaults.showProperties();
    }//GEN-LAST:event_ButtonShowDefaultsActionPerformed

    /**
     *
     *
     * @param evt Input ActionEvent object.
     */
    private void ButtonSmartBinningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonSmartBinningActionPerformed
        if (smartBinning == false) {
            smartBinning = true;
            ButtonSmartBinning.setBackground(Color.GRAY);
        } else {
            smartBinning = false;
            ButtonSmartBinning.setBackground(null);
        }
    }//GEN-LAST:event_ButtonSmartBinningActionPerformed

    /**
     * Define Runnables through lambda expressions.
     */
    private void initDefineRunnables() {
        RunnableStartRun = () -> {
            run_start_pedestal();
        };

        RunnableResetHistos = () -> {
            plot_ResetHistograms();
        };
    }

    /**
     * Configures and starts pedestal run.
     */
    private void run_start_pedestal() {
        Boolean ret;
        String fs;
        Integer LastEvtBCID, LastEvtL1ID;
        String strmean = "", strRMS = "";

        ArrayList<Integer> VinDACs = new ArrayList<>();
        VinDACs.clear();

        Console.print_cr("==> Pedestal run preparing... ");

        LabelSmartBinning.setText("");

        for (Integer md = 0; md < 4; md++) {
            for (Integer adc = 0; adc < 12; adc++) {
                dataMeanHG[md][adc] = 0.0;
                dataMeanLG[md][adc] = 0.0;
                dataRMSHG[md][adc] = 0.0;
                dataRMSLG[md][adc] = 0.0;
            }
        }

        // Unset visible run configuration settings.
        set_RunConfVisible(false);

        LabelRunStatus.setText("preparing...");
        LabelRunStatus.setForeground(Color.blue);

        // Change label run status in MainFrame.
        MainFrame.LabelPedestalRunMainFrame.setText("running");
        MainFrame.LabelPedestalRunMainFrame.setForeground(Color.GREEN.darker());

        // Initializes fields for proccessed events and entries.
        ProcEvents.set(0);
        ProcEntries.set(0);

        // Updates counters labels.
        LabelEventsCounter.setText(Integer.toString(ProcEvents.get()));
        LabelEntriesCounter.setText(Integer.toString(ProcEntries.get()));

        // Redefines histogram properties from JTextFields.
        plot_ResetHistograms();

        Console.print_cr("Getting pedestal run parameters... ");

        // Gets selected minidrawers.
        // --------------------------
        get_MDs();
        fs = "  Broadcast MD: %s";
        Console.print_cr(String.format(fs, ((MDbroadcast == 1) ? "True" : "False")));
        if (MDbroadcast == 0) {
            fs = "  Total minidrawers: %d. Active: %d %d %d %d";
            Console.print_cr(String.format(fs,
                    MDtot,
                    MDsel.get(0),
                    MDsel.get(1),
                    MDsel.get(2),
                    MDsel.get(3)));
        }

        if (MDtot == 0) {
            fs = "  No minidrawers selected.";
            Console.print_cr(String.format(fs));
            run_terminate_pedestal();
            return;
        }

        // Gets selected number of events.
        // -------------------------------
        get_TotEvents();
        fs = "  Number of events: %d";
        Console.print_cr(String.format(fs, totEvents));

        // Gets selected DB FPGA side.
        // --------------------------
        get_DB_FPGA_side();
        fs = "  DB FPGA Side: %d (%s)";
        Console.print_cr(String.format(fs, dbside, dbsidestring));

        // Gets number of samples to readout from each data pipeline.
        // ----------------------------------------------------------
        get_samples();
        Console.print_cr("  Samples: " + samples);

        // Gets BCID to read pipeline data from.
        // -------------------------------------
        get_BCID();
        Console.print_cr("  BCID: " + BCID);

        // Sets internal TTC.
        // ------------------
        Console.print_nr("Setting TTC internal... ");
        ret = pprlib.ppr.set_global_TTC_internal();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Resets CRC counters.
        // --------------------
        Console.print_nr("Resetting CRC counters... ");
        ret = pprlib.ppr.reset_CRC_counters();
        fs = "%s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Enables deadtime bit in global trigger configuration register
        // -------------------------------------------------------------
        Console.print_cr("Setting PPr enable deadtime bit in Global Trigger Conf... ");
        ret = pprlib.ppr.set_global_trigger_deadtime(0);
        fs = "  set bit to 0: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));
        ret = pprlib.ppr.set_global_trigger_deadtime(1);
        fs = "  set bit to 1: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Fail")));

        // Configures FEBs pedestal settings.
        // ----------------------------------
        Console.print_cr("Setting FEB ADC bias offsets... ");
        if ("DAC".equals(setPed)) {
            get_ped_DAC();
            ADCped = pprlib.feb.convert_ped_DACs_to_ADC(DACbiasP, DACbiasN);
            TextFieldADCped.setText(Integer.toString(ADCped));
            fs = "  DAC Vp: %d. DAC Vn: %d. ADC counts: %d";
            Console.print_cr(String.format(fs, DACbiasP, DACbiasN, ADCped));
        } else if ("ADC".equals(setPed)) {
            get_ped_ADC();
            VinDACs = pprlib.feb.convert_ped_ADC_to_DACs(ADCped);
            DACbiasP = VinDACs.get(0);
            DACbiasN = VinDACs.get(1);
            TextFieldDACbiasP.setText(Integer.toString(DACbiasP));
            TextFieldDACbiasN.setText(Integer.toString(DACbiasN));
            fs = "  ADC counts: %d. DAC Vp: %d. DAC Vn: %d";
            Console.print_cr(String.format(fs, ADCped, DACbiasP, DACbiasN));
        }
        ret = set_FEB_ADC_bias_offsets_DACs();
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configures FEBs to load ADC DACs.
        // ---------------------------------
        Console.print_cr("Loading FEB ADC DACs... ");
        ret = set_FEB_load_ADC_DACs();
        fs = "  Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        // Configures FEB switches.
        // ------------------------
        Console.print_cr("Setting FEB switches...");
        ret = set_FEB_switches();
        fs = "   Result: %s";
        Console.print_cr(String.format(fs, (ret ? "Ok" : "Failed")));

        Console.print_cr("==> Pedestal run starting... ");
        LabelRunStatus.setText("running...");
        LabelRunStatus.setForeground(Color.blue);

        smartBinningEntries = 0;

        // Loop while:
        // 1) Run is not stopped.
        // 2) IPbus connection has not dropped.
        // 3) Processed events not reached the selected value.
        while (true) {
            if (!IPbusPanel.getConnectFlag()
                    || PedestalRunStatusFlag.get() == 0
                    || ProcEvents.get() >= totEvents) {
                break;
            }

            // Reads last Event BCID (disables busy to read pipelines).
            LastEvtBCID = pprlib.ppr.get_counter_last_event_BCID();
            LabelLastEvtBCIDCounter.setText(Integer.toString(LastEvtBCID));

            // Reads last Event L1ID (disables busy to read pipelines).
            LastEvtL1ID = pprlib.ppr.get_counter_last_event_L1ID();
            LabelLastEvtL1IDCounter.setText(Integer.toString(LastEvtL1ID));

            // Generates trigger to be sent to all MDs.
            ret = pprlib.feb.send_L1A(BCID, 3);
            if (DebugSettings.getVerbose()) {
                fs = "  Event: %d  sending L1A trigger... %s";
                Console.print_cr(String.format(fs, ProcEvents.get() + 1, (ret ? "Ok" : "Fail")));
            }

            // Fills data array with data pipelines with selected number of samples.
            // Draw histograms. Only if data plots are not being reset.
            if (fillHistosPossible.get() == 0) {

                // Loop over selected minidrawers.
                for (int md = 0; md < 4; md++) {
                    if (MDsel.get(md) == 1) {

                        // Loop over ADCs.
                        for (int adc = 0; adc < 12; adc++) {
                            dataHG[md][adc].getData().clear();
                            dataLG[md][adc].getData().clear();

                            dataHG[md][adc].setData(pprlib.ppr.get_data_HG(md, adc, samples));
                            dataLG[md][adc].setData(pprlib.ppr.get_data_LG(md, adc, samples));

                            // Loop over HG samples to fill histograms.
                            for (int samp = 0; samp < samples; samp++) {
                                h_plot_HG[md][adc].fill(dataHG[md][adc].getData().get(samp));

                                if (DebugSettings.getVerbose()) {
                                    if (samp == 0) {
                                        fs = "    MD: %d. FEB %d. Data HG: ";
                                        Console.print_nr(String.format(fs,
                                                md + 1,
                                                adc));
                                    }
                                    fs = "%d ";
                                    Console.print_nr(String.format(fs,
                                            dataHG[md][adc].getData().get(samp)));
                                }

                                h_plot_HG[md][adc].draw();
                            }

                            if (DebugSettings.getVerbose()) {
                                Console.print_cr("");
                            }

                            // Loop over LG samples to fill histograms.
                            for (int samp = 0; samp < samples; samp++) {
                                h_plot_LG[md][adc].fill(dataLG[md][adc].getData().get(samp));

                                if (DebugSettings.getVerbose()) {
                                    if (samp == 0) {
                                        fs = "    MD: %d. FEB %d. Data LG: ";
                                        Console.print_nr(String.format(fs,
                                                md + 1,
                                                adc));
                                    }
                                    fs = "%d ";
                                    Console.print_nr(String.format(fs,
                                            dataLG[md][adc].getData().get(samp)));
                                }

                                h_plot_LG[md][adc].draw();
                            }

                            if (DebugSettings.getVerbose()) {
                                Console.print_cr("");
                            }

                            // Compute mean and RMS for eachADC channel.
                            dataMeanHG[md][adc] = h_plot_HG[md][adc].getMean();
                            dataRMSHG[md][adc] = h_plot_HG[md][adc].getRMS();
                            dataMeanLG[md][adc] = h_plot_LG[md][adc].getMean();
                            dataRMSLG[md][adc] = h_plot_LG[md][adc].getRMS();

                            if (DebugSettings.getVerbose()) {
                                fs = "    MD: %d. FEB %d. Mean HG: %.1f. RMS HG: %.1f";
                                Console.print_cr(String.format(fs,
                                        md + 1,
                                        adc,
                                        dataMeanHG[md][adc],
                                        dataRMSHG[md][adc]));

                                fs = "    MD: %d. FEB %d. Mean LG: %.1f. RMS LG: %.1f";
                                Console.print_cr(String.format(fs,
                                        md + 1,
                                        adc,
                                        dataMeanLG[md][adc],
                                        dataRMSLG[md][adc]));
                            }

                            if (md == selectedMDPlot) {
                                if ("HG".equals(selectedGainPlot)) {
                                    strmean = String.format("%.1f", dataMeanHG[selectedMDPlot][adc]);
                                    strRMS = String.format("%.1f", dataRMSHG[selectedMDPlot][adc]);
                                } else if ("LG".equals(selectedGainPlot)) {
                                    strmean = String.format("%.1f", dataMeanLG[selectedMDPlot][adc]);
                                    strRMS = String.format("%.1f", dataRMSLG[selectedMDPlot][adc]);
                                }

                                LabelMean[adc].setText(strmean);
                                LabelRMS[adc].setText(strRMS);
                            }
                        }
                    }
                }

                // Change binning for each plot if automatic binning is selected.
                if (smartBinning) {

                    LabelSmartBinning.setText("calculating...");
                    LabelSmartBinning.setForeground(Color.blue);

                    smartBinningEntries += samples;

                    if (smartBinningEntries >= 50) {

                        if (DebugSettings.getVerbose()) {
                            fs = "    Smart Binning -> entries: %d";
                            Console.print_cr(String.format(fs, smartBinningEntries));
                        }

                        // Loop over selected minidrawers.
                        for (int md = 0; md < 4; md++) {
                            if (MDsel.get(md) == 1) {
                                // Loop over ADCs.
                                for (int adc = 0; adc < 12; adc++) {

                                    if (DebugSettings.getVerbose()) {
                                        fs = "    Smart Binning: MD: %d. FEB %d. Mean HG: %.1f. Mean LG: %.1f";
                                        Console.print_cr(String.format(fs,
                                                md + 1,
                                                adc,
                                                dataMeanHG[md][adc],
                                                dataMeanLG[md][adc]));
                                    }

                                    plot_ResetSingleHistogram(
                                            md,
                                            adc,
                                            "HG",
                                            40,
                                            Math.round(dataMeanHG[md][adc].floatValue()) - 20,
                                            Math.round(dataMeanHG[md][adc].floatValue()) + 20);

                                    plot_ResetSingleHistogram(
                                            md,
                                            adc,
                                            "LG",
                                            20,
                                            Math.round(dataMeanLG[md][adc].floatValue()) - 10,
                                            Math.round(dataMeanLG[md][adc].floatValue()) + 10);
                                }
                            }
                        }

                        smartBinningEntries = 0;
                        smartBinning = false;
                        ButtonSmartBinning.setBackground(null);
                        LabelSmartBinning.setText("done...");
                        LabelSmartBinning.setForeground(Color.blue);
                    }
                }
            }

            ProcEvents.addAndGet(1);
            LabelEventsCounter.setText(Integer.toString(ProcEvents.get()));

            ProcEntries.addAndGet(samples);
            LabelEntriesCounter.setText(Integer.toString(ProcEntries.get()));
        }

        // Terminates run.
        run_terminate_pedestal();
    }

    /**
     * Terminates run.
     */
    private synchronized void run_terminate_pedestal() {
        PedestalRunStatusFlag.set(0);

        // Update label for pedestal run status.
        LabelRunStatus.setText("stopped");
        LabelRunStatus.setForeground(Color.RED);

        // Update MainFrame label for pedestal run run status.
        MainFrame.LabelPedestalRunMainFrame.setText("not running");
        MainFrame.LabelPedestalRunMainFrame.setForeground(Color.RED);

        Console.print_cr("==> Pedestal run terminated");

        // Set visible run configuration settings.
        set_RunConfVisible(true);
    }

    /**
     *
     */
    private synchronized void update_Mean_and_RMS() {
        String strmean = "", strRMS = "";

        for (Integer adc = 0; adc < 12; adc++) {
            if ("HG".equals(selectedGainPlot)) {
                strmean = String.format("%.1f", dataMeanHG[selectedMDPlot][adc]);
                strRMS = String.format("%.1f", dataRMSHG[selectedMDPlot][adc]);
            } else if ("LG".equals(selectedGainPlot)) {
                strmean = String.format("%.1f", dataMeanLG[selectedMDPlot][adc]);
                strRMS = String.format("%.1f", dataRMSLG[selectedMDPlot][adc]);
            }

            LabelMean[adc].setText(strmean);
            LabelRMS[adc].setText(strRMS);
        }
    }

    /**
     *
     */
    public static void plot_BookHistograms() {
        String mdstr;

        //Declares 2d-array (4 minidrawers, 12 FEBs) of histogram JH1 with HG/LG.
        h_plot_HG = new JH1[4][12];
        h_plot_LG = new JH1[4][12];

        // Loop on minidrawers.
        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);

            // Loop on ADC channels.
            for (int adc = 0; adc < 12; adc++) {
                htitle = String.format("%s Ch %1d (HG);ADC counts;", mdstr, adc);
                h_plot_HG[md][adc] = new JH1(htitle, hnbins, hxlow, hxhigh);

                htitle = String.format("%s Ch %1d (LG);ADC counts;", mdstr, adc);
                h_plot_LG[md][adc] = new JH1(htitle, hnbins, hxlow, hxhigh);
            }
        }

        // The JPanel where to place the histogram has to be added the property
        // of Set Layout to Border Layout by right-clicking on the Panel and 
        // select Set Layout.
        for (int adc = 0; adc < 12; adc++) {
            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }
        }

        // Validates this container and all of its subcomponents. 
        // Inherited from Java Container class.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].validate();
        }
    }

    /**
     * Resets histogram settings.
     */
    private void plot_ResetHistograms() {
        String mdstr;

        // Semaphore closed to prevent filling 
        // histograms while DAQ running.
        fillHistosPossible.set(1);

        // Removes all present JH1 histograms from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int md = 0; md < 4; md++) {
            for (int adc = 0; adc < 12; adc++) {
                h_plot_HG[md][adc].reset();
                h_plot_LG[md][adc].reset();
            }
        }

        // Gets histogram number of bins.
        get_bins();

        // Loop on mionidrawers.
        for (int md = 0; md < 4; md++) {
            mdstr = String.format("MD%1d", md + 1);

            // Loop on ADC channels.
            for (int adc = 0; adc < 12; adc++) {
                htitle = String.format("%s Ch %1d (HG);ADCs;", mdstr, adc);
                h_plot_HG[md][adc].setTitle(htitle);
                h_plot_HG[md][adc].setBins(hnbins, hxlow, hxhigh);
                h_plot_HG[md][adc].draw();

                htitle = String.format("%s Ch %1d (LG);ADCs;", mdstr, adc);
                h_plot_LG[md][adc].setTitle(htitle);
                h_plot_LG[md][adc].setBins(hnbins, hxlow, hxhigh);
                h_plot_LG[md][adc].draw();
            }
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            //PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }

        // Semaphore opened.
        fillHistosPossible.set(0);
    }

    /**
     * Reset individual histogram settings.
     */
    private void plot_ResetSingleHistogram(
            Integer md,
            Integer adc,
            String gain,
            Integer nb,
            Integer xlow,
            Integer xhigh) {
        String mdstr;

        // Semaphore closed to prevent filling 
        // histograms while DAQ running.
        fillHistosPossible.set(1);

        // Removes selected JH1 histogram from JPanel plot ArrayList.
        PanelPlots[adc].removeAll();

        h_plot_HG[md][adc].reset();
        h_plot_LG[md][adc].reset();

        mdstr = String.format("MD%1d", md + 1);

        if ("HG".equals(gain)) {
            htitle = String.format("%s Ch %1d (HG);ADCs;", mdstr, adc);
            h_plot_HG[md][adc].setTitle(htitle);
            h_plot_HG[md][adc].setBins(nb, xlow, xhigh);
            h_plot_HG[md][adc].draw();
        } else if ("LG".equals(gain)) {
            htitle = String.format("%s Ch %1d (LG);ADCs;", mdstr, adc);
            h_plot_LG[md][adc].setTitle(htitle);
            h_plot_LG[md][adc].setBins(nb, xlow, xhigh);
            h_plot_LG[md][adc].draw();
        }

        if ("HG".equals(selectedGainPlot)) {
            PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
        }

        if ("LG".equals(selectedGainPlot)) {
            PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
        }

        //PanelPlots[adc].revalidate();
        PanelPlots[adc].repaint();

        // Semaphore opened.
        fillHistosPossible.set(0);
    }

    /**
     * Refresh plots.
     */
    private void plot_RefreshPanelPlots() {
        // Semaphore closed to prevent filling 
        // histograms while DAQ running.
        fillHistosPossible.set(1);

        // Removes all present histogram JH1 components from JPanel plots ArrayList.
        for (int adc = 0; adc < 12; adc++) {
            PanelPlots[adc].removeAll();
        }

        for (int adc = 0; adc < 12; adc++) {
            if ("LG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_LG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            if ("HG".equals(selectedGainPlot)) {
                PanelPlots[adc].add(h_plot_HG[selectedMDPlot][adc], BorderLayout.CENTER);
            }

            PanelPlots[adc].revalidate();
            PanelPlots[adc].repaint();
        }

        // Semaphore opened.
        fillHistosPossible.set(0);
    }

    /**
     * Gets number of events to be proccess field from TextField.
     */
    private synchronized void get_TotEvents() {
        totEvents = Integer.valueOf(TextFieldEventsToProcess.getText(), 10);
        if (totEvents > Integer.MAX_VALUE) {
            totEvents = Integer.MAX_VALUE;
            TextFieldEventsToProcess.setText(String.valueOf(totEvents));
        }
        if (totEvents < 0) {
            totEvents = 0;
            TextFieldEventsToProcess.setText("0");
        }
    }

    /**
     * Sets number of events to be proccess.
     */
    public static synchronized void set_TotEvents() {
        TextFieldEventsToProcess.setText(String.valueOf(totEvents));
    }

    /**
     * Gets minidrawers to process and minidrawer broadcast mode.
     */
    private synchronized void get_MDs() {
        MDsel.clear();
        MDsel.add(CheckBoxMD1.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD2.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD3.isSelected() ? 1 : 0);
        MDsel.add(CheckBoxMD4.isSelected() ? 1 : 0);

        MDtot = 0;
        for (Integer md = 0; md < 4; md++) {
            if (MDsel.get(md) == 1) {
                MDtot++;
            }
        }

        MDbroadcast = (CheckBoxBroadcastMD.isSelected() ? 1 : 0);
    }

    /**
     * Sets minidrawers to process and minidrawer broadcast mode.
     */
    public static synchronized void set_MDs() {
        if (MDsel.get(0) == 0) {
            CheckBoxMD1.setSelected(false);
        } else {
            CheckBoxMD1.setSelected(true);
        }

        if (MDsel.get(1) == 0) {
            CheckBoxMD2.setSelected(false);
        } else {
            CheckBoxMD2.setSelected(true);
        }

        if (MDsel.get(2) == 0) {
            CheckBoxMD3.setSelected(false);
        } else {
            CheckBoxMD3.setSelected(true);
        }

        if (MDsel.get(3) == 0) {
            CheckBoxMD4.setSelected(false);
        } else {
            CheckBoxMD4.setSelected(true);
        }

        if (MDbroadcast == 0) {
            CheckBoxBroadcastMD.setSelected(false);
        } else {
            CheckBoxBroadcastMD.setSelected(true);
        }
    }

    /**
     * Gets DB FPGA side.
     */
    private synchronized void get_DB_FPGA_side() {
        String side_strings[] = new String[]{"Broadcast", "Side A", "Side B", "Unknown"};

        Object selected = ComboBoxDBSide.getSelectedItem();
        String DBSideStr = selected.toString();
        switch (DBSideStr) {
            case "Broadcast":
                dbside = 0;
                dbsidestring = side_strings[0];
                break;
            case "Side A":
                dbside = 1;
                dbsidestring = side_strings[1];
                break;
            case "Side B":
                dbside = 2;
                dbsidestring = side_strings[2];
                break;
            default:
                dbsidestring = side_strings[3];
                break;
        }
    }

    /**
     * Sets DB FPGA side.
     */
    public static synchronized void set_DB_FPGA_side() {
        switch (dbsidestring) {
            case "Broadcast":
                dbside = 0;
                break;
            case "Side A":
                dbside = 1;
                break;
            case "Side B":
                dbside = 2;
                break;
        }

        ComboBoxDBSide.setSelectedIndex(dbside);
    }

    /**
     * Gets samples.
     */
    private synchronized void get_samples() {
        Object selected = ComboBoxSamples.getSelectedItem();
        String strSamp = selected.toString();
        samples = Integer.valueOf(strSamp);
    }

    /**
     * Sets samples.
     */
    public static synchronized void set_samples() {
        ComboBoxSamples.setSelectedIndex(samples - 1);
    }

    /**
     * Gets BCID for L1A.
     */
    private synchronized void get_BCID() {
        BCID = Integer.valueOf(TextFieldBCID.getText(), 10);
        if (BCID > 3564) {
            BCID = 3564;
            TextFieldBCID.setText("3564");
        }
        if (BCID < 0) {
            BCID = 0;
            TextFieldBCID.setText("0");
        }
    }

    /**
     * Sets BCID for L1A.
     */
    public static synchronized void set_BCID() {
        TextFieldBCID.setText(String.valueOf(BCID));
    }

    /**
     * Gets ADC pedestal counts.
     */
    private synchronized void get_ped_ADC() {
        ADCped = Integer.valueOf(TextFieldADCped.getText(), 10);
        if (ADCped > 4095) {
            ADCped = 4095;
            TextFieldADCped.setText("4095");
        }
        if (ADCped < 0) {
            ADCped = 0;
            TextFieldADCped.setText("0");
        }
    }

    /**
     * Sets ADC pedestal counts.
     */
    public static synchronized void set_ped_ADC() {
        TextFieldADCped.setText(String.valueOf(ADCped));
    }

    /**
     * Set pedestal calculation mode.
     */
    public static synchronized void set_ped() {
        if ("DAC".equals(setPed)) {
            ButtonSetPedDACtoADC.setBackground(Color.GRAY);
            ButtonSetPedADCtoDAC.setBackground(null);
        } else if ("ADC".equals(setPed)) {
            ButtonSetPedDACtoADC.setBackground(null);
            ButtonSetPedADCtoDAC.setBackground(Color.GRAY);
        }
    }

    /**
     * Gets DAC pedestal positive and negative offsets.
     */
    private synchronized void get_ped_DAC() {
        DACbiasP = Integer.valueOf(TextFieldDACbiasP.getText(), 10);
        if (DACbiasP > 2261) {
            DACbiasP = 2261;
            TextFieldDACbiasP.setText("2261");
        }
        if (DACbiasP < 1278) {
            DACbiasP = 1278;
            TextFieldDACbiasP.setText("1278");
        }

        DACbiasN = Integer.valueOf(TextFieldDACbiasN.getText(), 10);
        if (DACbiasN > 2261) {
            DACbiasN = 2261;
            TextFieldDACbiasN.setText("2261");
        }
        if (DACbiasN < 1278) {
            DACbiasN = 1278;
            TextFieldDACbiasN.setText("1278");
        }
    }

    /**
     * Sets DAC pedestal positive and negative offsets.
     */
    public static synchronized void set_ped_DAC() {
        TextFieldDACbiasP.setText(String.valueOf(DACbiasP));
        TextFieldDACbiasN.setText(String.valueOf(DACbiasN));
    }

    /**
     * Gets number of bins for plots.
     */
    private synchronized void get_bins() {
        hnbins = Integer.valueOf(TextFieldBins.getText(), 10);
        if (hnbins < 0) {
            hnbins = 100;
            TextFieldBins.setText(Integer.toString(hnbins));
        }

        hxhigh = Integer.valueOf(TextFieldMaxBin.getText(), 10);
        if (hxhigh < 0) {
            hxhigh = 0;
            TextFieldMaxBin.setText("1");
        }

        hxlow = Integer.valueOf(TextFieldMinBin.getText(), 10);
        if (hxlow < 0) {
            hxlow = 0;
            TextFieldMinBin.setText("1");
        }

        if (hxlow >= hxhigh) {
            hxlow = 200;
            hxhigh = 300;
            set_bins();
        }
    }

    /**
     * Sets number of bins for plots.
     */
    public static synchronized void set_bins() {
        TextFieldBins.setText(String.valueOf(hnbins));
        TextFieldMinBin.setText(String.valueOf(hxlow));
        TextFieldMaxBin.setText(String.valueOf(hxhigh));
    }

    /**
     * Set automatic binning calculation mode.
     */
    public static synchronized void set_smart_binning() {
        if (true==smartBinning) {
            ButtonSmartBinning.setBackground(Color.GRAY);
        } else  {
            ButtonSmartBinning.setBackground(null);
        }
    }
    
    /**
     * Sets pedestal settings for selected FEBs (HG, LG, positive, negative).
     *
     * @return IPbus return status.
     */
    private synchronized Boolean set_FEB_ADC_bias_offsets_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_ped_HG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_pos(md, dbside, adc, DACbiasP);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGpos: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasP, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_HG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped HGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.set_ped_LG_neg(md, dbside, adc, DACbiasN);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set ped LGneg: %d result: %s";
                        Console.print_cr(String.format(fs, md, adc, DACbiasN, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Loads ADC DACS (HG and LG) for selected FEBs.
     *
     * @return IPbus return status.
     */
    private synchronized Boolean set_FEB_load_ADC_DACs() {
        Boolean ret = true;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {

                    ret = pprlib.feb.load_ped_HG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load HG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }

                    ret = pprlib.feb.load_ped_LG(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d load LG result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Sets switches for noise for selected FEBs.
     *
     * @return IPbus return status.
     */
    private synchronized Boolean set_FEB_switches() {
        Boolean ret = false;
        String fs;
        int MDmin, MDmax;

        if (MDbroadcast == 1) {
            MDmin = 0;
            MDmax = 1;
        } else {
            MDmin = 1;
            MDmax = 5;
        }

        int mdidx = 0;
        for (Integer md = MDmin; md < MDmax; md++) {
            if ((MDbroadcast == 1) || ((MDbroadcast == 0) && (MDsel.get(mdidx) == 1))) {
                for (Integer adc = 0; adc < 12; adc++) {
                    ret = pprlib.feb.set_switches_noise(md, dbside, adc);
                    if (DebugSettings.getVerbose()) {
                        fs = "  MD: %d FEB: %2d set switches result: %s";
                        Console.print_cr(String.format(fs, md, adc, (ret ? "Ok" : "Fail")));
                    }
                }
            }
            mdidx++;
        }

        return ret;
    }

    /**
     * Enables/disbles DAQ configuration settings objects according with input
     * flag.
     *
     * @param flag Input Boolean flag.
     */
    private synchronized void set_RunConfVisible(Boolean flag) {

        // Active minidrawer JCheckBoxes.
        CheckBoxMD1.setEnabled(flag);
        CheckBoxMD2.setEnabled(flag);
        CheckBoxMD3.setEnabled(flag);
        CheckBoxMD4.setEnabled(flag);

        // Broadcast minidrawer JCheckBox.
        CheckBoxBroadcastMD.setEnabled(flag);

        // Broadcast DaughterBoard JComboBox.
        ComboBoxDBSide.setEnabled(flag);
    }

    /**
     * Declares a PPr class object to communicate with PPr IPbus server.
     */
    private PPrLib pprlib;

    /**
     * Declares an ExecutorService interface (extends Executor interface), an
     * asynchronous execution mechanism which is capable of executing tasks
     * concurrently in the background. ExecutorService is created and return
     * from methods of the Executors Class.
     */
    public static ThreadPoolExecutor executor;

    /**
     * Declares Runnable for run_start_pedestal() method.
     */
    private Runnable RunnableStartRun;

    private Runnable RunnableResetHistos;

    /**
     * Flag to define the pedestal run status. Atomic Java variables are
     * lock-free thread-safe programming variables whose values may be updated
     * atomically. They allow concurrent accesses (used by many threads
     * concurrently).
     */
    public static AtomicInteger PedestalRunStatusFlag;

    /**
     * Selected total events to be proccessed.
     */
    public static Integer totEvents;

    /**
     * Selected BCID for L1A.
     */
    public static Integer BCID;

    /**
     * Selected samples.
     */
    public static Integer samples;

    /**
     * Selected DAC bias positive pedestal.
     */
    public static Integer DACbiasP;

    /**
     * Selected DAC bias negative pedestal.
     */
    public static Integer DACbiasN;

    /**
     * Selected ADC pedestal counts.
     */
    public static Integer ADCped;

    /**
     * Selected way to set pedestal (DACs or ADC counts)
     */
    public static String setPed;

    /**
     * Selected Daughterboard FPGA side.
     */
    public static Integer dbside;

    /**
     * Selected Daughterboard FPGA side (as String).
     */
    public static String dbsidestring;

    /**
     * Selected minidrawers broadcast flag.
     */
    public static Integer MDbroadcast;

    /**
     * Selected ArrayList with minidrawers to be filled with data.
     */
    public static ArrayList<Integer> MDsel;

    public static Boolean smartBinning;
    private Integer smartBinningEntries;

    /**
     * Selected number of bins for plots.
     */
    public static Integer hnbins;

    /**
     * Selected max X-scale for plots.
     */
    public static Integer hxlow;

    /**
     * Selected min X-scale for plots.
     */
    public static Integer hxhigh;

    /**
     * Total selected MDs.
     */
    private Integer MDtot;

    /**
     * Declares AtomicInteger counter for proccessed events.
     */
    AtomicInteger ProcEvents;

    /**
     * Declares AtomicInteger counter for proccessed entries (Events x Samples).
     */
    AtomicInteger ProcEntries;

    /**
     * Declares array of JPanel JH1 objects with histograms.
     */
    private static javax.swing.JPanel[] PanelPlots;

    /**
     * Declares object with HG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataHG;

    /**
     * Declares object with LG data 2d-array of Integer ArrayList with samples.
     */
    private DataArrayList[][] dataLG;

    /**
     * Selected gain for plots.
     */
    public static String selectedGainPlot;

    /**
     * Selected minidrawer for plots.
     */
    public static Integer selectedMDPlot;

    private static String htitle;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of histogram JH1 with HG (high
     * gain).
     */
    private static JH1[][] h_plot_HG;

    /**
     * Declares 2d-array (4 minidrawers, 12 FEBs) of histogram JH1 with LG (low
     * gain).
     */
    private static JH1[][] h_plot_LG;

    // Semaphore to prevent filling of histograms while resetting them.
    public static AtomicInteger fillHistosPossible;

    private Double[][] dataMeanLG;
    private Double[][] dataRMSLG;

    private Double[][] dataMeanHG;
    private Double[][] dataRMSHG;

    private static javax.swing.JLabel[] LabelMean;
    private static javax.swing.JLabel[] LabelRMS;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonLoadDefaults;
    private javax.swing.JButton ButtonResetPlots;
    private javax.swing.JButton ButtonSaveAsDefaults;
    private javax.swing.JButton ButtonSetHGPlot;
    private javax.swing.JButton ButtonSetLGPlot;
    private javax.swing.JButton ButtonSetMD1Plots;
    private javax.swing.JButton ButtonSetMD2Plots;
    private javax.swing.JButton ButtonSetMD3Plots;
    private javax.swing.JButton ButtonSetMD4Plots;
    public static javax.swing.JButton ButtonSetPedADCtoDAC;
    public static javax.swing.JButton ButtonSetPedDACtoADC;
    private javax.swing.JButton ButtonShowDefaults;
    public static javax.swing.JButton ButtonSmartBinning;
    private javax.swing.JButton ButtonStartPedestalRun;
    private javax.swing.JButton ButtonStopPedestalRun;
    public static javax.swing.JCheckBox CheckBoxBroadcastMD;
    public static javax.swing.JCheckBox CheckBoxMD1;
    public static javax.swing.JCheckBox CheckBoxMD2;
    public static javax.swing.JCheckBox CheckBoxMD3;
    public static javax.swing.JCheckBox CheckBoxMD4;
    public static javax.swing.JComboBox<String> ComboBoxDBSide;
    public static javax.swing.JComboBox<String> ComboBoxSamples;
    private javax.swing.JLabel LabelEntriesCounter;
    private javax.swing.JLabel LabelEventsCounter;
    private javax.swing.JLabel LabelLastEvtBCIDCounter;
    private javax.swing.JLabel LabelLastEvtL1IDCounter;
    private javax.swing.JLabel LabelMeanCh0;
    private javax.swing.JLabel LabelMeanCh1;
    private javax.swing.JLabel LabelMeanCh10;
    private javax.swing.JLabel LabelMeanCh11;
    private javax.swing.JLabel LabelMeanCh2;
    private javax.swing.JLabel LabelMeanCh3;
    private javax.swing.JLabel LabelMeanCh4;
    private javax.swing.JLabel LabelMeanCh5;
    private javax.swing.JLabel LabelMeanCh6;
    private javax.swing.JLabel LabelMeanCh7;
    private javax.swing.JLabel LabelMeanCh8;
    private javax.swing.JLabel LabelMeanCh9;
    private javax.swing.JLabel LabelRMSCh0;
    private javax.swing.JLabel LabelRMSCh1;
    private javax.swing.JLabel LabelRMSCh10;
    private javax.swing.JLabel LabelRMSCh11;
    private javax.swing.JLabel LabelRMSCh2;
    private javax.swing.JLabel LabelRMSCh3;
    private javax.swing.JLabel LabelRMSCh4;
    private javax.swing.JLabel LabelRMSCh5;
    private javax.swing.JLabel LabelRMSCh6;
    private javax.swing.JLabel LabelRMSCh7;
    private javax.swing.JLabel LabelRMSCh8;
    private javax.swing.JLabel LabelRMSCh9;
    private javax.swing.JLabel LabelRunStatus;
    private javax.swing.JLabel LabelSmartBinning;
    private javax.swing.JPanel PanelPlot1;
    private javax.swing.JPanel PanelPlot10;
    private javax.swing.JPanel PanelPlot11;
    private javax.swing.JPanel PanelPlot12;
    private javax.swing.JPanel PanelPlot2;
    private javax.swing.JPanel PanelPlot3;
    private javax.swing.JPanel PanelPlot4;
    private javax.swing.JPanel PanelPlot5;
    private javax.swing.JPanel PanelPlot6;
    private javax.swing.JPanel PanelPlot7;
    private javax.swing.JPanel PanelPlot8;
    private javax.swing.JPanel PanelPlot9;
    public static javax.swing.JTextField TextFieldADCped;
    public static javax.swing.JTextField TextFieldBCID;
    public static javax.swing.JTextField TextFieldBins;
    public static javax.swing.JTextField TextFieldDACbiasN;
    public static javax.swing.JTextField TextFieldDACbiasP;
    public static javax.swing.JTextField TextFieldEventsToProcess;
    public static javax.swing.JTextField TextFieldMaxBin;
    public static javax.swing.JTextField TextFieldMinBin;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    // End of variables declaration//GEN-END:variables
}
