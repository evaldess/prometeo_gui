/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package utilities;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public final class Constants {

    private Constants() {
    }

    public static final double C = 299792458;
    public static final double PI = 3.14159;
    public static final double PLANCK_CONSTANT = 6.62606896e-34;
}
