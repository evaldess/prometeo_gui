/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.ArrayList;

/**
 * Provides a multidimensional ArrayList object of Integers.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class DataArrayList {

    private ArrayList<Integer> data;

    /**
     *
     */
    public DataArrayList() {
        this.data = new ArrayList<>();
    }

    /**
     * Getter
     *
     * @return
     */
    public ArrayList<Integer> getData() {
        return this.data;
    }

    /**
     * Setter
     *
     * @param data
     */
    public void setData(ArrayList<Integer> data) {
        this.data = data;
    }
}
