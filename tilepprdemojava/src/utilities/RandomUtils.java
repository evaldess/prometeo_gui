/* 
 *  TestBench Project
 *  Cosmic Hodoscope to Test Silicon Photodiode Detectors
 *  2019 (c) IFIC-CERN
 */
package utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility class to handle random distributed numbers numbers.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class RandomUtils {

    /**
     * Generates an ArrayList with random Integers in a certain range from an
     * Uniform distribution.
     *
     * @param start Input start value.
     * @param end Input end value.
     * @param nsamp Input samples.
     * @return the ArrayList with Integer random numbers.
     */
    public static ArrayList<Integer> genSamplesUniform(Integer start, Integer end, Integer nsamp) {

        // ArrayList with the samples to return
        ArrayList<Integer> samples = new ArrayList<>();

        // Define a RandomGen object
        Random random = new Random();

        // Loop over samples
        for (Integer i = 0; i < nsamp; i++) {
            samples.add(getRandomInteger(start, end, random));
        }
        return samples;
    }

    /**
     * Generates an ArrayList with random Integers from a normal (Gaussian)
     * distribution given its mean and variance.
     *
     * @param mean Input mean.
     * @param sigma Input variance.
     * @param nsamp Input samples.
     * @return the ArrayList of Integers with generated random numbers.
     */
    public static ArrayList<Integer> genSamplesGaussian(Integer mean, Integer sigma, Integer nsamp) {

        // Defines ArrayList with the samples to return.
        ArrayList<Integer> samples = new ArrayList<>();

        // Defines a RandomGen object.
        Random fRandom = new Random();

        double aMean = mean.doubleValue();
        double aSigma = sigma.doubleValue();

        // Loop over samples
        for (Integer i = 0; i < nsamp; i++) {
            double val = aMean + fRandom.nextGaussian() * aSigma;
            Long lval = Math.round(val);
            samples.add(lval.intValue());
        }
        return samples;
    }

    /**
     * Generates a single random Integer in a certain range.
     *
     * @param aStart Input start int.
     * @param aEnd Input end int.
     * @param aRandom Input Random object.
     * @return the random Integer.
     */
    public static Integer getRandomInteger(int aStart, int aEnd, Random aRandom) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("getRandomInteger -> Start cannot exceed End");
        }

        // Get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;

        // Compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        Integer randomNumber = (int) (fraction + aStart);
        return randomNumber;
    }

    public static Integer getRandomGaussian(Integer mean, Integer sigma, Random aRandom) {
        double aMean = mean.doubleValue();
        double aSigma = sigma.doubleValue();
        double val;
        
        val = aMean + aRandom.nextGaussian() * aSigma;
        Long lval = Math.round(val);

        return lval.intValue();
    }

    /**
     * Calculates the average of an Input List of Integers.
     *
     * @param marks Input List of Integers.
     * @return the average of the input List of Integers.
     */
    public static Double calculateAverage(List<Integer> marks) {
        Integer sum = 0;
        if (marks != null && !marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return 0.;
    }
}
