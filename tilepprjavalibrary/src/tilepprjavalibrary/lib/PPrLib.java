package tilepprjavalibrary.lib;

/**
 * Class to define instances of Io, PPr and FEB objects.
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 * @author Fernando Carrió {@literal (fernando.carrio@cern.ch)}
 * @author Alberto Valero {@literal (alberto.valero@cern.ch)}
 */
public class PPrLib {

    public Io io;
    public PPr ppr;
    public FEB feb;

    /**
     * Boolean flag for full debug output.
     */
    private static Boolean debugPPrLib = false;

    /**
     * PPrDemo Java Library release.
     */
    private static final String RELEASE_NB = "0.1";

    /**
     * Constructor.
     */
    public PPrLib() {
        io = new Io();
        ppr = new PPr(io);
        feb = new FEB(io);
    }

    /**
     * Getter for PPrLib release.
     *
     * @return String release number.
     */
    public static String getRelease() {
        return RELEASE_NB;
    }

    /**
     * Getter for the IPbus debug flag.
     *
     * @return the IPbus debug flag.
     */
    public static Boolean getDebugPPr() {
        return debugPPrLib;
    }

    /**
     * Setter for the IPbus debug flag.
     *
     * @param val Input IPbus debug flag.
     */
    public static void setDebugPPr(Boolean val) {
        debugPPrLib = val;
    }
}
