/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tilepprjavalibrary.reg;

/**
 *
 * @author juano
 */
public class DBReg {

    static public Integer DB_FEB_COMMAND = 0x001;
    static public Integer DB_VERSION = 0x011;
    static public Integer DB_CONF_REG1 = 0x012;
    static public Integer DB_CONF_REG2 = 0x013;
    static public Integer DB_CONF_REG3_LINK_RESETS = 0x014;
    static public Integer DB_CONF_REG4_TEMP = 0x015;
    static public Integer DB_CONF_REG5_SOFT_ERR = 0x016;
    static public Integer DB_CONF_REG6 = 0x017;
    static public Integer DB_XADC_REG1 = 0x0E0;
    static public Integer DB_CIS = 0x121;
    
    static public Integer[] FPGA = {1, 3, 1, 3, 1, 3, 0, 2, 0, 2, 0, 2};
    static public Integer[] FPGA_CHANNEL = {2, 2, 1, 1, 0, 0, 2, 2, 1, 1, 0, 0};

    static public Integer CMD_BIT_E_MASK = 0x400000;
    static public Integer CMD_BIT_E_OFFSET = 22;
    static public Integer CMD_BIT_B_MASK = 0x200000;
    static public Integer CMD_BIT_B_OFFSET = 21;
    static public Integer CMD_FPGA_MASK = 0x1C0000;
    static public Integer CMD_FPGA_OFFSET = 18;
    static public Integer CMD_FPGA_CHANNEL_MASK = 0x030000;
    static public Integer CMD_FPGA_CARD_OFFSET = 16;
    static public Integer CMD_MASK = 0xF000;
    static public Integer CMD_OFFSET = 12;
    static public Integer CMD_DATA_MASK = 0xFFF;

    static public Integer CMD_SET_SWITCHES = 0;
    static public Integer CMD_SET_CIS_DAC = 1;
    static public Integer CMD_GET_SWITCHES = 2;
    static public Integer CMD_GET_CIS_DAC = 3;
    static public Integer CMD_SET_PED_LG_POS = 4;
    static public Integer CMD_SET_PED_LG_NEG = 5;
    static public Integer CMD_SET_PED_HG_POS = 6;
    static public Integer CMD_SET_PED_HG_NEG = 7;
    static public Integer CMD_GET_PED_LG_POS = 8;
    static public Integer CMD_GET_PED_LG_NEG = 9;
    static public Integer CMD_GET_PED_HG_POS = 10;
    static public Integer CMD_GET_PED_HG_NEG = 11;
    static public Integer CMD_LOAD_ADC_DAC_HG = 12;
    static public Integer CMD_LOAD_ADC_DAC_LG = 13;

}
