/* 
 *  PPr Java Library
 *  2019 (c) CERN
 */
package tilepprjavalibrary.reg;

/**
 *
 * @author Juan Valls {@literal (valls@cern.ch)}
 */
public class PPrReg {

    /**
     * Readout Dummy Register.
     */
    static public Integer RO_DUMMY = 0x0;
    static public Integer RO_FIRMWARE_VERSION = 0x1;
    static public Integer RO_GLOBAL = 0x2;
    static public Integer RO_GLOBAL_PIPELINE = 0x3;
    static public Integer RO_GLOBAL_TTC = 0x4;
    static public Integer RO_GLOBAL_TRIGGER = 0x5;
    static public Integer RO_SYNC_CMD = 0x6;
    static public Integer RO_RESET_CRC_CNTS = 0x20;

    static public Integer RO_GLOBAL_TTC_INT_MASK = 0x400000;
    static public Integer RO_GLOBAL_TTC_INT_OFFSET = 22;

    static public Integer CMD_COUNTER = 0x00000007;
    static public Integer ASYNC_CMD_COUNTER = 0x00000008;
    static public Integer SYNC_CMD_COUNTER = 0x00000009;
    static public Integer L1A_COUNTER = 0x0000000A;
    static public Integer EVTS_OUT_COUNTER = 0x0000000B;

    static public Integer LINKS_CTRL_GLB_RESETS = 0x0000000F;
    static public Integer GBT_LINKS_RESETS = 0x0000000E;

    static public Integer SYNC_ADDRESS = 0x00010000;
    static public Integer SYNC_COMMAND = 0x00010001;
    static public Integer SYNC_ADDRESS_MD_MASK = 0x70000000;

    static public Integer ASYNC_ADDRESS = 0x00010002;
    static public Integer ASYNC_COMMAND = 0x00010003;
    static public Integer ASYNC_ADDRESS_MD_MASK = 0x70000000;

    static public Integer SYNC_PPR_ADDRESS = 0x00010004;
    static public Integer SYNC_PPR_COMMAND = 0x00010005;

    static public Integer LAST_EVT_BCID = 0x0000009D;
    static public Integer LAST_EVT_L1ID = 0x0000009E;

    static public Integer PIPELINE_HG = 0x00000100;
    static public Integer PIPELINE_LG = 0x00000700;

    static public Integer CRC_FRAMES1_MD1 = 0x00000021;
    static public Integer CRC_FRAMES2_MD1 = 0x00000022;

    static public Integer CRC_FRAMES1_MD2 = 0x00000029;
    static public Integer CRC_FRAMES2_MD2 = 0x0000002A;

    static public Integer CRC_FRAMES1_MD3 = 0x00000031;
    static public Integer CRC_FRAMES2_MD3 = 0x00000032;

    static public Integer CRC_FRAMES1_MD4 = 0x00000039;
    static public Integer CRC_FRAMES2_MD4 = 0x0000003A;

    static public Integer LINK_STAT_MD1A = 0x00000010;
    static public Integer LINK_STAT_MD1B = 0x00000011;
    static public Integer LINK_STAT_MD2A = 0x00000012;
    static public Integer LINK_STAT_MD2B = 0x00000013;
    static public Integer LINK_STAT_MD3A = 0x00000014;
    static public Integer LINK_STAT_MD3B = 0x00000015;
    static public Integer LINK_STAT_MD4A = 0x00000016;
    static public Integer LINK_STAT_MD4B = 0x00000017;

    static public Integer CRC_ERR_LINK_A0_MD1 = 0x00000023;
    static public Integer CRC_ERR_LINK_A1_MD1 = 0x00000024;
    static public Integer CRC_ERR_LINK_B0_MD1 = 0x00000025;
    static public Integer CRC_ERR_LINK_B1_MD1 = 0x00000026;
    static public Integer CRC_ERR_TOT_SIDE_A_MD1 = 0x00000027;
    static public Integer CRC_ERR_TOT_SIDE_B_MD1 = 0x00000028;

    static public Integer CRC_ERR_LINK_A0_MD2 = 0x0000002B;
    static public Integer CRC_ERR_LINK_A1_MD2 = 0x0000002C;
    static public Integer CRC_ERR_LINK_B0_MD2 = 0x0000002D;
    static public Integer CRC_ERR_LINK_B1_MD2 = 0x0000002E;
    static public Integer CRC_ERR_TOT_SIDE_A_MD2 = 0x0000002F;
    static public Integer CRC_ERR_TOT_SIDE_B_MD2 = 0x00000030;

    static public Integer CRC_ERR_LINK_A0_MD3 = 0x00000033;
    static public Integer CRC_ERR_LINK_A1_MD3 = 0x00000034;
    static public Integer CRC_ERR_LINK_B0_MD3 = 0x00000035;
    static public Integer CRC_ERR_LINK_B1_MD3 = 0x00000036;
    static public Integer CRC_ERR_TOT_SIDE_A_MD3 = 0x00000037;
    static public Integer CRC_ERR_TOT_SIDE_B_MD3 = 0x00000038;

    static public Integer CRC_ERR_LINK_A0_MD4 = 0x0000003B;
    static public Integer CRC_ERR_LINK_A1_MD4 = 0x0000003C;
    static public Integer CRC_ERR_LINK_B0_MD4 = 0x0000003D;
    static public Integer CRC_ERR_LINK_B1_MD4 = 0x0000003E;
    static public Integer CRC_ERR_TOT_SIDE_A_MD4 = 0x0000003F;
    static public Integer CRC_ERR_TOT_SIDE_B_MD4 = 0x00000040;

    static public Integer RAM_MEM_HG = 0x00000100;
    static public Integer RAM_MEM_LG = 0x00000700;

    static public Integer[] PPR_FEB_CTRL_REG = {0x00010011, 0x00110011, 0x00210011, 0x00310011};
    static public Integer[] PPR_FEB_DATA_REG = {0x00010012, 0x00110012, 0x00210012, 0x00310012};

    static public Integer[] PPR_FEB_SWITCHES = {0x7, 0x10, 0x19, 0x22, 0x2B, 0x34};
    static public Integer[] PPR_FEB_CIS_DAC = {0x8, 0x11, 0x1A, 0x23, 0x2C, 0x35};
    static public Integer[] PPR_FEB_PED_HG_POS = {0x9, 0x12, 0x1B, 0x24, 0x2D, 0x36};
    static public Integer[] PPR_FEB_PED_HG_NEG = {0xA, 0x13, 0x1C, 0x25, 0x2E, 0x37};
    static public Integer[] PPR_FEB_PED_LG_POS = {0xB, 0x14, 0x1D, 0x26, 0x2F, 0x38};
    static public Integer[] PPR_FEB_PED_LG_NEG = {0xC, 0x15, 0x1E, 0x27, 0x30, 0x39};

    static public Integer[] PPR_DB_XADC = {0x9, 0x12, 0x21, 0x24, 0x2D, 0x36};

    static public Integer LAT_MD1_MD2 = 0x00000085;
    static public Integer LAT_MD3_MD4 = 0x00000086;
}
